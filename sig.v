`timescale 1ns / 1ps
// ********************************************************************
//	FileName	: sig.v
//	Author		：hpy
//	Email		：yuan_hp@qq.com
//	Date		：2020年07月27日
//	Description	：
// --------------------------------------------------------------------
module sig(
	output y
);
assign y = 1'b1;
endmodule
