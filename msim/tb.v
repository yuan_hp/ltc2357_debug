`timescale 1ns / 1ps
module tb ;
reg clk,rst;

//生成始时钟
parameter NCLK = 4;
initial begin
	clk=0;
	forever clk=#(NCLK/2) ~clk; 
end 

/****************** BEGIN ADD module inst ******************/
reg busy;
reg pre_clk;
initial begin
	pre_clk = 0;
	forever pre_clk =#(NCLK*5) ~pre_clk;
end

initial begin
	busy = 0;
	forever begin
		busy =#(NCLK*100) 0;
		busy =#(NCLK*800) 1;
	end
end


ltc ltc(
	.clk(pre_clk),
	.rst_n(rst),
	.busy(busy)
);

rec rec();
/****************** BEGIN END module inst ******************/

initial begin
    $dumpfile("wave.lxt2");
    $dumpvars(2, tb);   //dumpvars(深度, 实例化模块1，实例化模块2，.....)
end




initial begin
   	rst = 1;
    	#(NCLK*10) rst=0;
    	#(NCLK*10) rst=1; //复位信号

	repeat(10000) @(posedge clk)begin

	end
	$display("运行结束！");
	$dumpflush;
	$finish;
	$stop;	
end 
endmodule
