`timescale 1ns / 1ps
// ********************************************************************
//	FileName	: rec.v
//	Author		：hpy
//	Email		：yuan_hp@qq.com
//	Date		：2020年08月09日
//	Description	：
// --------------------------------------------------------------------
module rec(
);

reg sdi,scki,scko,sdo0,busy;
integer i;

reg [23:0] send_data,send_r,sdo_data,sdo_r;
//产生随机测试的数据
initial begin
	sdi=0;
	scki=0;
	scko=0;
	sdo0=0;
	busy=0;
	forever begin
		sdi=0;scki=1;
		send_data = $random;
		sdo_data = $random;
		send_r = send_data;
		sdo_r = sdo_data;
		sdo0 = sdo_r[23];
		busy = 1; #200;
		busy = 0;#2
		scki = 0;
		scko=0;#60;
		for(i=0;i<24;i=i+1)begin
			scki=1; sdi = send_r[23] ; send_r= send_r << 1 ;scko=~scko ; sdo_r = sdo_r<<1; sdo0 = sdo_r[23] ;#20;
			scki=0; scko = scko ;#20;
		end
		scki=1;
		sdo0=1'bx;
		#60;
	end
end

//模拟接收LTC2357数据
reg [23:0]rdata;
reg [24:0]rec_r; 
//为了使用时钟下降沿来读取数据，设计中必须保证时钟在
//空闲的时候，处于高电平，。在使用时钟下降沿读取数据时，将会
//多移位一次，因此获取24位的数据，我们要薄脆25位数据，在刷新
//转换的数据时我们取临时ad数据存储的高24位
always@(negedge scki or posedge busy)
begin
	if(busy)
		rdata <= rec_r[24:1];
	else 
		rec_r <= {rec_r[23:0],sdo0};
end

endmodule





















