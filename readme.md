---
typora-copy-images-to: image
typora-root-url: ./
---

# 说明

这是一个在linux上使用step fpga的ltc_2357芯片调试工程，本模板针对的是u盘模式的fpga，解决在liunx下拷贝文件到fpga的问题，这里使用tcl的拷贝函数可以解决该问题。
修改工程后，我们在终端执行 `./run.tcl`可以自动生成文件和下载到fpga，当然需要根据实际情况更改`run.tcl`文件。

# 开发环境

**仿真环境**

* iverilog 
* gtkwave

**脚本支持**

* TCL(Lattice的tcl解释器)
* bash shell(linux或者cygwin环境下)

**FPGA EDA工具**

* Diamond

**STEP FPGA型号**

* [Lattice XO2-4000 U盘模式](https://www.stepfpga.com/doc/step-mxo2-c)



# 关键性目录结构

```
.
├── 引脚.png stepfpga开发板引脚情况
├── auto_rst.v 上电自动复位模块
├── divide.v 任意时钟分频模块
├── image
├── impl1 综合生成文件存放文件夹
├── ltc2357_master.v ltc2357驱动（主机模式）
├── monopulse.v 单脉冲发生模块（用于产生CNV信号触发LTC2357芯片开始AD转换）
├── msim iverilog+gtkwave+bash的仿真文件夹，在该文件夹下写的模块用于仿真
│   ├── ltc.v
│   ├── run
│   ├── sim 仿真生成的文件
│   │   ├── wave
│   │   └── wave.lxt2 波形文件
│   ├── tags
│   └── tb.v
├── pll_clk.edn
├── pll_clk_generate.log
├── pll_clk.ipx
├── pll_clk.lpc
├── pll_clk.naf
├── pll_clk.sort
├── pll_clk.srp
├── pll_clk.sym
├── pll_clk.v PLL配置 12MHz->120MHz
├── promote.xml
├── readme.md 项目说明文件
├── reportview.xml
├── run.tcl 综合，下载的tcl脚本（根据实际的硬件需要更改文件的目录部分）
├── top.v verilog顶层文件
└── upCloud 上传本地项目到gitee

```



# 串口模块

引入串口模块以方便调试，此处设计的串口为上位机发送多少字节，下位机就返回多少字节，因此上位机发送的字节可以作为下位机返回数据的选择控制开关，根据下位机接收到的字节，决定下位机发送什么内容到上位机。例如：

```verilog
//+++++++++++++++++  串口模块  +++++++++++++++++++++++
reg [7:0] tx_data;
wire [7:0] rx_data;
always@(rx_data)
begin
	case(rx_data)
		8'h00:tx_data = rx_data;
		8'h01:tx_data = 8'h50;
		8'h02:tx_data = 8'h53;
		default:tx_data = rx_data;
	endcase
end
```

串口的模块实现所有代码均在`Uart_Bus.v`文件中。其顶层IO如下：

```verilog
//串口顶层模块
module Uart_Bus #
(
    parameter				BPS_PARA = 1250 //当使用12MHz时钟时波特率参数选择1250对应9600的波特率
)
(
    input					clk_in,			//系统时钟
    input					rst_n_in,		//系统复位，低有效
    input					rs232_rx,		//FPGA中UART接收端，分配给UART模块中的发送端TXD
    output					rs232_tx,		//FPGA中UART发送端，分配给UART模块中的接收端RXD
    output    [7:0]         rx_data,       //读到的数据
    input     [7:0]         tx_data        //发送的数据
);		
 
```



# 基于Linux的串口通信脚本

为了使得数据测试更方便，本项目在进行板上验证的时候使用bash shell的方式来实现对串口的读与写操作。

项目中的`usb.sh`实现获取一次LTC2357芯片ad数值，该脚本，将会得到如下结果：

![image-20200809213257243](/image/image-20200809213257243.png)

数据格式参考文档时序部分！

# LTC2357使用要点

**CMOS模式下的时序图**

![](/image/image-20200808222638573.png)



**LTC2357配置参数表**

![image-20200809212551730](/image/image-20200809212551730.png)





# 接收数据部分仿真

根据CMOS下LTC2357的时序，我们在使用FPGA驱动了芯片工作后，AD转换后的数据这将会在时钟的上升沿锁存数据，那么为了得到稳定的采样数据，应该在时钟的下降沿进行采样数据，但是我们在下降沿采样数据，如果使得时钟在空闲时候为低电平，我们将会漏掉ad数据的最高位，并额外读取了下一次AD数据的最高位填充到本次数据的最低位，为了解决这个问题，临时存储ad转换数据的寄存器采取比实际数据多一位的方式来存取，当当前ad数据获取完毕，我们锁存临时数据的高24位做为本次AD转换的转换数据即可！

```verilog
//模拟接收LTC2357数据
reg [23:0]rdata;
reg [24:0]rec_r; 
//为了使用时钟下降沿来读取数据，设计中必须保证时钟在
//空闲的时候，处于高电平，。在使用时钟下降沿读取数据时，将会
//多移位一次，因此获取24位的数据，我们要薄脆25位数据，在刷新
//转换的数据时我们取临时ad数据存储的高24位
always@(negedge scki or posedge busy)
begin
	if(busy)
		rdata <= rec_r[24:1];
	else 
		rec_r <= {rec_r[23:0],sdo0};
end
```



上述方式先进行仿真：

仿真脚本对应目录中的`msim/rev.v`文件。以下是完整的仿真文件！

```verilog
`timescale 1ns / 1ps
// ********************************************************************
//	FileName	: rec.v
//	Author		：hpy
//	Email		：yuan_hp@qq.com
//	Date		：2020年08月09日
//	Description	：
// --------------------------------------------------------------------
module rec(
);

reg sdi,scki,scko,sdo0,busy;
integer i;

reg [23:0] send_data,send_r,sdo_data,sdo_r;
//产生随机测试的数据
initial begin
	sdi=0;
	scki=0;
	scko=0;
	sdo0=0;
	busy=0;
	forever begin
		sdi=0;scki=1;
		send_data = $random;
		sdo_data = $random;
		send_r = send_data;
		sdo_r = sdo_data;
		sdo0 = sdo_r[23];
		busy = 1; #200;
		busy = 0;#2
		scki = 0;
		scko=0;#60;
		for(i=0;i<24;i=i+1)begin
			scki=1; sdi = send_r[23] ; send_r= send_r << 1 ;scko=~scko ; sdo_r = sdo_r<<1; sdo0 = sdo_r[23] ;#20;
			scki=0; scko = scko ;#20;
		end
		scki=1;
		sdo0=1'bx;
		#60;
	end
end

//模拟接收LTC2357数据
reg [23:0]rdata;
reg [24:0]rec_r; 
//为了使用时钟下降沿来读取数据，设计中必须保证时钟在
//空闲的时候，处于高电平，。在使用时钟下降沿读取数据时，将会
//多移位一次，因此获取24位的数据，我们要薄脆25位数据，在刷新
//转换的数据时我们取临时ad数据存储的高24位
always@(negedge scki or posedge busy)
begin
	if(busy)
		rdata <= rec_r[24:1];
	else 
		rec_r <= {rec_r[23:0],sdo0};
end

endmodule
```



该文件在`tb.v`中实例化，仿真结果如下：

![1596917597774](/image/1596917597774.png)





# 引脚对应关系

**芯片连接框图**

![1596918232470](/image/1596918232470.png)



| module引脚名 | FPGA引脚 | step引脚编号 |
| :----------: | :------: | :----------: |
|    clk_in    |    C1    |      -       |
|   rst_n_in   |   L14    |     KEY1     |
|     led1     |   N13    |     LED1     |
|     led2     |   M12    |     LED2     |
|   **cnv**    |    E3    |    GPIO0     |
|   **csn**    |    F3    |    GPIO1     |
|   **busy**   |    G3    |    GPIO2     |
|   **scki**   |    H3    |    GPIO3     |
|   **sdi**    |    J2    |    GPIO4     |
|   rs232_rx   |    P8    |    GPIO14    |
|   rs232_tx   |    N8    |    GPIO15    |
|   **sdo0**   |    J3    |    GPIO5     |
|   **sdo1**   |    K2    |    GPIO6     |
|   **sdo2**   |    K3    |    GPIO7     |
|   **sdo3**   |    L3    |    GPIO8     |

# 板上测试

用于测试的fpga为小脚丫fpga，型号为STEP-MXO2-C，该款fpga的晶振使用的是12MHz的，使用中，使用内部PLL将频率倍频到120MHz作为系统主频。

LTC2357的测试电路板使用自己画的`Debug-4-channel-AD-LTC2357`电路板。该电路板只包含LTC2357这款主要的芯片，专用于测试写的驱动的。可用测试记录如下表所示：

|          测试项目          |                           测试结果                           | 存在的问题 | 解决方案 |
| :------------------------: | :----------------------------------------------------------: | :--------: | :------: |
| 参数配置（测试范围、精度） |                    配置结果和返回结果匹配                    |     无     |    无    |
|       返回数据正确性       | 各通道均能返回正确数据，判断依据为读取的配置参数和根据通道ID的正确性 |     无     |    无    |
|        转换关系测试        | 配置-10.24~10.24的范围下，以+3.3V，+5V进行测试，结果基本匹配，其精度后期移植后今次那个测试（10.24×AD/2**15) |     无     |    无    |

