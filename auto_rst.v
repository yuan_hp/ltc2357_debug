`timescale 1ns / 1ps
// ********************************************************************
//	FileName	: auto_rst.v
//	Author		：hpy
//	Email		：yuan_hp@qq.com
//	Date		：
//	Description	：stepfpga上电自动生成复位信号模块
// --------------------------------------------------------------------
module auto_rst(
	input clk,
	output reg rst_n
);

reg [20:0] cnt;
initial 
	cnt = 0;

always @(posedge clk)begin 
	if (cnt<21'd500)begin
		rst_n <= 1'b1;
		cnt <= cnt + 1;
	end
	else if(cnt < 21'd1000)begin
		rst_n <= 1'b0;
		cnt <= cnt + 1;
	end
	else begin
		rst_n <= 1'b1;
	end
end

endmodule