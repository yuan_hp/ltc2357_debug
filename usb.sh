#!/usr/bin/env bash
#-------------------------------------------------------
#	FileName	: usb.sh
#	Author		：hpy
#	Date		：2020年08月09日
#	Description	：
#-------------------------------------------------------

getData(){
	stty -F /dev/ttyUSB0 raw speed 9600 -echo min 0 time 10 &> /dev/null
	case $1 in
		0)sendbuf="\x60\xcc";;
		1)sendbuf="\x61\xcc";;
		2)sendbuf="\x62\xcc";;
		3)sendbuf="\x63\xcc";;
		"*")exit 1;;
	esac
		
	cnt=23
	while [[ $cnt -ge 0 ]]
	do
		t=$(printf "%x" $cnt ) #十进制数据转十六进制数据显示
		sendbuf="$sendbuf\x$t"
		cnt=$[$cnt-1]
	done
	#echo $sendbuf
	echo -e -n "$sendbuf" >/dev/ttyUSB0 
	rec="ch$1 : $(cat -v /dev/ttyUSB0)"
	echo $rec
	#sleep 1
}
main(){
	getData 0
	getData 1
	getData 2
	getData 3
	exit 0	
}

main 



