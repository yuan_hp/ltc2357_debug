// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.10.0.111.2
// Netlist written on Sun Aug  9 21:20:39 2020
//
// Verilog Description of module top
//

module top (clk_in, rst_n_in, led1, busy, cnv, scki, sdi, csn, 
            sdo0, sdo1, sdo2, sdo3, rs232_rx, rs232_tx) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(1[8:11])
    input clk_in;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    input rst_n_in;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(4[8:16])
    output led1;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(5[9:13])
    input busy;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(7[8:12])
    output cnv;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(8[9:12])
    output scki;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(9[9:13])
    output sdi;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(10[9:12])
    output csn;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(11[9:12])
    input sdo0;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(13[8:12])
    input sdo1;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(14[8:12])
    input sdo2;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(15[8:12])
    input sdo3;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(16[8:12])
    input rs232_rx;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(18[8:16])
    output rs232_tx;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(19[9:17])
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire scki_c /* synthesis is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(9[9:13])
    wire clk /* synthesis SET_AS_NETWORK=clk, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(21[6:9])
    wire pre_clk /* synthesis is_clock=1, SET_AS_NETWORK=pre_clk */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(46[6:13])
    wire clk_N_242 /* synthesis is_inv_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[25:30])
    
    wire GND_net, rst_n_in_c, led1_c, busy_c, cnv_c, sdi_c, sdo0_c, 
        sdo1_c, sdo2_c, sdo3_c, rs232_rx_c, rs232_tx_c, rst_n_auto;
    wire [23:0]ad_data0;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(30[12:20])
    wire [23:0]ad_data1;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(30[23:31])
    wire [23:0]ad_data2;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(30[33:41])
    wire [23:0]ad_data3;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(30[43:51])
    wire [7:0]tx_data;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(104[11:18])
    wire [7:0]rx_data;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(105[12:19])
    wire [23:0]tmp;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(106[12:15])
    
    wire clk_in_c_enable_42, n3448, n12, n3485, n3458, n3455, n3452, 
        n3449, n3446, n3443, n3440, n3482, n3479, n3515, n3514, 
        n3427, n3447, n3476, n3473, n3470, n3467, n3464, n3461, 
        n3513, n3510, n3507, n3504, n3501, n3498, n3495, n3512, 
        n3426, n3511, n3509, n3445, n3444, n3442, n3508, n3506, 
        n3505, n3503, n3441, scki_N_353_enable_169;
    wire [2:0]cst;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(26[11:14])
    wire [2:0]cnt_adj_1018;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(27[26:29])
    
    wire n3646;
    wire [2:0]nst_2__N_277;
    
    wire n3439, y_N_295, scki_N_359, n3502, n3500;
    wire [23:0]send_data_23__N_809;
    
    wire n3438, n3436, n3435, n3434, n3433, n3499, n3497, n3425, 
        n3496, n3494, n3432, n416, n3492, n477, n3437, n3431, 
        n3493, n3491, n3430, n3490, n3424, n3489, n3488, n3429, 
        n3487, n3486, n3428, n3484, n3483, n3481, pre_clk_enable_5, 
        n3480, n3478, n3477, n8, n3475, n3474, n3472, n3471, 
        n3469, n3468, n3466, n3423, n3465, n3463, n3462, n3460, 
        n3422, n3459, n3457, n3371, n3456, n3454, VCC_net, n3874, 
        n3453, n3644, n3659, n3451, n3657, n3450, n3654, n3647;
    
    PFUMX i2841 (.BLUT(ad_data0[16]), .ALUT(ad_data1[16]), .C0(rx_data[0]), 
          .Z(n3513));
    PFUMX i2838 (.BLUT(ad_data0[17]), .ALUT(ad_data1[17]), .C0(rx_data[0]), 
          .Z(n3510));
    PFUMX i2774 (.BLUT(ad_data0[4]), .ALUT(ad_data1[4]), .C0(rx_data[0]), 
          .Z(n3446));
    PFUMX i2835 (.BLUT(ad_data0[18]), .ALUT(ad_data1[18]), .C0(rx_data[0]), 
          .Z(n3507));
    PFUMX i2832 (.BLUT(ad_data0[19]), .ALUT(ad_data1[19]), .C0(rx_data[0]), 
          .Z(n3504));
    PFUMX i2829 (.BLUT(ad_data0[20]), .ALUT(ad_data1[20]), .C0(rx_data[0]), 
          .Z(n3501));
    PFUMX i2826 (.BLUT(ad_data0[21]), .ALUT(ad_data1[21]), .C0(rx_data[0]), 
          .Z(n3498));
    PFUMX i2823 (.BLUT(ad_data0[22]), .ALUT(ad_data1[22]), .C0(rx_data[0]), 
          .Z(n3495));
    PFUMX i2802 (.BLUT(ad_data2[10]), .ALUT(ad_data3[10]), .C0(rx_data[0]), 
          .Z(n3474));
    PFUMX i2768 (.BLUT(ad_data0[6]), .ALUT(ad_data1[6]), .C0(rx_data[0]), 
          .Z(n3440));
    PFUMX i2808 (.BLUT(ad_data2[8]), .ALUT(ad_data3[8]), .C0(rx_data[0]), 
          .Z(n3480));
    IB busy_pad (.I(busy), .O(busy_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(7[8:12])
    LUT4 i2757_3_lut (.A(tmp[14]), .B(tmp[15]), .C(rx_data[0]), .Z(n3429)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2757_3_lut.init = 16'hcaca;
    LUT4 i2756_3_lut (.A(tmp[12]), .B(tmp[13]), .C(rx_data[0]), .Z(n3428)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2756_3_lut.init = 16'hcaca;
    LUT4 i2755_3_lut (.A(tmp[10]), .B(tmp[11]), .C(rx_data[0]), .Z(n3427)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2755_3_lut.init = 16'hcaca;
    LUT4 i2754_3_lut (.A(tmp[8]), .B(tmp[9]), .C(rx_data[0]), .Z(n3426)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2754_3_lut.init = 16'hcaca;
    LUT4 i2753_3_lut (.A(tmp[6]), .B(tmp[7]), .C(rx_data[0]), .Z(n3425)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2753_3_lut.init = 16'hcaca;
    LUT4 i2752_3_lut (.A(tmp[4]), .B(tmp[5]), .C(rx_data[0]), .Z(n3424)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2752_3_lut.init = 16'hcaca;
    LUT4 i2751_3_lut (.A(tmp[2]), .B(tmp[3]), .C(rx_data[0]), .Z(n3423)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2751_3_lut.init = 16'hcaca;
    LUT4 i2750_3_lut (.A(tmp[0]), .B(tmp[1]), .C(rx_data[0]), .Z(n3422)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2750_3_lut.init = 16'hcaca;
    LUT4 i331_3_lut (.A(scki_N_359), .B(send_data_23__N_809[12]), .C(n3644), 
         .Z(pre_clk_enable_5)) /* synthesis lut_function=(A (B+!(C))+!A (B)) */ ;
    defparam i331_3_lut.init = 16'hcece;
    PFUMX i2765 (.BLUT(ad_data0[7]), .ALUT(ad_data1[7]), .C0(rx_data[0]), 
          .Z(n3437));
    PFUMX i2811 (.BLUT(ad_data2[0]), .ALUT(ad_data3[0]), .C0(rx_data[0]), 
          .Z(n3483));
    IB rst_n_in_pad (.I(rst_n_in), .O(rst_n_in_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(4[8:16])
    IB clk_in_pad (.I(clk_in), .O(clk_in_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    OB rs232_tx_pad (.I(rs232_tx_c), .O(rs232_tx));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(19[9:17])
    OB csn_pad (.I(GND_net), .O(csn));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(11[9:12])
    OB sdi_pad (.I(sdi_c), .O(sdi));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(10[9:12])
    OB scki_pad (.I(scki_c), .O(scki));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(9[9:13])
    PFUMX i2817 (.BLUT(n3485), .ALUT(n3486), .C0(rx_data[1]), .Z(n3489));
    PFUMX i2771 (.BLUT(ad_data0[5]), .ALUT(ad_data1[5]), .C0(rx_data[0]), 
          .Z(n3443));
    PFUMX i2818 (.BLUT(n3487), .ALUT(n3488), .C0(rx_data[1]), .Z(n3490));
    LUT4 i2970_4_lut (.A(rx_data[3]), .B(n3654), .C(rx_data[7]), .D(rx_data[4]), 
         .Z(tx_data[4])) /* synthesis lut_function=(!(A (B+(C+(D)))+!A (B+(C)))) */ ;
    defparam i2970_4_lut.init = 16'h0103;
    OB cnv_pad (.I(cnv_c), .O(cnv));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(8[9:12])
    FD1P3AX tmp_i0_i0 (.D(n3484), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i0.GSR = "DISABLED";
    PFUMX i2820 (.BLUT(ad_data0[23]), .ALUT(ad_data1[23]), .C0(rx_data[0]), 
          .Z(n3492));
    OB led1_pad (.I(led1_c), .O(led1));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(5[9:13])
    GSR GSR_INST (.GSR(rst_n_in_c));
    LUT4 i931_1_lut (.A(busy_c), .Z(scki_N_353_enable_169)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(7[8:12])
    defparam i931_1_lut.init = 16'h5555;
    PFUMX i2821 (.BLUT(ad_data2[23]), .ALUT(ad_data3[23]), .C0(rx_data[0]), 
          .Z(n3493));
    LUT4 i2_2_lut_rep_27 (.A(rx_data[5]), .B(rx_data[6]), .Z(n3654)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i2_2_lut_rep_27.init = 16'heeee;
    LUT4 i1_3_lut_4_lut (.A(rx_data[5]), .B(rx_data[6]), .C(n12), .D(rx_data[7]), 
         .Z(tx_data[0])) /* synthesis lut_function=(!(A+(B+((D)+!C)))) */ ;
    defparam i1_3_lut_4_lut.init = 16'h0010;
    PFUMX i2824 (.BLUT(ad_data2[22]), .ALUT(ad_data3[22]), .C0(rx_data[0]), 
          .Z(n3496));
    LUT4 rst_n_auto_I_0_2_lut_rep_30 (.A(rst_n_auto), .B(rst_n_in_c), .Z(n3657)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(23[16:37])
    defparam rst_n_auto_I_0_2_lut_rep_30.init = 16'h8888;
    PFUMX i2799 (.BLUT(ad_data2[11]), .ALUT(ad_data3[11]), .C0(rx_data[0]), 
          .Z(n3471));
    LUT4 i2816_3_lut (.A(tmp[22]), .B(tmp[23]), .C(rx_data[0]), .Z(n3488)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2816_3_lut.init = 16'hcaca;
    LUT4 rst_n_I_0_1_lut_rep_20_2_lut (.A(rst_n_auto), .B(rst_n_in_c), .Z(n3647)) /* synthesis lut_function=(!(A (B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(23[16:37])
    defparam rst_n_I_0_1_lut_rep_20_2_lut.init = 16'h7777;
    LUT4 i2025_2_lut_rep_19_3_lut (.A(rst_n_auto), .B(rst_n_in_c), .C(n477), 
         .Z(n3646)) /* synthesis lut_function=(A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(23[16:37])
    defparam i2025_2_lut_rep_19_3_lut.init = 16'h8080;
    LUT4 i2_3_lut_4_lut (.A(rst_n_auto), .B(rst_n_in_c), .C(cnt_adj_1018[0]), 
         .D(n477), .Z(n3371)) /* synthesis lut_function=(A (B (C (D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(23[16:37])
    defparam i2_3_lut_4_lut.init = 16'h8000;
    LUT4 i2_3_lut_4_lut_adj_47 (.A(rst_n_auto), .B(rst_n_in_c), .C(n3659), 
         .D(n416), .Z(scki_N_359)) /* synthesis lut_function=(!(((C+!(D))+!B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(23[16:37])
    defparam i2_3_lut_4_lut_adj_47.init = 16'h0800;
    PFUMX i2758 (.BLUT(n3422), .ALUT(n3423), .C0(rx_data[1]), .Z(n3430));
    PFUMX i2796 (.BLUT(ad_data2[12]), .ALUT(ad_data3[12]), .C0(rx_data[0]), 
          .Z(n3468));
    PFUMX i2805 (.BLUT(ad_data2[9]), .ALUT(ad_data3[9]), .C0(rx_data[0]), 
          .Z(n3477));
    PFUMX i2759 (.BLUT(n3424), .ALUT(n3425), .C0(rx_data[1]), .Z(n3431));
    PFUMX i2827 (.BLUT(ad_data2[21]), .ALUT(ad_data3[21]), .C0(rx_data[0]), 
          .Z(n3499));
    LUT4 i2815_3_lut (.A(tmp[20]), .B(tmp[21]), .C(rx_data[0]), .Z(n3487)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2815_3_lut.init = 16'hcaca;
    PFUMX i2760 (.BLUT(n3426), .ALUT(n3427), .C0(rx_data[1]), .Z(n3432));
    PFUMX i2830 (.BLUT(ad_data2[20]), .ALUT(ad_data3[20]), .C0(rx_data[0]), 
          .Z(n3502));
    IB sdo0_pad (.I(sdo0), .O(sdo0_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(13[8:12])
    IB sdo1_pad (.I(sdo1), .O(sdo1_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(14[8:12])
    IB sdo2_pad (.I(sdo2), .O(sdo2_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(15[8:12])
    IB sdo3_pad (.I(sdo3), .O(sdo3_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(16[8:12])
    IB rs232_rx_pad (.I(rs232_rx), .O(rs232_rx_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(18[8:16])
    PFUMX i2833 (.BLUT(ad_data2[19]), .ALUT(ad_data3[19]), .C0(rx_data[0]), 
          .Z(n3505));
    PFUMX i2761 (.BLUT(n3428), .ALUT(n3429), .C0(rx_data[1]), .Z(n3433));
    PFUMX i2836 (.BLUT(ad_data2[18]), .ALUT(ad_data3[18]), .C0(rx_data[0]), 
          .Z(n3508));
    PFUMX i2766 (.BLUT(ad_data2[7]), .ALUT(ad_data3[7]), .C0(rx_data[0]), 
          .Z(n3438));
    PFUMX i2839 (.BLUT(ad_data2[17]), .ALUT(ad_data3[17]), .C0(rx_data[0]), 
          .Z(n3511));
    PFUMX i2769 (.BLUT(ad_data2[6]), .ALUT(ad_data3[6]), .C0(rx_data[0]), 
          .Z(n3441));
    PFUMX i2772 (.BLUT(ad_data2[5]), .ALUT(ad_data3[5]), .C0(rx_data[0]), 
          .Z(n3444));
    PFUMX i2842 (.BLUT(ad_data2[16]), .ALUT(ad_data3[16]), .C0(rx_data[0]), 
          .Z(n3514));
    PFUMX i2775 (.BLUT(ad_data2[4]), .ALUT(ad_data3[4]), .C0(rx_data[0]), 
          .Z(n3447));
    PFUMX i2810 (.BLUT(ad_data0[0]), .ALUT(ad_data1[0]), .C0(rx_data[0]), 
          .Z(n3482));
    PFUMX i2807 (.BLUT(ad_data0[8]), .ALUT(ad_data1[8]), .C0(rx_data[0]), 
          .Z(n3479));
    PFUMX i2804 (.BLUT(ad_data0[9]), .ALUT(ad_data1[9]), .C0(rx_data[0]), 
          .Z(n3476));
    PFUMX i2801 (.BLUT(ad_data0[10]), .ALUT(ad_data1[10]), .C0(rx_data[0]), 
          .Z(n3473));
    FD1P3AX tmp_i0_i1 (.D(n3457), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[1]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i1.GSR = "DISABLED";
    FD1P3AX tmp_i0_i2 (.D(n3454), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i2.GSR = "DISABLED";
    FD1P3AX tmp_i0_i3 (.D(n3451), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[3]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i3.GSR = "DISABLED";
    FD1P3AX tmp_i0_i4 (.D(n3448), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i4.GSR = "DISABLED";
    FD1P3AX tmp_i0_i5 (.D(n3445), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[5]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i5.GSR = "DISABLED";
    FD1P3AX tmp_i0_i6 (.D(n3442), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i6.GSR = "DISABLED";
    FD1P3AX tmp_i0_i7 (.D(n3439), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[7]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i7.GSR = "DISABLED";
    FD1P3AX tmp_i0_i8 (.D(n3481), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i8.GSR = "DISABLED";
    FD1P3AX tmp_i0_i9 (.D(n3478), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[9]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i9.GSR = "DISABLED";
    FD1P3AX tmp_i0_i10 (.D(n3475), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[10]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i10.GSR = "DISABLED";
    FD1P3AX tmp_i0_i11 (.D(n3472), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[11]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i11.GSR = "DISABLED";
    FD1P3AX tmp_i0_i12 (.D(n3469), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[12]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i12.GSR = "DISABLED";
    FD1P3AX tmp_i0_i13 (.D(n3466), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[13]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i13.GSR = "DISABLED";
    FD1P3AX tmp_i0_i14 (.D(n3463), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[14]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i14.GSR = "DISABLED";
    FD1P3AX tmp_i0_i15 (.D(n3460), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[15]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i15.GSR = "DISABLED";
    FD1P3AX tmp_i0_i16 (.D(n3515), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[16]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i16.GSR = "DISABLED";
    FD1P3AX tmp_i0_i17 (.D(n3512), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[17]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i17.GSR = "DISABLED";
    FD1P3AX tmp_i0_i18 (.D(n3509), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[18]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i18.GSR = "DISABLED";
    FD1P3AX tmp_i0_i19 (.D(n3506), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[19]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i19.GSR = "DISABLED";
    FD1P3AX tmp_i0_i20 (.D(n3503), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[20]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i20.GSR = "DISABLED";
    FD1P3AX tmp_i0_i21 (.D(n3500), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[21]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i21.GSR = "DISABLED";
    FD1P3AX tmp_i0_i22 (.D(n3497), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[22]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i22.GSR = "DISABLED";
    FD1P3AX tmp_i0_i23 (.D(n3494), .SP(clk_in_c_enable_42), .CK(clk_in_c), 
            .Q(tmp[23]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(109[7] 124[4])
    defparam tmp_i0_i23.GSR = "DISABLED";
    PFUMX i2798 (.BLUT(ad_data0[11]), .ALUT(ad_data1[11]), .C0(rx_data[0]), 
          .Z(n3470));
    PFUMX i2795 (.BLUT(ad_data0[12]), .ALUT(ad_data1[12]), .C0(rx_data[0]), 
          .Z(n3467));
    PFUMX i2792 (.BLUT(ad_data0[13]), .ALUT(ad_data1[13]), .C0(rx_data[0]), 
          .Z(n3464));
    LUT4 i2814_3_lut (.A(tmp[18]), .B(tmp[19]), .C(rx_data[0]), .Z(n3486)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2814_3_lut.init = 16'hcaca;
    ltc2357_master ltc2357_master (.scki_c(scki_c), .ad_data0({ad_data0}), 
            .ad_data2({ad_data2}), .busy_c(busy_c), .pre_clk(pre_clk), 
            .scki_N_359(scki_N_359), .n3647(n3647), .n416(n416), .n3644(n3644), 
            .pre_clk_enable_5(pre_clk_enable_5), .\send_data_23__N_809[12] (send_data_23__N_809[12]), 
            .scki_N_353_enable_169(scki_N_353_enable_169), .sdo1_c(sdo1_c), 
            .sdo2_c(sdo2_c), .sdo3_c(sdo3_c), .n3874(n3874), .ad_data1({ad_data1}), 
            .ad_data3({ad_data3}), .n3659(n3659), .n3657(n3657), .sdo0_c(sdo0_c), 
            .sdi_c(sdi_c)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(84[16] 101[2])
    LUT4 i2813_3_lut (.A(tmp[16]), .B(tmp[17]), .C(rx_data[0]), .Z(n3485)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2813_3_lut.init = 16'hcaca;
    PFUMX i2789 (.BLUT(ad_data0[14]), .ALUT(ad_data1[14]), .C0(rx_data[0]), 
          .Z(n3461));
    L6MUX21 i2764 (.D0(n3434), .D1(n3435), .SD(rx_data[3]), .Z(n3436));
    LUT4 i23_4_lut (.A(n3436), .B(n3491), .C(rx_data[4]), .D(rx_data[3]), 
         .Z(n12)) /* synthesis lut_function=(!(A (B (C (D))+!B (C))+!A (((D)+!C)+!B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(132[7] 134[5])
    defparam i23_4_lut.init = 16'h0aca;
    L6MUX21 i2779 (.D0(n3449), .D1(n3450), .SD(rx_data[1]), .Z(n3451));
    L6MUX21 i2782 (.D0(n3452), .D1(n3453), .SD(rx_data[1]), .Z(n3454));
    L6MUX21 i2785 (.D0(n3455), .D1(n3456), .SD(rx_data[1]), .Z(n3457));
    L6MUX21 i2788 (.D0(n3458), .D1(n3459), .SD(rx_data[1]), .Z(n3460));
    L6MUX21 i2791 (.D0(n3461), .D1(n3462), .SD(rx_data[1]), .Z(n3463));
    L6MUX21 i2794 (.D0(n3464), .D1(n3465), .SD(rx_data[1]), .Z(n3466));
    PFUMX i2786 (.BLUT(ad_data0[15]), .ALUT(ad_data1[15]), .C0(rx_data[0]), 
          .Z(n3458));
    L6MUX21 i2797 (.D0(n3467), .D1(n3468), .SD(rx_data[1]), .Z(n3469));
    L6MUX21 i2800 (.D0(n3470), .D1(n3471), .SD(rx_data[1]), .Z(n3472));
    L6MUX21 i2803 (.D0(n3473), .D1(n3474), .SD(rx_data[1]), .Z(n3475));
    L6MUX21 i2806 (.D0(n3476), .D1(n3477), .SD(rx_data[1]), .Z(n3478));
    L6MUX21 i2809 (.D0(n3479), .D1(n3480), .SD(rx_data[1]), .Z(n3481));
    L6MUX21 i2812 (.D0(n3482), .D1(n3483), .SD(rx_data[1]), .Z(n3484));
    L6MUX21 i2819 (.D0(n3489), .D1(n3490), .SD(rx_data[2]), .Z(n3491));
    L6MUX21 i2822 (.D0(n3492), .D1(n3493), .SD(rx_data[1]), .Z(n3494));
    L6MUX21 i2825 (.D0(n3495), .D1(n3496), .SD(rx_data[1]), .Z(n3497));
    L6MUX21 i2828 (.D0(n3498), .D1(n3499), .SD(rx_data[1]), .Z(n3500));
    L6MUX21 i2831 (.D0(n3501), .D1(n3502), .SD(rx_data[1]), .Z(n3503));
    L6MUX21 i2762 (.D0(n3430), .D1(n3431), .SD(rx_data[2]), .Z(n3434));
    L6MUX21 i2834 (.D0(n3504), .D1(n3505), .SD(rx_data[1]), .Z(n3506));
    L6MUX21 i2763 (.D0(n3432), .D1(n3433), .SD(rx_data[2]), .Z(n3435));
    L6MUX21 i2767 (.D0(n3437), .D1(n3438), .SD(rx_data[1]), .Z(n3439));
    L6MUX21 i2837 (.D0(n3507), .D1(n3508), .SD(rx_data[1]), .Z(n3509));
    L6MUX21 i2770 (.D0(n3440), .D1(n3441), .SD(rx_data[1]), .Z(n3442));
    L6MUX21 i2840 (.D0(n3510), .D1(n3511), .SD(rx_data[1]), .Z(n3512));
    L6MUX21 i2773 (.D0(n3443), .D1(n3444), .SD(rx_data[1]), .Z(n3445));
    L6MUX21 i2843 (.D0(n3513), .D1(n3514), .SD(rx_data[1]), .Z(n3515));
    L6MUX21 i2776 (.D0(n3446), .D1(n3447), .SD(rx_data[1]), .Z(n3448));
    PFUMX i2783 (.BLUT(ad_data0[1]), .ALUT(ad_data1[1]), .C0(rx_data[0]), 
          .Z(n3455));
    PFUMX i2778 (.BLUT(ad_data2[3]), .ALUT(ad_data3[3]), .C0(rx_data[0]), 
          .Z(n3450));
    PFUMX i2781 (.BLUT(ad_data2[2]), .ALUT(ad_data3[2]), .C0(rx_data[0]), 
          .Z(n3453));
    PFUMX i2784 (.BLUT(ad_data2[1]), .ALUT(ad_data3[1]), .C0(rx_data[0]), 
          .Z(n3456));
    PFUMX i2787 (.BLUT(ad_data2[15]), .ALUT(ad_data3[15]), .C0(rx_data[0]), 
          .Z(n3459));
    PFUMX i2790 (.BLUT(ad_data2[14]), .ALUT(ad_data3[14]), .C0(rx_data[0]), 
          .Z(n3462));
    PFUMX i2793 (.BLUT(ad_data2[13]), .ALUT(ad_data3[13]), .C0(rx_data[0]), 
          .Z(n3465));
    PFUMX i2780 (.BLUT(ad_data0[2]), .ALUT(ad_data1[2]), .C0(rx_data[0]), 
          .Z(n3452));
    LUT4 i4_3_lut (.A(rx_data[5]), .B(n8), .C(rx_data[7]), .Z(clk_in_c_enable_42)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i4_3_lut.init = 16'h0808;
    \divide(WIDTH=10,N=3)  divide_clk (.clk(clk), .n3647(n3647), .clk_N_242(clk_N_242), 
            .pre_clk(pre_clk), .GND_net(GND_net), .n3657(n3657), .\nst_2__N_277[1] (nst_2__N_277[1]), 
            .\cst[2] (cst[2]), .\cst[0] (cst[0]), .y_N_295(y_N_295)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(50[3] 54[2])
    PFUMX i2777 (.BLUT(ad_data0[3]), .ALUT(ad_data1[3]), .C0(rx_data[0]), 
          .Z(n3449));
    LUT4 i3_4_lut (.A(rx_data[4]), .B(rx_data[3]), .C(rx_data[2]), .D(rx_data[6]), 
         .Z(n8)) /* synthesis lut_function=(!(A+(B+(C+!(D))))) */ ;
    defparam i3_4_lut.init = 16'h0100;
    Uart_Bus Uart_Debug (.clk_in_c(clk_in_c), .rs232_tx_c(rs232_tx_c), .\tx_data[0] (tx_data[0]), 
            .\tx_data[4] (tx_data[4]), .n3874(n3874), .rs232_rx_c(rs232_rx_c), 
            .rx_data({rx_data}), .GND_net(GND_net)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(139[1] 146[2])
    auto_rst auto_rst_inst (.rst_n_auto(rst_n_auto), .clk_in_c(clk_in_c), 
            .GND_net(GND_net)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(24[10:56])
    \divide(WIDTH=32,N=12000)  divide_led (.GND_net(GND_net), .led1_c(led1_c), 
            .clk_in_c(clk_in_c), .n3647(n3647)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(36[3] 40[2])
    VHI i3130 (.Z(VCC_net));
    pll_clk pll_clk (.clk_in_c(clk_in_c), .clk(clk), .GND_net(GND_net), 
            .clk_N_242(clk_N_242)) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(28[9:43])
    VLO i1 (.Z(GND_net));
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    \monopulse(N=6)  monopulse_inst (.cst({Open_0, Open_1, cst[0]}), .clk(clk), 
            .y_N_295(y_N_295), .cnt({Open_2, Open_3, cnt_adj_1018[0]}), 
            .n3646(n3646), .\cst[2] (cst[2]), .n3647(n3647), .n3371(n3371), 
            .led1_c(led1_c), .n477(n477), .n3657(n3657), .cnv_c(cnv_c), 
            .\nst_2__N_277[1] (nst_2__N_277[1])) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(76[20] 83[2])
    LUT4 m1_lut (.Z(n3874)) /* synthesis lut_function=1, syn_instantiated=1 */ ;
    defparam m1_lut.init = 16'hffff;
    
endmodule
//
// Verilog Description of module ltc2357_master
//

module ltc2357_master (scki_c, ad_data0, ad_data2, busy_c, pre_clk, 
            scki_N_359, n3647, n416, n3644, pre_clk_enable_5, \send_data_23__N_809[12] , 
            scki_N_353_enable_169, sdo1_c, sdo2_c, sdo3_c, n3874, 
            ad_data1, ad_data3, n3659, n3657, sdo0_c, sdi_c) /* synthesis syn_module_defined=1 */ ;
    output scki_c;
    output [23:0]ad_data0;
    output [23:0]ad_data2;
    input busy_c;
    input pre_clk;
    input scki_N_359;
    input n3647;
    output n416;
    output n3644;
    input pre_clk_enable_5;
    output \send_data_23__N_809[12] ;
    input scki_N_353_enable_169;
    input sdo1_c;
    input sdo2_c;
    input sdo3_c;
    input n3874;
    output [23:0]ad_data1;
    output [23:0]ad_data3;
    output n3659;
    input n3657;
    input sdo0_c;
    output sdi_c;
    
    wire scki_N_353 /* synthesis is_inv_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(145[11:17])
    wire scki_c /* synthesis is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(9[9:13])
    wire pre_clk /* synthesis is_clock=1, SET_AS_NETWORK=pre_clk */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(46[6:13])
    
    wire n1945, n1944, scki_N_353_enable_106, n1972, scki_N_353_enable_1, 
        ad_data0_23__N_314, n1973, n1753, n1752, scki_N_353_enable_219;
    wire [24:0]rec_r1;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(145[20:26])
    
    wire ad_data1_23__N_435, n1820, scki_N_353_enable_2, ad_data2_23__N_576, 
        n1821, n1745, scki_N_353_enable_3, ad_data2_23__N_612, n1744, 
        n1976, scki_N_353_enable_4, ad_data0_23__N_315, n1977, n1632, 
        scki_N_353_enable_5, ad_data2_23__N_577, n1633, n1636, scki_N_353_enable_6, 
        ad_data3_23__N_698, n1637, n1980, scki_N_353_enable_7, ad_data0_23__N_316, 
        n1981, n1984, scki_N_353_enable_8, ad_data0_23__N_317, n1985, 
        n1988, scki_N_353_enable_9, ad_data0_23__N_318, n1989, n1640, 
        scki_N_353_enable_10, ad_data3_23__N_675, n1641, n1661, scki_N_353_enable_11, 
        ad_data3_23__N_739, n1660, ad_data1_23__N_488, n1829, scki_N_353_enable_12, 
        ad_data1_23__N_485, n1828, n1992, scki_N_353_enable_13, ad_data0_23__N_319, 
        n1993, n1996, scki_N_353_enable_14, ad_data0_23__N_320, n1997, 
        ad_data1_23__N_436, ad_data1_23__N_491, ad_data1_23__N_437, n2000, 
        scki_N_353_enable_15, ad_data0_23__N_321, n2001, ad_data1_23__N_494, 
        n2004, scki_N_353_enable_16, ad_data0_23__N_322, n2005, n1824, 
        scki_N_353_enable_17, ad_data1_23__N_433, n1825, ad_data1_23__N_438, 
        ad_data1_23__N_457;
    wire [3:0]n413;
    
    wire scki_N_353_enable_18, ad_data1_23__N_434;
    wire [24:0]rec_r3;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(145[37:43])
    
    wire ad_data3_23__N_778, n1194;
    wire [4:0]scnt;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(56[10:14])
    
    wire n3270, n1652, scki_N_353_enable_19, ad_data3_23__N_678, n1653, 
        ad_data1_23__N_497, scki_N_353_enable_21, n1713, scki_N_353_enable_22, 
        n1712, n1644, scki_N_353_enable_23, ad_data3_23__N_676, n1645, 
        scki_N_353_enable_24, ad_data2_23__N_557, scki_N_356;
    wire [24:0]rec_r2;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(145[29:35])
    
    wire scki_N_353_enable_48, ad_data1_23__N_456, ad_data1_23__N_551, 
        scki_N_353_enable_41, ad_data2_23__N_672, ad_data1_23__N_439, 
        ad_data1_23__N_500, ad_data1_23__N_440, ad_data1_23__N_503, ad_data1_23__N_441, 
        ad_data1_23__N_506, ad_data1_23__N_442, ad_data1_23__N_509, ad_data1_23__N_443, 
        ad_data1_23__N_512, ad_data1_23__N_444, ad_data1_23__N_515, ad_data1_23__N_445, 
        ad_data1_23__N_518, ad_data1_23__N_446, ad_data1_23__N_521, ad_data1_23__N_447, 
        ad_data1_23__N_524, ad_data1_23__N_448, ad_data1_23__N_527, ad_data1_23__N_449, 
        ad_data1_23__N_530, scki_N_353_enable_35, ad_data3_23__N_793, 
        ad_data1_23__N_450, ad_data1_23__N_533, ad_data1_23__N_451, scki_N_353_enable_37, 
        n1889, n1888, scki_N_353_enable_179, n1657, n1656, scki_N_353_enable_45, 
        n1749, n1748, scki_N_353_enable_221, scki_N_353_enable_54, ad_data1_23__N_536, 
        ad_data1_23__N_452, ad_data1_23__N_539, ad_data1_23__N_453, ad_data1_23__N_542, 
        ad_data1_23__N_454, scki_N_353_enable_33, ad_data3_23__N_699, 
        ad_data1_23__N_545, ad_data1_23__N_455, ad_data1_23__N_548;
    wire [24:0]rec_r0;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(145[11:17])
    
    wire ad_data0_23__N_300, ad_data0_23__N_324, ad_data0_23__N_301, ad_data0_23__N_364, 
        ad_data0_23__N_302, n1649, n1648, scki_N_353_enable_135, scki_N_353_enable_43, 
        n1741, n1740, scki_N_353_enable_177, ad_data0_23__N_367, n1785, 
        n1784, scki_N_353_enable_197, n1693, n1692, scki_N_353_enable_205, 
        scki_N_353_enable_288, scki_N_353_enable_220, scki_N_353_enable_287, 
        scki_N_353_enable_202, scki_N_353_enable_286, scki_N_353_enable_285, 
        n1789, n1788, scki_N_353_enable_193, scki_N_353_enable_218, 
        scki_N_353_enable_284, scki_N_353_enable_217, scki_N_353_enable_283, 
        scki_N_353_enable_216, scki_N_353_enable_282, scki_N_353_enable_215, 
        scki_N_353_enable_281, scki_N_353_enable_214, scki_N_353_enable_280, 
        scki_N_353_enable_213, scki_N_353_enable_279, scki_N_353_enable_40, 
        scki_N_353_enable_212, scki_N_353_enable_278, scki_N_353_enable_211, 
        scki_N_353_enable_277, n1737, n1736, scki_N_353_enable_198, 
        scki_N_353_enable_210, scki_N_353_enable_276, scki_N_353_enable_208, 
        scki_N_353_enable_275, scki_N_353_enable_209, scki_N_353_enable_274, 
        scki_N_353_enable_207, scki_N_353_enable_273, scki_N_353_enable_272, 
        scki_N_353_enable_203, scki_N_353_enable_271, scki_N_353_enable_206, 
        scki_N_353_enable_270, scki_N_353_enable_204, scki_N_353_enable_269, 
        scki_N_353_enable_199, scki_N_353_enable_268, scki_N_353_enable_267, 
        scki_N_353_enable_201, scki_N_353_enable_266, scki_N_353_enable_200, 
        scki_N_353_enable_265, scki_N_353_enable_264, scki_N_353_enable_196, 
        scki_N_353_enable_263, scki_N_353_enable_195, scki_N_353_enable_262, 
        scki_N_353_enable_194, scki_N_353_enable_261, n1681, n1680, 
        scki_N_353_enable_178, scki_N_353_enable_260, scki_N_353_enable_192, 
        scki_N_353_enable_259, n1777, n1776, scki_N_353_enable_191, 
        scki_N_353_enable_258, scki_N_353_enable_257, n1685, n1684, 
        scki_N_353_enable_190, scki_N_353_enable_256, scki_N_353_enable_189, 
        scki_N_353_enable_255, scki_N_353_enable_187, scki_N_353_enable_254, 
        scki_N_353_enable_188, scki_N_353_enable_253, scki_N_353_enable_186, 
        scki_N_353_enable_252, scki_N_353_enable_27, ad_data0_23__N_427, 
        scki_N_353_enable_184, scki_N_353_enable_251, n1664, scki_N_353_enable_28, 
        ad_data3_23__N_681, n1665, scki_N_353_enable_29, ad_data3_23__N_742, 
        scki_N_353_enable_42, scki_N_353_enable_183, scki_N_353_enable_250, 
        scki_N_353_enable_185, scki_N_353_enable_249, ad_data0_23__N_385, 
        scki_N_353_enable_30, ad_data0_23__N_424, scki_N_353_enable_182, 
        scki_N_353_enable_248, scki_N_353_enable_180, scki_N_353_enable_247, 
        scki_N_353_enable_181, scki_N_353_enable_246, n426, scki_N_353_enable_245, 
        ad_data0_23__N_303, scki_N_353_enable_244, scki_N_353_enable_31, 
        ad_data0_23__N_421, ad_data0_23__N_370, scki_N_353_enable_175, 
        scki_N_353_enable_243, ad_data0_23__N_304, scki_N_353_enable_32, 
        ad_data0_23__N_418, scki_N_353_enable_176, scki_N_353_enable_242, 
        scki_N_353_enable_174, scki_N_353_enable_241, scki_N_353_enable_240, 
        scki_N_353_enable_34, ad_data0_23__N_415, scki_N_353_enable_239, 
        ad_data0_23__N_373, scki_N_353_enable_36, ad_data0_23__N_412, 
        scki_N_353_enable_173, scki_N_353_enable_238, ad_data0_23__N_409, 
        scki_N_353_enable_171, scki_N_353_enable_237, ad_data0_23__N_305, 
        n1625, scki_N_353_enable_38, ad_data0_23__N_430, n1624, scki_N_353_enable_172, 
        scki_N_353_enable_236, scki_N_353_enable_39, ad_data2_23__N_669, 
        ad_data3_23__N_727, scki_N_353_enable_170, scki_N_353_enable_235, 
        scki_N_353_enable_146, scki_N_353_enable_234, scki_N_353_enable_145, 
        scki_N_353_enable_233, scki_N_353_enable_144, scki_N_353_enable_232, 
        scki_N_353_enable_231, ad_data0_23__N_406, scki_N_353_enable_111, 
        scki_N_353_enable_230, scki_N_353_enable_110, scki_N_353_enable_229, 
        scki_N_353_enable_109, scki_N_353_enable_228, ad_data0_23__N_403, 
        scki_N_353_enable_108, scki_N_353_enable_227, scki_N_353_enable_107, 
        scki_N_353_enable_226, ad_data0_23__N_376, scki_N_353_enable_225, 
        n1969, scki_N_353_enable_44, ad_data0_23__N_400, n1968, scki_N_353_enable_104, 
        scki_N_353_enable_224;
    wire [23:0]send_data;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(99[12:21])
    
    wire n3241, pre_clk_enable_6, scki_N_353_enable_57, scki_N_353_enable_223, 
        scki_N_353_enable_56, scki_N_353_enable_222, n3651;
    wire [4:0]n1085;
    
    wire ad_data0_23__N_306, ad_data3_23__N_736, busy_diff1, n1965, 
        scki_N_353_enable_46, ad_data0_23__N_397, n1964, n1961, scki_N_353_enable_47, 
        ad_data0_23__N_394, n1960, ad_data0_23__N_379, ad_data0_23__N_307, 
        n1629, n1628, n1817, scki_N_353_enable_49, ad_data2_23__N_666, 
        n1816, ad_data0_23__N_382, n1813, scki_N_353_enable_50, ad_data2_23__N_663, 
        n1812, n1809, scki_N_353_enable_51, ad_data2_23__N_660, n1808, 
        n1957, scki_N_353_enable_52, ad_data0_23__N_391, n1956, ad_data3_23__N_677, 
        ad_data3_23__N_730, ad_data3_23__N_733, ad_data3_23__N_679, ad_data0_23__N_308, 
        scki_N_353_enable_55, ad_data0_23__N_313, ad_data3_23__N_680, 
        ad_data3_23__N_682, ad_data3_23__N_745, n1805, ad_data2_23__N_657, 
        n1804, n1953, ad_data0_23__N_388, n1952, ad_data3_23__N_683, 
        ad_data3_23__N_748, ad_data3_23__N_684, ad_data3_23__N_751, ad_data3_23__N_685, 
        ad_data3_23__N_754, ad_data3_23__N_686, ad_data3_23__N_757, ad_data3_23__N_687, 
        ad_data3_23__N_760, n1733, n1732, ad_data3_23__N_688, ad_data3_23__N_763, 
        ad_data0_23__N_323, ad_data3_23__N_689, ad_data3_23__N_766, ad_data3_23__N_690, 
        ad_data3_23__N_769, busy_diff2, ad_data3_23__N_691, ad_data3_23__N_772, 
        n1917, n1916, ad_data3_23__N_692, ad_data3_23__N_775, ad_data3_23__N_693, 
        ad_data3_23__N_694, ad_data3_23__N_781, ad_data3_23__N_695, ad_data3_23__N_784, 
        ad_data3_23__N_696, ad_data3_23__N_787, ad_data3_23__N_697, ad_data3_23__N_790, 
        ad_data2_23__N_554, ad_data2_23__N_578, ad_data2_23__N_555, ad_data2_23__N_606, 
        ad_data2_23__N_556, ad_data2_23__N_609, ad_data2_23__N_558, ad_data2_23__N_615, 
        ad_data2_23__N_559, ad_data2_23__N_618, ad_data2_23__N_560, ad_data2_23__N_621, 
        ad_data2_23__N_561, ad_data2_23__N_624, ad_data2_23__N_562, ad_data2_23__N_627, 
        ad_data2_23__N_563, ad_data2_23__N_630, ad_data2_23__N_564, ad_data2_23__N_633, 
        ad_data2_23__N_565, ad_data2_23__N_636, ad_data2_23__N_566, ad_data2_23__N_639, 
        ad_data2_23__N_567, ad_data2_23__N_642, ad_data2_23__N_568, ad_data2_23__N_645, 
        ad_data2_23__N_569, ad_data2_23__N_648, ad_data2_23__N_570, ad_data2_23__N_651, 
        ad_data2_23__N_571, ad_data2_23__N_654, ad_data2_23__N_572, ad_data2_23__N_573, 
        ad_data2_23__N_574, ad_data2_23__N_575, ad_data0_23__N_309, ad_data0_23__N_310, 
        ad_data0_23__N_311, ad_data0_23__N_312, scki_N_353_enable_143, 
        scki_N_353_enable_142, scki_N_353_enable_141, scki_N_353_enable_140, 
        scki_N_353_enable_139, scki_N_353_enable_138, scki_N_353_enable_137, 
        scki_N_353_enable_136, n1905, n1904, n3643, n1909, n1908, 
        n1697, n1696, n1773, n1772, n1677, n1676, n1769, n1768, 
        n1673, n1672, n1765, n1764, n1729, n1728, n1725, n1724, 
        n1721, n1720, n1617, n1949, n1948, scki_N_353_enable_105, 
        n1913, n1912, n1941, n1940, n1937, n1936, n1857, n1856, 
        n1933, n1932, n1929, n1928, n1925, n1924, n427, n1861, 
        n1860, n3658, n1921, n1920, pre_clk_enable_17, n1801, n1800, 
        n1901, n1900, n1897, n1896, n1797, n1796, n1865, n1864, 
        n1893, n1892, n1709, n1708, n1869, n1868, n1885, n1884, 
        n1881, n1880, n1705, n1704, n1793, n1792, n1877, n1876, 
        n1873, n1872, n1781, n1780, n1689, n1688, n1853, n1852, 
        n1701, n1700, n1849, n1848, n1845, n1844, n1669, n1668, 
        n1841, n1840, n1837, n1836, n1833, n1832, n1761, n1760, 
        n1757, n1756, n1717, n1716;
    
    INV i3132 (.A(scki_c), .Z(scki_N_353));
    LUT4 i1314_3_lut (.A(n1945), .B(n1944), .C(scki_N_353_enable_106), 
         .Z(ad_data0[16])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1314_3_lut.init = 16'hcaca;
    FD1P3BX ad_data0_i9_1340_1341_set (.D(n1973), .SP(scki_N_353_enable_1), 
            .CK(scki_N_353), .PD(ad_data0_23__N_314), .Q(n1972)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i9_1340_1341_set.GSR = "DISABLED";
    LUT4 i1122_3_lut (.A(n1753), .B(n1752), .C(scki_N_353_enable_219), 
         .Z(ad_data2[18])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1122_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_421_2_lut (.A(busy_c), .B(rec_r1[21]), .Z(ad_data1_23__N_435)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_421_2_lut.init = 16'h8888;
    FD1P3BX ad_data2_i1_1188_1189_set (.D(n1821), .SP(scki_N_353_enable_2), 
            .CK(scki_N_353), .PD(ad_data2_23__N_576), .Q(n1820)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i1_1188_1189_set.GSR = "DISABLED";
    FD1P3DX ad_data2_i20_1112_1113_reset (.D(n1744), .SP(scki_N_353_enable_3), 
            .CK(scki_N_353), .CD(ad_data2_23__N_612), .Q(n1745)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i20_1112_1113_reset.GSR = "DISABLED";
    FD1P3BX ad_data0_i8_1344_1345_set (.D(n1977), .SP(scki_N_353_enable_4), 
            .CK(scki_N_353), .PD(ad_data0_23__N_315), .Q(n1976)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i8_1344_1345_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i0_1000_1001_set (.D(n1633), .SP(scki_N_353_enable_5), 
            .CK(scki_N_353), .PD(ad_data2_23__N_577), .Q(n1632)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i0_1000_1001_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i0_1004_1005_set (.D(n1637), .SP(scki_N_353_enable_6), 
            .CK(scki_N_353), .PD(ad_data3_23__N_698), .Q(n1636)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i0_1004_1005_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i7_1348_1349_set (.D(n1981), .SP(scki_N_353_enable_7), 
            .CK(scki_N_353), .PD(ad_data0_23__N_316), .Q(n1980)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i7_1348_1349_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i6_1352_1353_set (.D(n1985), .SP(scki_N_353_enable_8), 
            .CK(scki_N_353), .PD(ad_data0_23__N_317), .Q(n1984)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i6_1352_1353_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i5_1356_1357_set (.D(n1989), .SP(scki_N_353_enable_9), 
            .CK(scki_N_353), .PD(ad_data0_23__N_318), .Q(n1988)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i5_1356_1357_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i23_1008_1009_set (.D(n1641), .SP(scki_N_353_enable_10), 
            .CK(scki_N_353), .PD(ad_data3_23__N_675), .Q(n1640)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i23_1008_1009_set.GSR = "DISABLED";
    FD1P3DX ad_data3_i18_1028_1029_reset (.D(n1660), .SP(scki_N_353_enable_11), 
            .CK(scki_N_353), .CD(ad_data3_23__N_739), .Q(n1661)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i18_1028_1029_reset.GSR = "DISABLED";
    LUT4 busy_I_0_445_2_lut (.A(busy_c), .B(rec_r1[21]), .Z(ad_data1_23__N_488)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_445_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i22_1196_1197_reset (.D(n1828), .SP(scki_N_353_enable_12), 
            .CK(scki_N_353), .CD(ad_data1_23__N_485), .Q(n1829)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i22_1196_1197_reset.GSR = "DISABLED";
    FD1P3BX ad_data0_i4_1360_1361_set (.D(n1993), .SP(scki_N_353_enable_13), 
            .CK(scki_N_353), .PD(ad_data0_23__N_319), .Q(n1992)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i4_1360_1361_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i3_1364_1365_set (.D(n1997), .SP(scki_N_353_enable_14), 
            .CK(scki_N_353), .PD(ad_data0_23__N_320), .Q(n1996)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i3_1364_1365_set.GSR = "DISABLED";
    LUT4 busy_I_0_422_2_lut (.A(busy_c), .B(rec_r1[20]), .Z(ad_data1_23__N_436)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_422_2_lut.init = 16'h8888;
    LUT4 busy_I_0_446_2_lut (.A(busy_c), .B(rec_r1[20]), .Z(ad_data1_23__N_491)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_446_2_lut.init = 16'h2222;
    LUT4 busy_I_0_423_2_lut (.A(busy_c), .B(rec_r1[19]), .Z(ad_data1_23__N_437)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_423_2_lut.init = 16'h8888;
    FD1P3BX ad_data0_i2_1368_1369_set (.D(n2001), .SP(scki_N_353_enable_15), 
            .CK(scki_N_353), .PD(ad_data0_23__N_321), .Q(n2000)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i2_1368_1369_set.GSR = "DISABLED";
    LUT4 busy_I_0_447_2_lut (.A(busy_c), .B(rec_r1[19]), .Z(ad_data1_23__N_494)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_447_2_lut.init = 16'h2222;
    FD1P3BX ad_data0_i1_1372_1373_set (.D(n2005), .SP(scki_N_353_enable_16), 
            .CK(scki_N_353), .PD(ad_data0_23__N_322), .Q(n2004)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i1_1372_1373_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i23_1192_1193_set (.D(n1825), .SP(scki_N_353_enable_17), 
            .CK(scki_N_353), .PD(ad_data1_23__N_433), .Q(n1824)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i23_1192_1193_set.GSR = "DISABLED";
    LUT4 busy_I_0_424_2_lut (.A(busy_c), .B(rec_r1[18]), .Z(ad_data1_23__N_438)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_424_2_lut.init = 16'h8888;
    LUT4 busy_I_0_443_2_lut (.A(busy_c), .B(rec_r1[23]), .Z(ad_data1_23__N_457)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_443_2_lut.init = 16'h2222;
    FD1S3AX cst_FSM_i3 (.D(scki_N_359), .CK(pre_clk), .Q(n413[3]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam cst_FSM_i3.GSR = "DISABLED";
    FD1P3BX ad_data1_i22_1196_1197_set (.D(n1829), .SP(scki_N_353_enable_18), 
            .CK(scki_N_353), .PD(ad_data1_23__N_434), .Q(n1828)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i22_1196_1197_set.GSR = "DISABLED";
    LUT4 busy_I_0_557_2_lut (.A(busy_c), .B(rec_r3[5]), .Z(ad_data3_23__N_778)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_557_2_lut.init = 16'h2222;
    FD1S3IX cst_FSM_i2 (.D(n1194), .CK(pre_clk), .CD(n3647), .Q(n413[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam cst_FSM_i2.GSR = "DISABLED";
    FD1S3AX cst_FSM_i1 (.D(n3644), .CK(pre_clk), .Q(n416));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam cst_FSM_i1.GSR = "DISABLED";
    FD1P3IX scnt__i0 (.D(n3270), .SP(pre_clk_enable_5), .CD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(scnt[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scnt__i0.GSR = "DISABLED";
    FD1P3BX ad_data3_i20_1020_1021_set (.D(n1653), .SP(scki_N_353_enable_19), 
            .CK(scki_N_353), .PD(ad_data3_23__N_678), .Q(n1652)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i20_1020_1021_set.GSR = "DISABLED";
    FD1P3AX rec_r1__i1 (.D(sdo1_c), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i1.GSR = "DISABLED";
    LUT4 busy_I_0_448_2_lut (.A(busy_c), .B(rec_r1[18]), .Z(ad_data1_23__N_497)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_448_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i23_1192_1193_reset (.D(n1824), .SP(scki_N_353_enable_21), 
            .CK(scki_N_353), .CD(ad_data1_23__N_457), .Q(n1825)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i23_1192_1193_reset.GSR = "DISABLED";
    FD1P3DX ad_data3_i5_1080_1081_reset (.D(n1712), .SP(scki_N_353_enable_22), 
            .CK(scki_N_353), .CD(ad_data3_23__N_778), .Q(n1713)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i5_1080_1081_reset.GSR = "DISABLED";
    FD1P3BX ad_data3_i22_1012_1013_set (.D(n1645), .SP(scki_N_353_enable_23), 
            .CK(scki_N_353), .PD(ad_data3_23__N_676), .Q(n1644)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i22_1012_1013_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i20_1112_1113_set (.D(n1745), .SP(scki_N_353_enable_24), 
            .CK(scki_N_353), .PD(ad_data2_23__N_557), .Q(n1744)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i20_1112_1113_set.GSR = "DISABLED";
    FD1S3JX scki_67 (.D(scki_N_356), .CK(pre_clk), .PD(\send_data_23__N_809[12] ), 
            .Q(scki_c)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scki_67.GSR = "DISABLED";
    FD1P3AX rec_r2__i1 (.D(sdo2_c), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i1.GSR = "DISABLED";
    FD1P3AX rec_r3__i1 (.D(sdo3_c), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i1.GSR = "DISABLED";
    FD1S1D i995 (.D(n3874), .CK(ad_data1_23__N_456), .CD(ad_data1_23__N_551), 
           .Q(scki_N_353_enable_48));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i995.GSR = "DISABLED";
    FD1S1D i999 (.D(n3874), .CK(ad_data2_23__N_577), .CD(ad_data2_23__N_672), 
           .Q(scki_N_353_enable_41));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i999.GSR = "DISABLED";
    LUT4 busy_I_0_425_2_lut (.A(busy_c), .B(rec_r1[17]), .Z(ad_data1_23__N_439)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_425_2_lut.init = 16'h8888;
    LUT4 busy_I_0_449_2_lut (.A(busy_c), .B(rec_r1[17]), .Z(ad_data1_23__N_500)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_449_2_lut.init = 16'h2222;
    LUT4 busy_I_0_426_2_lut (.A(busy_c), .B(rec_r1[16]), .Z(ad_data1_23__N_440)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_426_2_lut.init = 16'h8888;
    LUT4 busy_I_0_450_2_lut (.A(busy_c), .B(rec_r1[16]), .Z(ad_data1_23__N_503)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_450_2_lut.init = 16'h2222;
    LUT4 busy_I_0_427_2_lut (.A(busy_c), .B(rec_r1[15]), .Z(ad_data1_23__N_441)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_427_2_lut.init = 16'h8888;
    LUT4 busy_I_0_451_2_lut (.A(busy_c), .B(rec_r1[15]), .Z(ad_data1_23__N_506)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_451_2_lut.init = 16'h2222;
    LUT4 busy_I_0_428_2_lut (.A(busy_c), .B(rec_r1[14]), .Z(ad_data1_23__N_442)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_428_2_lut.init = 16'h8888;
    LUT4 busy_I_0_452_2_lut (.A(busy_c), .B(rec_r1[14]), .Z(ad_data1_23__N_509)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_452_2_lut.init = 16'h2222;
    LUT4 busy_I_0_429_2_lut (.A(busy_c), .B(rec_r1[13]), .Z(ad_data1_23__N_443)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_429_2_lut.init = 16'h8888;
    LUT4 busy_I_0_453_2_lut (.A(busy_c), .B(rec_r1[13]), .Z(ad_data1_23__N_512)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_453_2_lut.init = 16'h2222;
    LUT4 busy_I_0_430_2_lut (.A(busy_c), .B(rec_r1[12]), .Z(ad_data1_23__N_444)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_430_2_lut.init = 16'h8888;
    LUT4 busy_I_0_454_2_lut (.A(busy_c), .B(rec_r1[12]), .Z(ad_data1_23__N_515)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_454_2_lut.init = 16'h2222;
    LUT4 busy_I_0_431_2_lut (.A(busy_c), .B(rec_r1[11]), .Z(ad_data1_23__N_445)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_431_2_lut.init = 16'h8888;
    LUT4 busy_I_0_455_2_lut (.A(busy_c), .B(rec_r1[11]), .Z(ad_data1_23__N_518)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_455_2_lut.init = 16'h2222;
    LUT4 busy_I_0_432_2_lut (.A(busy_c), .B(rec_r1[10]), .Z(ad_data1_23__N_446)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_432_2_lut.init = 16'h8888;
    LUT4 busy_I_0_456_2_lut (.A(busy_c), .B(rec_r1[10]), .Z(ad_data1_23__N_521)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_456_2_lut.init = 16'h2222;
    LUT4 busy_I_0_433_2_lut (.A(busy_c), .B(rec_r1[9]), .Z(ad_data1_23__N_447)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_433_2_lut.init = 16'h8888;
    LUT4 busy_I_0_457_2_lut (.A(busy_c), .B(rec_r1[9]), .Z(ad_data1_23__N_524)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_457_2_lut.init = 16'h2222;
    LUT4 busy_I_0_434_2_lut (.A(busy_c), .B(rec_r1[8]), .Z(ad_data1_23__N_448)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_434_2_lut.init = 16'h8888;
    LUT4 busy_I_0_458_2_lut (.A(busy_c), .B(rec_r1[8]), .Z(ad_data1_23__N_527)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_458_2_lut.init = 16'h2222;
    LUT4 busy_I_0_435_2_lut (.A(busy_c), .B(rec_r1[7]), .Z(ad_data1_23__N_449)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_435_2_lut.init = 16'h8888;
    LUT4 busy_I_0_459_2_lut (.A(busy_c), .B(rec_r1[7]), .Z(ad_data1_23__N_530)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_459_2_lut.init = 16'h2222;
    FD1S1D i1003 (.D(n3874), .CK(ad_data3_23__N_698), .CD(ad_data3_23__N_793), 
           .Q(scki_N_353_enable_35));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1003.GSR = "DISABLED";
    LUT4 busy_I_0_436_2_lut (.A(busy_c), .B(rec_r1[6]), .Z(ad_data1_23__N_450)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_436_2_lut.init = 16'h8888;
    LUT4 busy_I_0_460_2_lut (.A(busy_c), .B(rec_r1[6]), .Z(ad_data1_23__N_533)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_460_2_lut.init = 16'h2222;
    LUT4 busy_I_0_437_2_lut (.A(busy_c), .B(rec_r1[5]), .Z(ad_data1_23__N_451)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_437_2_lut.init = 16'h8888;
    LUT4 i1350_3_lut (.A(n1981), .B(n1980), .C(scki_N_353_enable_37), 
         .Z(ad_data0[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1350_3_lut.init = 16'hcaca;
    LUT4 i1258_3_lut (.A(n1889), .B(n1888), .C(scki_N_353_enable_179), 
         .Z(ad_data1[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1258_3_lut.init = 16'hcaca;
    LUT4 i1026_3_lut (.A(n1657), .B(n1656), .C(scki_N_353_enable_45), 
         .Z(ad_data3[19])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1026_3_lut.init = 16'hcaca;
    LUT4 i1118_3_lut (.A(n1749), .B(n1748), .C(scki_N_353_enable_221), 
         .Z(ad_data2[19])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1118_3_lut.init = 16'hcaca;
    LUT4 i1022_3_lut (.A(n1653), .B(n1652), .C(scki_N_353_enable_54), 
         .Z(ad_data3[20])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1022_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_461_2_lut (.A(busy_c), .B(rec_r1[5]), .Z(ad_data1_23__N_536)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_461_2_lut.init = 16'h2222;
    LUT4 busy_I_0_438_2_lut (.A(busy_c), .B(rec_r1[4]), .Z(ad_data1_23__N_452)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_438_2_lut.init = 16'h8888;
    LUT4 busy_I_0_462_2_lut (.A(busy_c), .B(rec_r1[4]), .Z(ad_data1_23__N_539)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_462_2_lut.init = 16'h2222;
    LUT4 busy_I_0_439_2_lut (.A(busy_c), .B(rec_r1[3]), .Z(ad_data1_23__N_453)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_439_2_lut.init = 16'h8888;
    LUT4 i1114_3_lut (.A(n1745), .B(n1744), .C(scki_N_353_enable_3), .Z(ad_data2[20])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1114_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_463_2_lut (.A(busy_c), .B(rec_r1[3]), .Z(ad_data1_23__N_542)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_463_2_lut.init = 16'h2222;
    LUT4 busy_I_0_440_2_lut (.A(busy_c), .B(rec_r1[2]), .Z(ad_data1_23__N_454)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_440_2_lut.init = 16'h8888;
    FD1S1D i1007 (.D(n3874), .CK(ad_data3_23__N_675), .CD(ad_data3_23__N_699), 
           .Q(scki_N_353_enable_33));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1007.GSR = "DISABLED";
    LUT4 busy_I_0_464_2_lut (.A(busy_c), .B(rec_r1[2]), .Z(ad_data1_23__N_545)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_464_2_lut.init = 16'h2222;
    LUT4 busy_I_0_441_2_lut (.A(busy_c), .B(rec_r1[1]), .Z(ad_data1_23__N_455)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_441_2_lut.init = 16'h8888;
    LUT4 busy_I_0_465_2_lut (.A(busy_c), .B(rec_r1[1]), .Z(ad_data1_23__N_548)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_465_2_lut.init = 16'h2222;
    LUT4 busy_I_0_368_2_lut (.A(busy_c), .B(rec_r0[23]), .Z(ad_data0_23__N_300)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_368_2_lut.init = 16'h8888;
    LUT4 busy_I_0_395_2_lut (.A(busy_c), .B(rec_r0[23]), .Z(ad_data0_23__N_324)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_395_2_lut.init = 16'h2222;
    LUT4 busy_I_0_369_2_lut (.A(busy_c), .B(rec_r0[22]), .Z(ad_data0_23__N_301)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_369_2_lut.init = 16'h8888;
    LUT4 busy_I_0_396_2_lut (.A(busy_c), .B(rec_r0[22]), .Z(ad_data0_23__N_364)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_396_2_lut.init = 16'h2222;
    LUT4 busy_I_0_370_2_lut (.A(busy_c), .B(rec_r0[21]), .Z(ad_data0_23__N_302)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_370_2_lut.init = 16'h8888;
    LUT4 i1380_1_lut (.A(scki_N_353_enable_21), .Z(scki_N_353_enable_17)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1380_1_lut.init = 16'h5555;
    LUT4 i1018_3_lut (.A(n1649), .B(n1648), .C(scki_N_353_enable_135), 
         .Z(ad_data3[21])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1018_3_lut.init = 16'hcaca;
    LUT4 i1395_1_lut (.A(scki_N_353_enable_43), .Z(scki_N_353_enable_1)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1395_1_lut.init = 16'h5555;
    LUT4 i1110_3_lut (.A(n1741), .B(n1740), .C(scki_N_353_enable_177), 
         .Z(ad_data2[21])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1110_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_419_2_lut (.A(busy_c), .B(rec_r1[23]), .Z(ad_data1_23__N_433)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_419_2_lut.init = 16'h8888;
    LUT4 busy_I_0_397_2_lut (.A(busy_c), .B(rec_r0[21]), .Z(ad_data0_23__N_367)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_397_2_lut.init = 16'h2222;
    LUT4 i1154_3_lut (.A(n1785), .B(n1784), .C(scki_N_353_enable_197), 
         .Z(ad_data2[10])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1154_3_lut.init = 16'hcaca;
    LUT4 i1379_1_lut (.A(scki_N_353_enable_12), .Z(scki_N_353_enable_18)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1379_1_lut.init = 16'h5555;
    LUT4 i1062_3_lut (.A(n1693), .B(n1692), .C(scki_N_353_enable_205), 
         .Z(ad_data3[10])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1062_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_420_2_lut (.A(busy_c), .B(rec_r1[22]), .Z(ad_data1_23__N_434)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_420_2_lut.init = 16'h8888;
    LUT4 i1499_1_lut (.A(scki_N_353_enable_221), .Z(scki_N_353_enable_288)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1499_1_lut.init = 16'h5555;
    LUT4 i1498_1_lut (.A(scki_N_353_enable_220), .Z(scki_N_353_enable_287)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1498_1_lut.init = 16'h5555;
    LUT4 i1497_1_lut (.A(scki_N_353_enable_202), .Z(scki_N_353_enable_286)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1497_1_lut.init = 16'h5555;
    LUT4 i1496_1_lut (.A(scki_N_353_enable_219), .Z(scki_N_353_enable_285)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1496_1_lut.init = 16'h5555;
    LUT4 i1158_3_lut (.A(n1789), .B(n1788), .C(scki_N_353_enable_193), 
         .Z(ad_data2[9])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1158_3_lut.init = 16'hcaca;
    LUT4 i1495_1_lut (.A(scki_N_353_enable_218), .Z(scki_N_353_enable_284)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1495_1_lut.init = 16'h5555;
    LUT4 i1494_1_lut (.A(scki_N_353_enable_217), .Z(scki_N_353_enable_283)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1494_1_lut.init = 16'h5555;
    LUT4 i1493_1_lut (.A(scki_N_353_enable_216), .Z(scki_N_353_enable_282)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1493_1_lut.init = 16'h5555;
    LUT4 i1492_1_lut (.A(scki_N_353_enable_215), .Z(scki_N_353_enable_281)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1492_1_lut.init = 16'h5555;
    LUT4 i1491_1_lut (.A(scki_N_353_enable_214), .Z(scki_N_353_enable_280)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1491_1_lut.init = 16'h5555;
    LUT4 i1490_1_lut (.A(scki_N_353_enable_213), .Z(scki_N_353_enable_279)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1490_1_lut.init = 16'h5555;
    LUT4 i1014_3_lut (.A(n1645), .B(n1644), .C(scki_N_353_enable_40), 
         .Z(ad_data3[22])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1014_3_lut.init = 16'hcaca;
    LUT4 i1489_1_lut (.A(scki_N_353_enable_212), .Z(scki_N_353_enable_278)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1489_1_lut.init = 16'h5555;
    LUT4 i1488_1_lut (.A(scki_N_353_enable_211), .Z(scki_N_353_enable_277)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1488_1_lut.init = 16'h5555;
    LUT4 i1106_3_lut (.A(n1737), .B(n1736), .C(scki_N_353_enable_198), 
         .Z(ad_data2[22])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1106_3_lut.init = 16'hcaca;
    LUT4 i1487_1_lut (.A(scki_N_353_enable_210), .Z(scki_N_353_enable_276)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1487_1_lut.init = 16'h5555;
    LUT4 i1486_1_lut (.A(scki_N_353_enable_208), .Z(scki_N_353_enable_275)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1486_1_lut.init = 16'h5555;
    LUT4 i1485_1_lut (.A(scki_N_353_enable_209), .Z(scki_N_353_enable_274)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1485_1_lut.init = 16'h5555;
    LUT4 i1484_1_lut (.A(scki_N_353_enable_207), .Z(scki_N_353_enable_273)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1484_1_lut.init = 16'h5555;
    LUT4 i1483_1_lut (.A(scki_N_353_enable_205), .Z(scki_N_353_enable_272)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1483_1_lut.init = 16'h5555;
    LUT4 i1482_1_lut (.A(scki_N_353_enable_203), .Z(scki_N_353_enable_271)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1482_1_lut.init = 16'h5555;
    LUT4 i1481_1_lut (.A(scki_N_353_enable_206), .Z(scki_N_353_enable_270)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1481_1_lut.init = 16'h5555;
    LUT4 i1480_1_lut (.A(scki_N_353_enable_204), .Z(scki_N_353_enable_269)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1480_1_lut.init = 16'h5555;
    LUT4 i1479_1_lut (.A(scki_N_353_enable_199), .Z(scki_N_353_enable_268)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1479_1_lut.init = 16'h5555;
    LUT4 i1478_1_lut (.A(scki_N_353_enable_198), .Z(scki_N_353_enable_267)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1478_1_lut.init = 16'h5555;
    LUT4 i1477_1_lut (.A(scki_N_353_enable_201), .Z(scki_N_353_enable_266)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1477_1_lut.init = 16'h5555;
    LUT4 i1476_1_lut (.A(scki_N_353_enable_200), .Z(scki_N_353_enable_265)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1476_1_lut.init = 16'h5555;
    LUT4 i1475_1_lut (.A(scki_N_353_enable_197), .Z(scki_N_353_enable_264)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1475_1_lut.init = 16'h5555;
    LUT4 i1474_1_lut (.A(scki_N_353_enable_196), .Z(scki_N_353_enable_263)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1474_1_lut.init = 16'h5555;
    LUT4 i1473_1_lut (.A(scki_N_353_enable_195), .Z(scki_N_353_enable_262)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1473_1_lut.init = 16'h5555;
    LUT4 i1472_1_lut (.A(scki_N_353_enable_194), .Z(scki_N_353_enable_261)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1472_1_lut.init = 16'h5555;
    LUT4 i1050_3_lut (.A(n1681), .B(n1680), .C(scki_N_353_enable_178), 
         .Z(ad_data3[13])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1050_3_lut.init = 16'hcaca;
    LUT4 i1471_1_lut (.A(scki_N_353_enable_193), .Z(scki_N_353_enable_260)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1471_1_lut.init = 16'h5555;
    LUT4 i1470_1_lut (.A(scki_N_353_enable_192), .Z(scki_N_353_enable_259)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1470_1_lut.init = 16'h5555;
    LUT4 i1146_3_lut (.A(n1777), .B(n1776), .C(scki_N_353_enable_204), 
         .Z(ad_data2[12])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1146_3_lut.init = 16'hcaca;
    LUT4 i1469_1_lut (.A(scki_N_353_enable_191), .Z(scki_N_353_enable_258)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1469_1_lut.init = 16'h5555;
    LUT4 i1468_1_lut (.A(scki_N_353_enable_177), .Z(scki_N_353_enable_257)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1468_1_lut.init = 16'h5555;
    LUT4 i1054_3_lut (.A(n1685), .B(n1684), .C(scki_N_353_enable_212), 
         .Z(ad_data3[12])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1054_3_lut.init = 16'hcaca;
    LUT4 i1467_1_lut (.A(scki_N_353_enable_190), .Z(scki_N_353_enable_256)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1467_1_lut.init = 16'h5555;
    LUT4 i1466_1_lut (.A(scki_N_353_enable_189), .Z(scki_N_353_enable_255)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1466_1_lut.init = 16'h5555;
    LUT4 i1465_1_lut (.A(scki_N_353_enable_187), .Z(scki_N_353_enable_254)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1465_1_lut.init = 16'h5555;
    LUT4 i1464_1_lut (.A(scki_N_353_enable_188), .Z(scki_N_353_enable_253)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1464_1_lut.init = 16'h5555;
    LUT4 i1463_1_lut (.A(scki_N_353_enable_186), .Z(scki_N_353_enable_252)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1463_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i1_1372_1373_reset (.D(n2004), .SP(scki_N_353_enable_27), 
            .CK(scki_N_353), .CD(ad_data0_23__N_427), .Q(n2005)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i1_1372_1373_reset.GSR = "DISABLED";
    LUT4 i1462_1_lut (.A(scki_N_353_enable_184), .Z(scki_N_353_enable_251)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1462_1_lut.init = 16'h5555;
    FD1P3BX ad_data3_i17_1032_1033_set (.D(n1665), .SP(scki_N_353_enable_28), 
            .CK(scki_N_353), .PD(ad_data3_23__N_681), .Q(n1664)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i17_1032_1033_set.GSR = "DISABLED";
    LUT4 i1377_1_lut (.A(scki_N_353_enable_40), .Z(scki_N_353_enable_23)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1377_1_lut.init = 16'h5555;
    FD1P3DX ad_data3_i17_1032_1033_reset (.D(n1664), .SP(scki_N_353_enable_29), 
            .CK(scki_N_353), .CD(ad_data3_23__N_742), .Q(n1665)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i17_1032_1033_reset.GSR = "DISABLED";
    LUT4 i1393_1_lut (.A(scki_N_353_enable_42), .Z(scki_N_353_enable_4)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1393_1_lut.init = 16'h5555;
    LUT4 i1461_1_lut (.A(scki_N_353_enable_183), .Z(scki_N_353_enable_250)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1461_1_lut.init = 16'h5555;
    LUT4 i1010_3_lut (.A(n1641), .B(n1640), .C(scki_N_353_enable_33), 
         .Z(ad_data3[23])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1010_3_lut.init = 16'hcaca;
    LUT4 i1460_1_lut (.A(scki_N_353_enable_185), .Z(scki_N_353_enable_249)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1460_1_lut.init = 16'h5555;
    LUT4 busy_I_0_403_2_lut (.A(busy_c), .B(rec_r0[15]), .Z(ad_data0_23__N_385)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_403_2_lut.init = 16'h2222;
    LUT4 busy_I_0_382_2_lut (.A(busy_c), .B(rec_r0[9]), .Z(ad_data0_23__N_314)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_382_2_lut.init = 16'h8888;
    LUT4 busy_I_0_516_2_lut (.A(busy_c), .B(rec_r3[22]), .Z(ad_data3_23__N_676)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_516_2_lut.init = 16'h8888;
    FD1P3DX ad_data0_i2_1368_1369_reset (.D(n2000), .SP(scki_N_353_enable_30), 
            .CK(scki_N_353), .CD(ad_data0_23__N_424), .Q(n2001)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i2_1368_1369_reset.GSR = "DISABLED";
    LUT4 i1459_1_lut (.A(scki_N_353_enable_182), .Z(scki_N_353_enable_248)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1459_1_lut.init = 16'h5555;
    LUT4 i1376_1_lut (.A(scki_N_353_enable_3), .Z(scki_N_353_enable_24)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1376_1_lut.init = 16'h5555;
    LUT4 i1458_1_lut (.A(scki_N_353_enable_180), .Z(scki_N_353_enable_247)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1458_1_lut.init = 16'h5555;
    LUT4 i1410_1_lut (.A(scki_N_353_enable_54), .Z(scki_N_353_enable_19)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1410_1_lut.init = 16'h5555;
    LUT4 i1457_1_lut (.A(scki_N_353_enable_181), .Z(scki_N_353_enable_246)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1457_1_lut.init = 16'h5555;
    FD1S3JX cst_FSM_i0 (.D(n426), .CK(pre_clk), .PD(n3647), .Q(n413[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam cst_FSM_i0.GSR = "DISABLED";
    LUT4 i1456_1_lut (.A(scki_N_353_enable_178), .Z(scki_N_353_enable_245)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1456_1_lut.init = 16'h5555;
    LUT4 busy_I_0_371_2_lut (.A(busy_c), .B(rec_r0[20]), .Z(ad_data0_23__N_303)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_371_2_lut.init = 16'h8888;
    LUT4 i1455_1_lut (.A(scki_N_353_enable_179), .Z(scki_N_353_enable_244)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1455_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i3_1364_1365_reset (.D(n1996), .SP(scki_N_353_enable_31), 
            .CK(scki_N_353), .CD(ad_data0_23__N_421), .Q(n1997)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i3_1364_1365_reset.GSR = "DISABLED";
    LUT4 busy_I_0_470_2_lut (.A(busy_c), .B(rec_r2[20]), .Z(ad_data2_23__N_557)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_470_2_lut.init = 16'h8888;
    LUT4 busy_I_0_398_2_lut (.A(busy_c), .B(rec_r0[20]), .Z(ad_data0_23__N_370)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_398_2_lut.init = 16'h2222;
    LUT4 i1454_1_lut (.A(scki_N_353_enable_175), .Z(scki_N_353_enable_243)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1454_1_lut.init = 16'h5555;
    LUT4 busy_I_0_372_2_lut (.A(busy_c), .B(rec_r0[19]), .Z(ad_data0_23__N_304)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_372_2_lut.init = 16'h8888;
    FD1P3DX ad_data0_i4_1360_1361_reset (.D(n1992), .SP(scki_N_353_enable_32), 
            .CK(scki_N_353), .CD(ad_data0_23__N_418), .Q(n1993)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i4_1360_1361_reset.GSR = "DISABLED";
    LUT4 i1453_1_lut (.A(scki_N_353_enable_176), .Z(scki_N_353_enable_242)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1453_1_lut.init = 16'h5555;
    FD1P3DX ad_data3_i23_1008_1009_reset (.D(n1640), .SP(scki_N_353_enable_33), 
            .CK(scki_N_353), .CD(ad_data3_23__N_699), .Q(n1641)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i23_1008_1009_reset.GSR = "DISABLED";
    LUT4 i1452_1_lut (.A(scki_N_353_enable_174), .Z(scki_N_353_enable_241)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1452_1_lut.init = 16'h5555;
    LUT4 i1451_1_lut (.A(scki_N_353_enable_48), .Z(scki_N_353_enable_240)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1451_1_lut.init = 16'h5555;
    LUT4 busy_I_0_383_2_lut (.A(busy_c), .B(rec_r0[8]), .Z(ad_data0_23__N_315)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_383_2_lut.init = 16'h8888;
    FD1P3DX ad_data0_i5_1356_1357_reset (.D(n1988), .SP(scki_N_353_enable_34), 
            .CK(scki_N_353), .CD(ad_data0_23__N_415), .Q(n1989)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i5_1356_1357_reset.GSR = "DISABLED";
    LUT4 i1392_1_lut (.A(scki_N_353_enable_41), .Z(scki_N_353_enable_5)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1392_1_lut.init = 16'h5555;
    LUT4 i1450_1_lut (.A(scki_N_353_enable_22), .Z(scki_N_353_enable_239)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1450_1_lut.init = 16'h5555;
    FD1P3DX ad_data3_i0_1004_1005_reset (.D(n1636), .SP(scki_N_353_enable_35), 
            .CK(scki_N_353), .CD(ad_data3_23__N_793), .Q(n1637)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i0_1004_1005_reset.GSR = "DISABLED";
    LUT4 busy_I_0_399_2_lut (.A(busy_c), .B(rec_r0[19]), .Z(ad_data0_23__N_373)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_399_2_lut.init = 16'h2222;
    FD1P3DX ad_data0_i6_1352_1353_reset (.D(n1984), .SP(scki_N_353_enable_36), 
            .CK(scki_N_353), .CD(ad_data0_23__N_412), .Q(n1985)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i6_1352_1353_reset.GSR = "DISABLED";
    LUT4 i1449_1_lut (.A(scki_N_353_enable_173), .Z(scki_N_353_enable_238)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1449_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i7_1348_1349_reset (.D(n1980), .SP(scki_N_353_enable_37), 
            .CK(scki_N_353), .CD(ad_data0_23__N_409), .Q(n1981)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i7_1348_1349_reset.GSR = "DISABLED";
    LUT4 i1448_1_lut (.A(scki_N_353_enable_171), .Z(scki_N_353_enable_237)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1448_1_lut.init = 16'h5555;
    LUT4 busy_I_0_490_2_lut (.A(busy_c), .B(rec_r2[0]), .Z(ad_data2_23__N_577)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_490_2_lut.init = 16'h8888;
    LUT4 busy_I_0_373_2_lut (.A(busy_c), .B(rec_r0[18]), .Z(ad_data0_23__N_305)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_373_2_lut.init = 16'h8888;
    FD1P3DX ad_data0_i0_992_993_reset (.D(n1624), .SP(scki_N_353_enable_38), 
            .CK(scki_N_353), .CD(ad_data0_23__N_430), .Q(n1625)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i0_992_993_reset.GSR = "DISABLED";
    LUT4 i1447_1_lut (.A(scki_N_353_enable_172), .Z(scki_N_353_enable_236)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1447_1_lut.init = 16'h5555;
    FD1P3DX ad_data2_i1_1188_1189_reset (.D(n1820), .SP(scki_N_353_enable_39), 
            .CK(scki_N_353), .CD(ad_data2_23__N_669), .Q(n1821)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i1_1188_1189_reset.GSR = "DISABLED";
    FD1P3DX ad_data3_i22_1012_1013_reset (.D(n1644), .SP(scki_N_353_enable_40), 
            .CK(scki_N_353), .CD(ad_data3_23__N_727), .Q(n1645)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i22_1012_1013_reset.GSR = "DISABLED";
    LUT4 i1446_1_lut (.A(scki_N_353_enable_170), .Z(scki_N_353_enable_235)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1446_1_lut.init = 16'h5555;
    LUT4 i1445_1_lut (.A(scki_N_353_enable_146), .Z(scki_N_353_enable_234)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1445_1_lut.init = 16'h5555;
    FD1P3DX ad_data2_i0_1000_1001_reset (.D(n1632), .SP(scki_N_353_enable_41), 
            .CK(scki_N_353), .CD(ad_data2_23__N_672), .Q(n1633)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i0_1000_1001_reset.GSR = "DISABLED";
    LUT4 i1444_1_lut (.A(scki_N_353_enable_145), .Z(scki_N_353_enable_233)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1444_1_lut.init = 16'h5555;
    LUT4 i1443_1_lut (.A(scki_N_353_enable_144), .Z(scki_N_353_enable_232)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1443_1_lut.init = 16'h5555;
    LUT4 i1439_1_lut (.A(scki_N_353_enable_135), .Z(scki_N_353_enable_231)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1439_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i8_1344_1345_reset (.D(n1976), .SP(scki_N_353_enable_42), 
            .CK(scki_N_353), .CD(ad_data0_23__N_406), .Q(n1977)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i8_1344_1345_reset.GSR = "DISABLED";
    LUT4 i1438_1_lut (.A(scki_N_353_enable_111), .Z(scki_N_353_enable_230)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1438_1_lut.init = 16'h5555;
    LUT4 i1437_1_lut (.A(scki_N_353_enable_110), .Z(scki_N_353_enable_229)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1437_1_lut.init = 16'h5555;
    LUT4 i1436_1_lut (.A(scki_N_353_enable_109), .Z(scki_N_353_enable_228)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1436_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i9_1340_1341_reset (.D(n1972), .SP(scki_N_353_enable_43), 
            .CK(scki_N_353), .CD(ad_data0_23__N_403), .Q(n1973)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i9_1340_1341_reset.GSR = "DISABLED";
    LUT4 i1435_1_lut (.A(scki_N_353_enable_108), .Z(scki_N_353_enable_227)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1435_1_lut.init = 16'h5555;
    LUT4 i1434_1_lut (.A(scki_N_353_enable_107), .Z(scki_N_353_enable_226)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1434_1_lut.init = 16'h5555;
    LUT4 busy_I_0_400_2_lut (.A(busy_c), .B(rec_r0[18]), .Z(ad_data0_23__N_376)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_400_2_lut.init = 16'h2222;
    LUT4 i1433_1_lut (.A(scki_N_353_enable_106), .Z(scki_N_353_enable_225)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1433_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i10_1336_1337_reset (.D(n1968), .SP(scki_N_353_enable_44), 
            .CK(scki_N_353), .CD(ad_data0_23__N_400), .Q(n1969)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i10_1336_1337_reset.GSR = "DISABLED";
    LUT4 i1432_1_lut (.A(scki_N_353_enable_104), .Z(scki_N_353_enable_224)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1432_1_lut.init = 16'h5555;
    LUT4 i3_4_lut (.A(send_data[23]), .B(\send_data_23__N_809[12] ), .C(n3659), 
         .D(n3644), .Z(n3241)) /* synthesis lut_function=(!((B+(C+!(D)))+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam i3_4_lut.init = 16'h0200;
    LUT4 i2_3_lut (.A(n3644), .B(\send_data_23__N_809[12] ), .C(scki_N_359), 
         .Z(pre_clk_enable_6)) /* synthesis lut_function=(A+(B+!(C))) */ ;
    defparam i2_3_lut.init = 16'hefef;
    LUT4 i1412_1_lut (.A(scki_N_353_enable_57), .Z(scki_N_353_enable_223)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1412_1_lut.init = 16'h5555;
    LUT4 i1411_1_lut (.A(scki_N_353_enable_56), .Z(scki_N_353_enable_222)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1411_1_lut.init = 16'h5555;
    LUT4 i443_2_lut_3_lut_4_lut (.A(scnt[1]), .B(n3651), .C(scnt[3]), 
         .D(scnt[2]), .Z(n1085[3])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C))+!A !(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(129[13:21])
    defparam i443_2_lut_3_lut_4_lut.init = 16'h78f0;
    LUT4 busy_I_0_374_2_lut (.A(busy_c), .B(rec_r0[17]), .Z(ad_data0_23__N_306)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_374_2_lut.init = 16'h8888;
    FD1P3DX ad_data3_i19_1024_1025_reset (.D(n1656), .SP(scki_N_353_enable_45), 
            .CK(scki_N_353), .CD(ad_data3_23__N_736), .Q(n1657)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i19_1024_1025_reset.GSR = "DISABLED";
    FD1S3IX busy_diff1_63 (.D(n3657), .CK(pre_clk), .CD(scki_N_353_enable_169), 
            .Q(busy_diff1));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(36[7] 46[4])
    defparam busy_diff1_63.GSR = "DISABLED";
    FD1P3DX ad_data0_i11_1332_1333_reset (.D(n1964), .SP(scki_N_353_enable_46), 
            .CK(scki_N_353), .CD(ad_data0_23__N_397), .Q(n1965)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i11_1332_1333_reset.GSR = "DISABLED";
    FD1P3DX ad_data0_i12_1328_1329_reset (.D(n1960), .SP(scki_N_353_enable_47), 
            .CK(scki_N_353), .CD(ad_data0_23__N_394), .Q(n1961)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i12_1328_1329_reset.GSR = "DISABLED";
    LUT4 i1391_1_lut (.A(scki_N_353_enable_35), .Z(scki_N_353_enable_6)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1391_1_lut.init = 16'h5555;
    LUT4 busy_I_0_401_2_lut (.A(busy_c), .B(rec_r0[17]), .Z(ad_data0_23__N_379)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_401_2_lut.init = 16'h2222;
    LUT4 busy_I_0_442_2_lut (.A(busy_c), .B(rec_r1[0]), .Z(ad_data1_23__N_456)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_442_2_lut.init = 16'h8888;
    FD1S1D i1011 (.D(n3874), .CK(ad_data3_23__N_676), .CD(ad_data3_23__N_727), 
           .Q(scki_N_353_enable_40));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1011.GSR = "DISABLED";
    LUT4 busy_I_0_375_2_lut (.A(busy_c), .B(rec_r0[16]), .Z(ad_data0_23__N_307)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_375_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i0_996_997_reset (.D(n1628), .SP(scki_N_353_enable_48), 
            .CK(scki_N_353), .CD(ad_data1_23__N_551), .Q(n1629)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i0_996_997_reset.GSR = "DISABLED";
    LUT4 busy_I_0_466_2_lut (.A(busy_c), .B(rec_r1[0]), .Z(ad_data1_23__N_551)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_466_2_lut.init = 16'h2222;
    FD1P3DX ad_data2_i2_1184_1185_reset (.D(n1816), .SP(scki_N_353_enable_49), 
            .CK(scki_N_353), .CD(ad_data2_23__N_666), .Q(n1817)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i2_1184_1185_reset.GSR = "DISABLED";
    LUT4 busy_I_0_402_2_lut (.A(busy_c), .B(rec_r0[16]), .Z(ad_data0_23__N_382)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_402_2_lut.init = 16'h2222;
    FD1P3DX ad_data2_i3_1180_1181_reset (.D(n1812), .SP(scki_N_353_enable_50), 
            .CK(scki_N_353), .CD(ad_data2_23__N_663), .Q(n1813)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i3_1180_1181_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i4_1176_1177_reset (.D(n1808), .SP(scki_N_353_enable_51), 
            .CK(scki_N_353), .CD(ad_data2_23__N_660), .Q(n1809)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i4_1176_1177_reset.GSR = "DISABLED";
    FD1P3DX ad_data0_i13_1324_1325_reset (.D(n1956), .SP(scki_N_353_enable_52), 
            .CK(scki_N_353), .CD(ad_data0_23__N_391), .Q(n1957)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i13_1324_1325_reset.GSR = "DISABLED";
    LUT4 busy_I_0_514_2_lut (.A(busy_c), .B(rec_r2[0]), .Z(ad_data2_23__N_672)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_514_2_lut.init = 16'h2222;
    LUT4 busy_I_0_571_2_lut (.A(busy_c), .B(rec_r3[0]), .Z(ad_data3_23__N_793)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_571_2_lut.init = 16'h2222;
    FD1S1D i1015 (.D(n3874), .CK(ad_data3_23__N_677), .CD(ad_data3_23__N_730), 
           .Q(scki_N_353_enable_135));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1015.GSR = "DISABLED";
    FD1S1D i1019 (.D(n3874), .CK(ad_data3_23__N_678), .CD(ad_data3_23__N_733), 
           .Q(scki_N_353_enable_54));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1019.GSR = "DISABLED";
    FD1S1D i1023 (.D(n3874), .CK(ad_data3_23__N_679), .CD(ad_data3_23__N_736), 
           .Q(scki_N_353_enable_45));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1023.GSR = "DISABLED";
    LUT4 busy_I_0_376_2_lut (.A(busy_c), .B(rec_r0[15]), .Z(ad_data0_23__N_308)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_376_2_lut.init = 16'h8888;
    FD1P3AX rec_r0__i1 (.D(sdo0_c), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i1.GSR = "DISABLED";
    FD1P3DX ad_data3_i20_1020_1021_reset (.D(n1652), .SP(scki_N_353_enable_54), 
            .CK(scki_N_353), .CD(ad_data3_23__N_733), .Q(n1653)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i20_1020_1021_reset.GSR = "DISABLED";
    FD1P3BX ad_data0_i10_1336_1337_set (.D(n1969), .SP(scki_N_353_enable_55), 
            .CK(scki_N_353), .PD(ad_data0_23__N_313), .Q(n1968)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i10_1336_1337_set.GSR = "DISABLED";
    FD1S1D i1027 (.D(n3874), .CK(ad_data3_23__N_680), .CD(ad_data3_23__N_739), 
           .Q(scki_N_353_enable_11));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1027.GSR = "DISABLED";
    FD1S1D i1031 (.D(n3874), .CK(ad_data3_23__N_681), .CD(ad_data3_23__N_742), 
           .Q(scki_N_353_enable_29));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1031.GSR = "DISABLED";
    FD1S1D i1035 (.D(n3874), .CK(ad_data3_23__N_682), .CD(ad_data3_23__N_745), 
           .Q(scki_N_353_enable_202));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1035.GSR = "DISABLED";
    FD1P3DX ad_data2_i5_1172_1173_reset (.D(n1804), .SP(scki_N_353_enable_56), 
            .CK(scki_N_353), .CD(ad_data2_23__N_657), .Q(n1805)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i5_1172_1173_reset.GSR = "DISABLED";
    FD1P3DX ad_data0_i14_1320_1321_reset (.D(n1952), .SP(scki_N_353_enable_57), 
            .CK(scki_N_353), .CD(ad_data0_23__N_388), .Q(n1953)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i14_1320_1321_reset.GSR = "DISABLED";
    FD1S1D i1039 (.D(n3874), .CK(ad_data3_23__N_683), .CD(ad_data3_23__N_748), 
           .Q(scki_N_353_enable_191));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1039.GSR = "DISABLED";
    FD1S1D i1043 (.D(n3874), .CK(ad_data3_23__N_684), .CD(ad_data3_23__N_751), 
           .Q(scki_N_353_enable_187));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1043.GSR = "DISABLED";
    FD1S1D i1047 (.D(n3874), .CK(ad_data3_23__N_685), .CD(ad_data3_23__N_754), 
           .Q(scki_N_353_enable_178));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1047.GSR = "DISABLED";
    FD1S1D i1051 (.D(n3874), .CK(ad_data3_23__N_686), .CD(ad_data3_23__N_757), 
           .Q(scki_N_353_enable_212));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1051.GSR = "DISABLED";
    FD1S1D i1055 (.D(n3874), .CK(ad_data3_23__N_687), .CD(ad_data3_23__N_760), 
           .Q(scki_N_353_enable_210));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1055.GSR = "DISABLED";
    LUT4 i1102_3_lut (.A(n1733), .B(n1732), .C(scki_N_353_enable_207), 
         .Z(ad_data2[23])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1102_3_lut.init = 16'hcaca;
    FD1S1D i1059 (.D(n3874), .CK(ad_data3_23__N_688), .CD(ad_data3_23__N_763), 
           .Q(scki_N_353_enable_205));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1059.GSR = "DISABLED";
    LUT4 busy_I_0_539_2_lut (.A(busy_c), .B(rec_r3[23]), .Z(ad_data3_23__N_699)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_539_2_lut.init = 16'h2222;
    LUT4 busy_I_0_518_2_lut (.A(busy_c), .B(rec_r3[20]), .Z(ad_data3_23__N_678)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_518_2_lut.init = 16'h8888;
    LUT4 i1194_3_lut (.A(n1825), .B(n1824), .C(scki_N_353_enable_21), 
         .Z(ad_data1[23])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1194_3_lut.init = 16'hcaca;
    FD1S1D i991 (.D(n3874), .CK(ad_data0_23__N_323), .CD(ad_data0_23__N_430), 
           .Q(scki_N_353_enable_38));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i991.GSR = "DISABLED";
    FD1S1D i1063 (.D(n3874), .CK(ad_data3_23__N_689), .CD(ad_data3_23__N_766), 
           .Q(scki_N_353_enable_201));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1063.GSR = "DISABLED";
    FD1S1D i1067 (.D(n3874), .CK(ad_data3_23__N_690), .CD(ad_data3_23__N_769), 
           .Q(scki_N_353_enable_195));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1067.GSR = "DISABLED";
    FD1S3IX busy_diff2_64 (.D(busy_diff1), .CK(pre_clk), .CD(n3647), .Q(busy_diff2));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(36[7] 46[4])
    defparam busy_diff2_64.GSR = "DISABLED";
    FD1S1D i1071 (.D(n3874), .CK(ad_data3_23__N_691), .CD(ad_data3_23__N_772), 
           .Q(scki_N_353_enable_183));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1071.GSR = "DISABLED";
    LUT4 i1286_3_lut (.A(n1917), .B(n1916), .C(scki_N_353_enable_145), 
         .Z(ad_data0[23])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1286_3_lut.init = 16'hcaca;
    FD1S1D i1075 (.D(n3874), .CK(ad_data3_23__N_692), .CD(ad_data3_23__N_775), 
           .Q(scki_N_353_enable_180));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1075.GSR = "DISABLED";
    FD1S1D i1079 (.D(n3874), .CK(ad_data3_23__N_693), .CD(ad_data3_23__N_778), 
           .Q(scki_N_353_enable_22));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1079.GSR = "DISABLED";
    FD1S1D i1083 (.D(n3874), .CK(ad_data3_23__N_694), .CD(ad_data3_23__N_781), 
           .Q(scki_N_353_enable_220));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1083.GSR = "DISABLED";
    FD1S1D i1087 (.D(n3874), .CK(ad_data3_23__N_695), .CD(ad_data3_23__N_784), 
           .Q(scki_N_353_enable_218));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1087.GSR = "DISABLED";
    FD1S1D i1091 (.D(n3874), .CK(ad_data3_23__N_696), .CD(ad_data3_23__N_787), 
           .Q(scki_N_353_enable_216));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1091.GSR = "DISABLED";
    FD1S1D i1095 (.D(n3874), .CK(ad_data3_23__N_697), .CD(ad_data3_23__N_790), 
           .Q(scki_N_353_enable_214));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1095.GSR = "DISABLED";
    FD1S1D i1099 (.D(n3874), .CK(ad_data2_23__N_554), .CD(ad_data2_23__N_578), 
           .Q(scki_N_353_enable_207));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1099.GSR = "DISABLED";
    FD1S1D i1103 (.D(n3874), .CK(ad_data2_23__N_555), .CD(ad_data2_23__N_606), 
           .Q(scki_N_353_enable_198));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1103.GSR = "DISABLED";
    FD1S1D i1107 (.D(n3874), .CK(ad_data2_23__N_556), .CD(ad_data2_23__N_609), 
           .Q(scki_N_353_enable_177));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1107.GSR = "DISABLED";
    FD1S1D i1111 (.D(n3874), .CK(ad_data2_23__N_557), .CD(ad_data2_23__N_612), 
           .Q(scki_N_353_enable_3));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1111.GSR = "DISABLED";
    FD1S1D i1115 (.D(n3874), .CK(ad_data2_23__N_558), .CD(ad_data2_23__N_615), 
           .Q(scki_N_353_enable_221));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1115.GSR = "DISABLED";
    FD1S1D i1119 (.D(n3874), .CK(ad_data2_23__N_559), .CD(ad_data2_23__N_618), 
           .Q(scki_N_353_enable_219));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1119.GSR = "DISABLED";
    FD1S1D i1123 (.D(n3874), .CK(ad_data2_23__N_560), .CD(ad_data2_23__N_621), 
           .Q(scki_N_353_enable_217));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1123.GSR = "DISABLED";
    FD1S1D i1127 (.D(n3874), .CK(ad_data2_23__N_561), .CD(ad_data2_23__N_624), 
           .Q(scki_N_353_enable_215));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1127.GSR = "DISABLED";
    FD1S1D i1131 (.D(n3874), .CK(ad_data2_23__N_562), .CD(ad_data2_23__N_627), 
           .Q(scki_N_353_enable_213));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1131.GSR = "DISABLED";
    FD1S1D i1135 (.D(n3874), .CK(ad_data2_23__N_563), .CD(ad_data2_23__N_630), 
           .Q(scki_N_353_enable_209));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1135.GSR = "DISABLED";
    FD1S1D i1139 (.D(n3874), .CK(ad_data2_23__N_564), .CD(ad_data2_23__N_633), 
           .Q(scki_N_353_enable_206));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1139.GSR = "DISABLED";
    FD1S1D i1143 (.D(n3874), .CK(ad_data2_23__N_565), .CD(ad_data2_23__N_636), 
           .Q(scki_N_353_enable_204));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1143.GSR = "DISABLED";
    FD1S1D i1147 (.D(n3874), .CK(ad_data2_23__N_566), .CD(ad_data2_23__N_639), 
           .Q(scki_N_353_enable_199));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1147.GSR = "DISABLED";
    FD1S1D i1151 (.D(n3874), .CK(ad_data2_23__N_567), .CD(ad_data2_23__N_642), 
           .Q(scki_N_353_enable_197));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1151.GSR = "DISABLED";
    FD1S1D i1155 (.D(n3874), .CK(ad_data2_23__N_568), .CD(ad_data2_23__N_645), 
           .Q(scki_N_353_enable_193));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1155.GSR = "DISABLED";
    FD1S1D i1159 (.D(n3874), .CK(ad_data2_23__N_569), .CD(ad_data2_23__N_648), 
           .Q(scki_N_353_enable_184));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1159.GSR = "DISABLED";
    FD1S1D i1163 (.D(n3874), .CK(ad_data2_23__N_570), .CD(ad_data2_23__N_651), 
           .Q(scki_N_353_enable_175));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1163.GSR = "DISABLED";
    FD1S1D i1167 (.D(n3874), .CK(ad_data2_23__N_571), .CD(ad_data2_23__N_654), 
           .Q(scki_N_353_enable_171));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1167.GSR = "DISABLED";
    FD1S1D i1171 (.D(n3874), .CK(ad_data2_23__N_572), .CD(ad_data2_23__N_657), 
           .Q(scki_N_353_enable_56));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1171.GSR = "DISABLED";
    FD1S1D i1175 (.D(n3874), .CK(ad_data2_23__N_573), .CD(ad_data2_23__N_660), 
           .Q(scki_N_353_enable_51));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1175.GSR = "DISABLED";
    FD1S1D i1179 (.D(n3874), .CK(ad_data2_23__N_574), .CD(ad_data2_23__N_663), 
           .Q(scki_N_353_enable_50));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1179.GSR = "DISABLED";
    FD1S1D i1183 (.D(n3874), .CK(ad_data2_23__N_575), .CD(ad_data2_23__N_666), 
           .Q(scki_N_353_enable_49));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1183.GSR = "DISABLED";
    FD1S1D i1187 (.D(n3874), .CK(ad_data2_23__N_576), .CD(ad_data2_23__N_669), 
           .Q(scki_N_353_enable_39));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1187.GSR = "DISABLED";
    FD1S1D i1191 (.D(n3874), .CK(ad_data1_23__N_433), .CD(ad_data1_23__N_457), 
           .Q(scki_N_353_enable_21));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1191.GSR = "DISABLED";
    FD1S1D i1195 (.D(n3874), .CK(ad_data1_23__N_434), .CD(ad_data1_23__N_485), 
           .Q(scki_N_353_enable_12));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1195.GSR = "DISABLED";
    FD1S1D i1199 (.D(n3874), .CK(ad_data1_23__N_435), .CD(ad_data1_23__N_488), 
           .Q(scki_N_353_enable_211));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1199.GSR = "DISABLED";
    FD1S1D i1203 (.D(n3874), .CK(ad_data1_23__N_436), .CD(ad_data1_23__N_491), 
           .Q(scki_N_353_enable_208));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1203.GSR = "DISABLED";
    FD1S1D i1207 (.D(n3874), .CK(ad_data1_23__N_437), .CD(ad_data1_23__N_494), 
           .Q(scki_N_353_enable_203));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1207.GSR = "DISABLED";
    FD1S1D i1211 (.D(n3874), .CK(ad_data1_23__N_438), .CD(ad_data1_23__N_497), 
           .Q(scki_N_353_enable_200));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1211.GSR = "DISABLED";
    FD1S1D i1215 (.D(n3874), .CK(ad_data1_23__N_439), .CD(ad_data1_23__N_500), 
           .Q(scki_N_353_enable_196));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1215.GSR = "DISABLED";
    FD1S1D i1219 (.D(n3874), .CK(ad_data1_23__N_440), .CD(ad_data1_23__N_503), 
           .Q(scki_N_353_enable_194));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1219.GSR = "DISABLED";
    FD1S1D i1223 (.D(n3874), .CK(ad_data1_23__N_441), .CD(ad_data1_23__N_506), 
           .Q(scki_N_353_enable_192));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1223.GSR = "DISABLED";
    FD1S1D i1227 (.D(n3874), .CK(ad_data1_23__N_442), .CD(ad_data1_23__N_509), 
           .Q(scki_N_353_enable_190));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1227.GSR = "DISABLED";
    FD1S1D i1231 (.D(n3874), .CK(ad_data1_23__N_443), .CD(ad_data1_23__N_512), 
           .Q(scki_N_353_enable_189));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1231.GSR = "DISABLED";
    FD1S1D i1235 (.D(n3874), .CK(ad_data1_23__N_444), .CD(ad_data1_23__N_515), 
           .Q(scki_N_353_enable_188));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1235.GSR = "DISABLED";
    FD1S1D i1239 (.D(n3874), .CK(ad_data1_23__N_445), .CD(ad_data1_23__N_518), 
           .Q(scki_N_353_enable_186));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1239.GSR = "DISABLED";
    FD1S1D i1243 (.D(n3874), .CK(ad_data1_23__N_446), .CD(ad_data1_23__N_521), 
           .Q(scki_N_353_enable_185));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1243.GSR = "DISABLED";
    FD1S1D i1247 (.D(n3874), .CK(ad_data1_23__N_447), .CD(ad_data1_23__N_524), 
           .Q(scki_N_353_enable_182));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1247.GSR = "DISABLED";
    FD1S1D i1251 (.D(n3874), .CK(ad_data1_23__N_448), .CD(ad_data1_23__N_527), 
           .Q(scki_N_353_enable_181));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1251.GSR = "DISABLED";
    FD1S1D i1255 (.D(n3874), .CK(ad_data1_23__N_449), .CD(ad_data1_23__N_530), 
           .Q(scki_N_353_enable_179));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1255.GSR = "DISABLED";
    FD1S1D i1259 (.D(n3874), .CK(ad_data1_23__N_450), .CD(ad_data1_23__N_533), 
           .Q(scki_N_353_enable_176));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1259.GSR = "DISABLED";
    FD1S1D i1263 (.D(n3874), .CK(ad_data1_23__N_451), .CD(ad_data1_23__N_536), 
           .Q(scki_N_353_enable_174));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1263.GSR = "DISABLED";
    FD1S1D i1267 (.D(n3874), .CK(ad_data1_23__N_452), .CD(ad_data1_23__N_539), 
           .Q(scki_N_353_enable_173));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1267.GSR = "DISABLED";
    FD1S1D i1271 (.D(n3874), .CK(ad_data1_23__N_453), .CD(ad_data1_23__N_542), 
           .Q(scki_N_353_enable_172));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1271.GSR = "DISABLED";
    FD1S1D i1275 (.D(n3874), .CK(ad_data1_23__N_454), .CD(ad_data1_23__N_545), 
           .Q(scki_N_353_enable_170));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1275.GSR = "DISABLED";
    FD1S1D i1279 (.D(n3874), .CK(ad_data1_23__N_455), .CD(ad_data1_23__N_548), 
           .Q(scki_N_353_enable_146));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1279.GSR = "DISABLED";
    FD1S1D i1283 (.D(n3874), .CK(ad_data0_23__N_300), .CD(ad_data0_23__N_324), 
           .Q(scki_N_353_enable_145));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1283.GSR = "DISABLED";
    FD1S1D i1287 (.D(n3874), .CK(ad_data0_23__N_301), .CD(ad_data0_23__N_364), 
           .Q(scki_N_353_enable_144));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1287.GSR = "DISABLED";
    FD1S1D i1291 (.D(n3874), .CK(ad_data0_23__N_302), .CD(ad_data0_23__N_367), 
           .Q(scki_N_353_enable_111));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1291.GSR = "DISABLED";
    FD1S1D i1295 (.D(n3874), .CK(ad_data0_23__N_303), .CD(ad_data0_23__N_370), 
           .Q(scki_N_353_enable_110));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1295.GSR = "DISABLED";
    FD1S1D i1299 (.D(n3874), .CK(ad_data0_23__N_304), .CD(ad_data0_23__N_373), 
           .Q(scki_N_353_enable_109));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1299.GSR = "DISABLED";
    FD1S1D i1303 (.D(n3874), .CK(ad_data0_23__N_305), .CD(ad_data0_23__N_376), 
           .Q(scki_N_353_enable_108));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1303.GSR = "DISABLED";
    FD1S1D i1307 (.D(n3874), .CK(ad_data0_23__N_306), .CD(ad_data0_23__N_379), 
           .Q(scki_N_353_enable_107));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1307.GSR = "DISABLED";
    FD1S1D i1311 (.D(n3874), .CK(ad_data0_23__N_307), .CD(ad_data0_23__N_382), 
           .Q(scki_N_353_enable_106));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1311.GSR = "DISABLED";
    FD1S1D i1315 (.D(n3874), .CK(ad_data0_23__N_308), .CD(ad_data0_23__N_385), 
           .Q(scki_N_353_enable_104));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1315.GSR = "DISABLED";
    FD1S1D i1319 (.D(n3874), .CK(ad_data0_23__N_309), .CD(ad_data0_23__N_388), 
           .Q(scki_N_353_enable_57));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1319.GSR = "DISABLED";
    FD1S1D i1323 (.D(n3874), .CK(ad_data0_23__N_310), .CD(ad_data0_23__N_391), 
           .Q(scki_N_353_enable_52));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1323.GSR = "DISABLED";
    FD1S1D i1327 (.D(n3874), .CK(ad_data0_23__N_311), .CD(ad_data0_23__N_394), 
           .Q(scki_N_353_enable_47));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1327.GSR = "DISABLED";
    FD1S1D i1331 (.D(n3874), .CK(ad_data0_23__N_312), .CD(ad_data0_23__N_397), 
           .Q(scki_N_353_enable_46));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1331.GSR = "DISABLED";
    FD1S1D i1335 (.D(n3874), .CK(ad_data0_23__N_313), .CD(ad_data0_23__N_400), 
           .Q(scki_N_353_enable_44));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1335.GSR = "DISABLED";
    FD1S1D i1339 (.D(n3874), .CK(ad_data0_23__N_314), .CD(ad_data0_23__N_403), 
           .Q(scki_N_353_enable_43));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1339.GSR = "DISABLED";
    FD1S1D i1343 (.D(n3874), .CK(ad_data0_23__N_315), .CD(ad_data0_23__N_406), 
           .Q(scki_N_353_enable_42));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1343.GSR = "DISABLED";
    FD1S1D i1347 (.D(n3874), .CK(ad_data0_23__N_316), .CD(ad_data0_23__N_409), 
           .Q(scki_N_353_enable_37));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1347.GSR = "DISABLED";
    FD1S1D i1351 (.D(n3874), .CK(ad_data0_23__N_317), .CD(ad_data0_23__N_412), 
           .Q(scki_N_353_enable_36));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1351.GSR = "DISABLED";
    FD1S1D i1355 (.D(n3874), .CK(ad_data0_23__N_318), .CD(ad_data0_23__N_415), 
           .Q(scki_N_353_enable_34));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1355.GSR = "DISABLED";
    FD1S1D i1359 (.D(n3874), .CK(ad_data0_23__N_319), .CD(ad_data0_23__N_418), 
           .Q(scki_N_353_enable_32));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1359.GSR = "DISABLED";
    FD1S1D i1363 (.D(n3874), .CK(ad_data0_23__N_320), .CD(ad_data0_23__N_421), 
           .Q(scki_N_353_enable_31));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1363.GSR = "DISABLED";
    FD1S1D i1367 (.D(n3874), .CK(ad_data0_23__N_321), .CD(ad_data0_23__N_424), 
           .Q(scki_N_353_enable_30));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1367.GSR = "DISABLED";
    FD1S1D i1371 (.D(n3874), .CK(ad_data0_23__N_322), .CD(ad_data0_23__N_427), 
           .Q(scki_N_353_enable_27));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1371.GSR = "DISABLED";
    LUT4 i1409_1_lut (.A(scki_N_353_enable_52), .Z(scki_N_353_enable_143)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1409_1_lut.init = 16'h5555;
    LUT4 i1408_1_lut (.A(scki_N_353_enable_51), .Z(scki_N_353_enable_142)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1408_1_lut.init = 16'h5555;
    LUT4 i1407_1_lut (.A(scki_N_353_enable_50), .Z(scki_N_353_enable_141)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1407_1_lut.init = 16'h5555;
    LUT4 i1406_1_lut (.A(scki_N_353_enable_49), .Z(scki_N_353_enable_140)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1406_1_lut.init = 16'h5555;
    LUT4 i1405_1_lut (.A(scki_N_353_enable_47), .Z(scki_N_353_enable_139)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1405_1_lut.init = 16'h5555;
    LUT4 i1404_1_lut (.A(scki_N_353_enable_45), .Z(scki_N_353_enable_138)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1404_1_lut.init = 16'h5555;
    LUT4 i1403_1_lut (.A(scki_N_353_enable_38), .Z(scki_N_353_enable_137)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1403_1_lut.init = 16'h5555;
    LUT4 i1401_1_lut (.A(scki_N_353_enable_46), .Z(scki_N_353_enable_136)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1401_1_lut.init = 16'h5555;
    LUT4 i1274_3_lut (.A(n1905), .B(n1904), .C(scki_N_353_enable_172), 
         .Z(ad_data1[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1274_3_lut.init = 16'hcaca;
    LUT4 i1366_3_lut (.A(n1997), .B(n1996), .C(scki_N_353_enable_31), 
         .Z(ad_data0[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1366_3_lut.init = 16'hcaca;
    LUT4 i1394_1_lut (.A(scki_N_353_enable_39), .Z(scki_N_353_enable_2)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1394_1_lut.init = 16'h5555;
    LUT4 i450_3_lut_4_lut (.A(scnt[2]), .B(n3643), .C(scnt[3]), .D(scnt[4]), 
         .Z(n1085[4])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(D))+!A !(D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(129[13:21])
    defparam i450_3_lut_4_lut.init = 16'h7f80;
    LUT4 i1278_3_lut (.A(n1909), .B(n1908), .C(scki_N_353_enable_170), 
         .Z(ad_data1[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1278_3_lut.init = 16'hcaca;
    LUT4 i1370_3_lut (.A(n2001), .B(n2000), .C(scki_N_353_enable_30), 
         .Z(ad_data0[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1370_3_lut.init = 16'hcaca;
    LUT4 i1066_3_lut (.A(n1697), .B(n1696), .C(scki_N_353_enable_201), 
         .Z(ad_data3[9])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1066_3_lut.init = 16'hcaca;
    LUT4 i1142_3_lut (.A(n1773), .B(n1772), .C(scki_N_353_enable_206), 
         .Z(ad_data2[13])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1142_3_lut.init = 16'hcaca;
    LUT4 i1046_3_lut (.A(n1677), .B(n1676), .C(scki_N_353_enable_187), 
         .Z(ad_data3[14])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1046_3_lut.init = 16'hcaca;
    LUT4 i1138_3_lut (.A(n1769), .B(n1768), .C(scki_N_353_enable_209), 
         .Z(ad_data2[14])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1138_3_lut.init = 16'hcaca;
    LUT4 i1042_3_lut (.A(n1673), .B(n1672), .C(scki_N_353_enable_191), 
         .Z(ad_data3[15])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1042_3_lut.init = 16'hcaca;
    LUT4 i1134_3_lut (.A(n1765), .B(n1764), .C(scki_N_353_enable_213), 
         .Z(ad_data2[15])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1134_3_lut.init = 16'hcaca;
    LUT4 i436_2_lut_3_lut_4_lut (.A(scnt[0]), .B(n3659), .C(scnt[2]), 
         .D(scnt[1]), .Z(n1085[2])) /* synthesis lut_function=(A (B (C)+!B !(C (D)+!C !(D)))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(129[13:21])
    defparam i436_2_lut_3_lut_4_lut.init = 16'hd2f0;
    LUT4 i1098_3_lut (.A(n1729), .B(n1728), .C(scki_N_353_enable_214), 
         .Z(ad_data3[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1098_3_lut.init = 16'hcaca;
    LUT4 i1190_3_lut (.A(n1821), .B(n1820), .C(scki_N_353_enable_39), 
         .Z(ad_data2[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1190_3_lut.init = 16'hcaca;
    LUT4 i1094_3_lut (.A(n1725), .B(n1724), .C(scki_N_353_enable_216), 
         .Z(ad_data3[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1094_3_lut.init = 16'hcaca;
    LUT4 i1186_3_lut (.A(n1817), .B(n1816), .C(scki_N_353_enable_49), 
         .Z(ad_data2[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1186_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_417_2_lut (.A(busy_c), .B(rec_r0[1]), .Z(ad_data0_23__N_427)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_417_2_lut.init = 16'h2222;
    LUT4 i1375_1_lut (.A(scki_N_353_enable_29), .Z(scki_N_353_enable_28)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1375_1_lut.init = 16'h5555;
    LUT4 busy_I_0_538_2_lut (.A(busy_c), .B(rec_r3[0]), .Z(ad_data3_23__N_698)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_538_2_lut.init = 16'h8888;
    FD1P3AX rec_r3__i24 (.D(rec_r3[22]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[23])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i24.GSR = "DISABLED";
    FD1P3AX rec_r3__i23 (.D(rec_r3[21]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[22])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i23.GSR = "DISABLED";
    FD1P3AX rec_r3__i22 (.D(rec_r3[20]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[21])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i22.GSR = "DISABLED";
    FD1P3AX rec_r3__i21 (.D(rec_r3[19]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[20])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i21.GSR = "DISABLED";
    FD1P3AX rec_r3__i20 (.D(rec_r3[18]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[19])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i20.GSR = "DISABLED";
    FD1P3AX rec_r3__i19 (.D(rec_r3[17]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[18])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i19.GSR = "DISABLED";
    FD1P3AX rec_r3__i18 (.D(rec_r3[16]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[17])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i18.GSR = "DISABLED";
    FD1P3AX rec_r3__i17 (.D(rec_r3[15]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[16])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i17.GSR = "DISABLED";
    FD1P3AX rec_r3__i16 (.D(rec_r3[14]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[15])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i16.GSR = "DISABLED";
    FD1P3AX rec_r3__i15 (.D(rec_r3[13]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[14])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i15.GSR = "DISABLED";
    FD1P3AX rec_r3__i14 (.D(rec_r3[12]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[13])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i14.GSR = "DISABLED";
    FD1P3AX rec_r3__i13 (.D(rec_r3[11]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[12])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i13.GSR = "DISABLED";
    FD1P3AX rec_r3__i12 (.D(rec_r3[10]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[11])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i12.GSR = "DISABLED";
    FD1P3AX rec_r3__i11 (.D(rec_r3[9]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[10])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i11.GSR = "DISABLED";
    FD1P3AX rec_r3__i10 (.D(rec_r3[8]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[9])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i10.GSR = "DISABLED";
    FD1P3AX rec_r3__i9 (.D(rec_r3[7]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[8])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i9.GSR = "DISABLED";
    FD1P3AX rec_r3__i8 (.D(rec_r3[6]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[7])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i8.GSR = "DISABLED";
    FD1P3AX rec_r3__i7 (.D(rec_r3[5]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[6])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i7.GSR = "DISABLED";
    FD1P3AX rec_r3__i6 (.D(rec_r3[4]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[5])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i6.GSR = "DISABLED";
    FD1P3AX rec_r3__i5 (.D(rec_r3[3]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[4])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i5.GSR = "DISABLED";
    FD1P3AX rec_r3__i4 (.D(rec_r3[2]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[3])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i4.GSR = "DISABLED";
    FD1P3AX rec_r3__i3 (.D(rec_r3[1]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i3.GSR = "DISABLED";
    FD1P3AX rec_r3__i2 (.D(rec_r3[0]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r3[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r3__i2.GSR = "DISABLED";
    FD1P3AX rec_r2__i24 (.D(rec_r2[22]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[23])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i24.GSR = "DISABLED";
    FD1P3AX rec_r2__i23 (.D(rec_r2[21]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[22])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i23.GSR = "DISABLED";
    FD1P3AX rec_r2__i22 (.D(rec_r2[20]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[21])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i22.GSR = "DISABLED";
    FD1P3AX rec_r2__i21 (.D(rec_r2[19]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[20])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i21.GSR = "DISABLED";
    FD1P3AX rec_r2__i20 (.D(rec_r2[18]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[19])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i20.GSR = "DISABLED";
    FD1P3AX rec_r2__i19 (.D(rec_r2[17]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[18])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i19.GSR = "DISABLED";
    FD1P3AX rec_r2__i18 (.D(rec_r2[16]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[17])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i18.GSR = "DISABLED";
    FD1P3AX rec_r2__i17 (.D(rec_r2[15]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[16])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i17.GSR = "DISABLED";
    FD1P3AX rec_r2__i16 (.D(rec_r2[14]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[15])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i16.GSR = "DISABLED";
    FD1P3AX rec_r2__i15 (.D(rec_r2[13]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[14])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i15.GSR = "DISABLED";
    FD1P3AX rec_r2__i14 (.D(rec_r2[12]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[13])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i14.GSR = "DISABLED";
    FD1P3AX rec_r2__i13 (.D(rec_r2[11]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[12])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i13.GSR = "DISABLED";
    FD1P3AX rec_r2__i12 (.D(rec_r2[10]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[11])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i12.GSR = "DISABLED";
    FD1P3AX rec_r2__i11 (.D(rec_r2[9]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[10])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i11.GSR = "DISABLED";
    FD1P3AX rec_r2__i10 (.D(rec_r2[8]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[9])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i10.GSR = "DISABLED";
    FD1P3AX rec_r2__i9 (.D(rec_r2[7]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[8])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i9.GSR = "DISABLED";
    FD1P3AX rec_r2__i8 (.D(rec_r2[6]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[7])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i8.GSR = "DISABLED";
    FD1P3AX rec_r2__i7 (.D(rec_r2[5]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[6])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i7.GSR = "DISABLED";
    FD1P3AX rec_r2__i6 (.D(rec_r2[4]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[5])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i6.GSR = "DISABLED";
    FD1P3AX rec_r2__i5 (.D(rec_r2[3]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[4])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i5.GSR = "DISABLED";
    FD1P3AX rec_r2__i4 (.D(rec_r2[2]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[3])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i4.GSR = "DISABLED";
    FD1P3AX rec_r2__i3 (.D(rec_r2[1]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i3.GSR = "DISABLED";
    FD1P3AX rec_r2__i2 (.D(rec_r2[0]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r2[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r2__i2.GSR = "DISABLED";
    LUT4 i1090_3_lut (.A(n1721), .B(n1720), .C(scki_N_353_enable_218), 
         .Z(ad_data3[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1090_3_lut.init = 16'hcaca;
    FD1S3JX send_data_i12 (.D(n1617), .CK(pre_clk), .PD(\send_data_23__N_809[12] ), 
            .Q(send_data[12])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i12.GSR = "DISABLED";
    FD1P3DX ad_data0_i15_1316_1317_reset (.D(n1948), .SP(scki_N_353_enable_104), 
            .CK(scki_N_353), .CD(ad_data0_23__N_385), .Q(n1949)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i15_1316_1317_reset.GSR = "DISABLED";
    FD1P3BX ad_data3_i18_1028_1029_set (.D(n1661), .SP(scki_N_353_enable_105), 
            .CK(scki_N_353), .PD(ad_data3_23__N_680), .Q(n1660)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i18_1028_1029_set.GSR = "DISABLED";
    LUT4 i1182_3_lut (.A(n1813), .B(n1812), .C(scki_N_353_enable_50), 
         .Z(ad_data2[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1182_3_lut.init = 16'hcaca;
    FD1P3DX ad_data0_i16_1312_1313_reset (.D(n1944), .SP(scki_N_353_enable_106), 
            .CK(scki_N_353), .CD(ad_data0_23__N_382), .Q(n1945)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i16_1312_1313_reset.GSR = "DISABLED";
    LUT4 i1282_3_lut (.A(n1913), .B(n1912), .C(scki_N_353_enable_146), 
         .Z(ad_data1[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1282_3_lut.init = 16'hcaca;
    FD1P3DX ad_data0_i17_1308_1309_reset (.D(n1940), .SP(scki_N_353_enable_107), 
            .CK(scki_N_353), .CD(ad_data0_23__N_379), .Q(n1941)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i17_1308_1309_reset.GSR = "DISABLED";
    FD1P3DX ad_data0_i18_1304_1305_reset (.D(n1936), .SP(scki_N_353_enable_108), 
            .CK(scki_N_353), .CD(ad_data0_23__N_376), .Q(n1937)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i18_1304_1305_reset.GSR = "DISABLED";
    LUT4 i1374_3_lut (.A(n2005), .B(n2004), .C(scki_N_353_enable_27), 
         .Z(ad_data0[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1374_3_lut.init = 16'hcaca;
    LUT4 i1226_3_lut (.A(n1857), .B(n1856), .C(scki_N_353_enable_192), 
         .Z(ad_data1[15])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1226_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_521_2_lut (.A(busy_c), .B(rec_r3[17]), .Z(ad_data3_23__N_681)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_521_2_lut.init = 16'h8888;
    LUT4 busy_I_0_489_2_lut (.A(busy_c), .B(rec_r2[1]), .Z(ad_data2_23__N_576)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_489_2_lut.init = 16'h8888;
    LUT4 busy_I_0_545_2_lut (.A(busy_c), .B(rec_r3[17]), .Z(ad_data3_23__N_742)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_545_2_lut.init = 16'h2222;
    LUT4 busy_I_0_416_2_lut (.A(busy_c), .B(rec_r0[2]), .Z(ad_data0_23__N_424)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_416_2_lut.init = 16'h2222;
    FD1P3DX ad_data0_i19_1300_1301_reset (.D(n1932), .SP(scki_N_353_enable_109), 
            .CK(scki_N_353), .CD(ad_data0_23__N_373), .Q(n1933)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i19_1300_1301_reset.GSR = "DISABLED";
    LUT4 i1390_1_lut (.A(scki_N_353_enable_37), .Z(scki_N_353_enable_7)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1390_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i20_1296_1297_reset (.D(n1928), .SP(scki_N_353_enable_110), 
            .CK(scki_N_353), .CD(ad_data0_23__N_370), .Q(n1929)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i20_1296_1297_reset.GSR = "DISABLED";
    LUT4 busy_I_0_377_2_lut (.A(busy_c), .B(rec_r0[14]), .Z(ad_data0_23__N_309)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_377_2_lut.init = 16'h8888;
    FD1P3DX ad_data0_i21_1292_1293_reset (.D(n1924), .SP(scki_N_353_enable_111), 
            .CK(scki_N_353), .CD(ad_data0_23__N_367), .Q(n1925)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i21_1292_1293_reset.GSR = "DISABLED";
    LUT4 busy_I_0_415_2_lut (.A(busy_c), .B(rec_r0[3]), .Z(ad_data0_23__N_421)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_415_2_lut.init = 16'h2222;
    FD1P3AX rec_r1__i24 (.D(rec_r1[22]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[23])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i24.GSR = "DISABLED";
    FD1P3AX rec_r1__i23 (.D(rec_r1[21]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[22])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i23.GSR = "DISABLED";
    FD1P3AX rec_r1__i22 (.D(rec_r1[20]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[21])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i22.GSR = "DISABLED";
    FD1P3AX rec_r1__i21 (.D(rec_r1[19]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[20])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i21.GSR = "DISABLED";
    FD1P3AX rec_r1__i20 (.D(rec_r1[18]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[19])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i20.GSR = "DISABLED";
    FD1P3AX rec_r1__i19 (.D(rec_r1[17]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[18])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i19.GSR = "DISABLED";
    FD1P3AX rec_r1__i18 (.D(rec_r1[16]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[17])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i18.GSR = "DISABLED";
    FD1P3AX rec_r1__i17 (.D(rec_r1[15]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[16])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i17.GSR = "DISABLED";
    FD1P3AX rec_r1__i16 (.D(rec_r1[14]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[15])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i16.GSR = "DISABLED";
    FD1P3AX rec_r1__i15 (.D(rec_r1[13]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[14])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i15.GSR = "DISABLED";
    FD1P3AX rec_r1__i14 (.D(rec_r1[12]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[13])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i14.GSR = "DISABLED";
    FD1P3AX rec_r1__i13 (.D(rec_r1[11]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[12])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i13.GSR = "DISABLED";
    FD1P3AX rec_r1__i12 (.D(rec_r1[10]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[11])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i12.GSR = "DISABLED";
    FD1P3AX rec_r1__i11 (.D(rec_r1[9]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[10])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i11.GSR = "DISABLED";
    FD1P3AX rec_r1__i10 (.D(rec_r1[8]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[9])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i10.GSR = "DISABLED";
    FD1P3AX rec_r1__i9 (.D(rec_r1[7]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[8])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i9.GSR = "DISABLED";
    FD1P3AX rec_r1__i8 (.D(rec_r1[6]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[7])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i8.GSR = "DISABLED";
    FD1P3AX rec_r1__i7 (.D(rec_r1[5]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[6])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i7.GSR = "DISABLED";
    FD1P3AX rec_r1__i6 (.D(rec_r1[4]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[5])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i6.GSR = "DISABLED";
    FD1P3AX rec_r1__i5 (.D(rec_r1[3]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[4])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i5.GSR = "DISABLED";
    FD1P3AX rec_r1__i4 (.D(rec_r1[2]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[3])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i4.GSR = "DISABLED";
    FD1P3AX rec_r1__i3 (.D(rec_r1[1]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i3.GSR = "DISABLED";
    FD1P3AX rec_r1__i2 (.D(rec_r1[0]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r1[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r1__i2.GSR = "DISABLED";
    FD1P3IX scnt__i4 (.D(n1085[4]), .SP(pre_clk_enable_5), .CD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(scnt[4])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scnt__i4.GSR = "DISABLED";
    FD1P3IX scnt__i3 (.D(n1085[3]), .SP(pre_clk_enable_5), .CD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(scnt[3])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scnt__i3.GSR = "DISABLED";
    FD1P3IX scnt__i2 (.D(n1085[2]), .SP(pre_clk_enable_5), .CD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(scnt[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scnt__i2.GSR = "DISABLED";
    FD1P3IX scnt__i1 (.D(n1085[1]), .SP(pre_clk_enable_5), .CD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(scnt[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam scnt__i1.GSR = "DISABLED";
    FD1P3DX ad_data3_i21_1016_1017_reset (.D(n1648), .SP(scki_N_353_enable_135), 
            .CK(scki_N_353), .CD(ad_data3_23__N_730), .Q(n1649)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i21_1016_1017_reset.GSR = "DISABLED";
    LUT4 i1318_3_lut (.A(n1949), .B(n1948), .C(scki_N_353_enable_104), 
         .Z(ad_data0[15])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1318_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_494_2_lut (.A(busy_c), .B(rec_r2[20]), .Z(ad_data2_23__N_612)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_494_2_lut.init = 16'h2222;
    LUT4 busy_I_0_414_2_lut (.A(busy_c), .B(rec_r0[4]), .Z(ad_data0_23__N_418)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_414_2_lut.init = 16'h2222;
    FD1P3BX ad_data0_i11_1332_1333_set (.D(n1965), .SP(scki_N_353_enable_136), 
            .CK(scki_N_353), .PD(ad_data0_23__N_312), .Q(n1964)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i11_1332_1333_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i0_992_993_set (.D(n1625), .SP(scki_N_353_enable_137), 
            .CK(scki_N_353), .PD(ad_data0_23__N_323), .Q(n1624)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i0_992_993_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i19_1024_1025_set (.D(n1657), .SP(scki_N_353_enable_138), 
            .CK(scki_N_353), .PD(ad_data3_23__N_679), .Q(n1656)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i19_1024_1025_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i12_1328_1329_set (.D(n1961), .SP(scki_N_353_enable_139), 
            .CK(scki_N_353), .PD(ad_data0_23__N_311), .Q(n1960)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i12_1328_1329_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i2_1184_1185_set (.D(n1817), .SP(scki_N_353_enable_140), 
            .CK(scki_N_353), .PD(ad_data2_23__N_575), .Q(n1816)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i2_1184_1185_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i3_1180_1181_set (.D(n1813), .SP(scki_N_353_enable_141), 
            .CK(scki_N_353), .PD(ad_data2_23__N_574), .Q(n1812)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i3_1180_1181_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i4_1176_1177_set (.D(n1809), .SP(scki_N_353_enable_142), 
            .CK(scki_N_353), .PD(ad_data2_23__N_573), .Q(n1808)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i4_1176_1177_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i13_1324_1325_set (.D(n1957), .SP(scki_N_353_enable_143), 
            .CK(scki_N_353), .PD(ad_data0_23__N_310), .Q(n1956)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i13_1324_1325_set.GSR = "DISABLED";
    LUT4 i1930_4_lut_rep_17 (.A(n3659), .B(n3657), .C(n427), .D(n413[3]), 
         .Z(n3644)) /* synthesis lut_function=(A (B (C))+!A (B (C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam i1930_4_lut_rep_17.init = 16'hc4c0;
    LUT4 i1230_3_lut (.A(n1861), .B(n1860), .C(scki_N_353_enable_190), 
         .Z(ad_data1[14])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1230_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_378_2_lut (.A(busy_c), .B(rec_r0[13]), .Z(ad_data0_23__N_310)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_378_2_lut.init = 16'h8888;
    LUT4 busy_n_I_14_2_lut_rep_31 (.A(busy_diff1), .B(busy_diff2), .Z(n3658)) /* synthesis lut_function=(!(A+!(B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(35[17:41])
    defparam busy_n_I_14_2_lut_rep_31.init = 16'h4444;
    LUT4 i71_2_lut_3_lut (.A(busy_diff1), .B(busy_diff2), .C(n413[0]), 
         .Z(n427)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(35[17:41])
    defparam i71_2_lut_3_lut.init = 16'h4040;
    LUT4 i590_2_lut_3_lut_4_lut (.A(busy_diff1), .B(busy_diff2), .C(n413[2]), 
         .D(n413[0]), .Z(n426)) /* synthesis lut_function=(A (C+(D))+!A (B (C)+!B (C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(35[17:41])
    defparam i590_2_lut_3_lut_4_lut.init = 16'hfbf0;
    LUT4 i1322_3_lut (.A(n1953), .B(n1952), .C(scki_N_353_enable_57), 
         .Z(ad_data0[14])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1322_3_lut.init = 16'hcaca;
    LUT4 i1387_1_lut (.A(scki_N_353_enable_33), .Z(scki_N_353_enable_10)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1387_1_lut.init = 16'h5555;
    LUT4 busy_I_0_413_2_lut (.A(busy_c), .B(rec_r0[5]), .Z(ad_data0_23__N_415)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_413_2_lut.init = 16'h2222;
    LUT4 busy_I_0_515_2_lut (.A(busy_c), .B(rec_r3[23]), .Z(ad_data3_23__N_675)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_515_2_lut.init = 16'h8888;
    LUT4 busy_I_0_544_2_lut (.A(busy_c), .B(rec_r3[18]), .Z(ad_data3_23__N_739)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_544_2_lut.init = 16'h2222;
    LUT4 busy_I_0_412_2_lut (.A(busy_c), .B(rec_r0[6]), .Z(ad_data0_23__N_412)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_412_2_lut.init = 16'h2222;
    LUT4 busy_I_0_411_2_lut (.A(busy_c), .B(rec_r0[7]), .Z(ad_data0_23__N_409)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_411_2_lut.init = 16'h2222;
    LUT4 busy_I_0_418_2_lut (.A(busy_c), .B(rec_r0[0]), .Z(ad_data0_23__N_430)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_418_2_lut.init = 16'h2222;
    LUT4 busy_I_0_384_2_lut (.A(busy_c), .B(rec_r0[7]), .Z(ad_data0_23__N_316)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_384_2_lut.init = 16'h8888;
    LUT4 busy_I_0_513_2_lut (.A(busy_c), .B(rec_r2[1]), .Z(ad_data2_23__N_669)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_513_2_lut.init = 16'h2222;
    FD1P3DX ad_data0_i22_1288_1289_reset (.D(n1920), .SP(scki_N_353_enable_144), 
            .CK(scki_N_353), .CD(ad_data0_23__N_364), .Q(n1921)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i22_1288_1289_reset.GSR = "DISABLED";
    LUT4 busy_I_0_540_2_lut (.A(busy_c), .B(rec_r3[22]), .Z(ad_data3_23__N_727)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_540_2_lut.init = 16'h2222;
    LUT4 busy_I_0_444_2_lut (.A(busy_c), .B(rec_r1[22]), .Z(ad_data1_23__N_485)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_444_2_lut.init = 16'h2222;
    LUT4 i1386_1_lut (.A(scki_N_353_enable_32), .Z(scki_N_353_enable_13)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1386_1_lut.init = 16'h5555;
    FD1P3DX ad_data0_i23_1284_1285_reset (.D(n1916), .SP(scki_N_353_enable_145), 
            .CK(scki_N_353), .CD(ad_data0_23__N_324), .Q(n1917)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i23_1284_1285_reset.GSR = "DISABLED";
    LUT4 busy_I_0_387_2_lut (.A(busy_c), .B(rec_r0[4]), .Z(ad_data0_23__N_319)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_387_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i1_1280_1281_reset (.D(n1912), .SP(scki_N_353_enable_146), 
            .CK(scki_N_353), .CD(ad_data1_23__N_548), .Q(n1913)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i1_1280_1281_reset.GSR = "DISABLED";
    FD1P3AX rec_r0__i2 (.D(rec_r0[0]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i2.GSR = "DISABLED";
    LUT4 i2014_2_lut_rep_32 (.A(scnt[3]), .B(scnt[4]), .Z(n3659)) /* synthesis lut_function=(A (B)) */ ;
    defparam i2014_2_lut_rep_32.init = 16'h8888;
    LUT4 i1_2_lut_3_lut_4_lut (.A(scnt[3]), .B(scnt[4]), .C(\send_data_23__N_809[12] ), 
         .D(n3644), .Z(pre_clk_enable_17)) /* synthesis lut_function=(A (B (C)+!B (C+(D)))+!A (C+(D))) */ ;
    defparam i1_2_lut_3_lut_4_lut.init = 16'hf7f0;
    LUT4 busy_I_0_410_2_lut (.A(busy_c), .B(rec_r0[8]), .Z(ad_data0_23__N_406)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_410_2_lut.init = 16'h2222;
    LUT4 i431_2_lut_rep_16_3_lut_4_lut (.A(scnt[3]), .B(scnt[4]), .C(scnt[1]), 
         .D(scnt[0]), .Z(n3643)) /* synthesis lut_function=(!(A (B+!(C (D)))+!A !(C (D)))) */ ;
    defparam i431_2_lut_rep_16_3_lut_4_lut.init = 16'h7000;
    LUT4 i1358_3_lut (.A(n1989), .B(n1988), .C(scki_N_353_enable_34), 
         .Z(ad_data0[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1358_3_lut.init = 16'hcaca;
    LUT4 i1389_1_lut (.A(scki_N_353_enable_36), .Z(scki_N_353_enable_8)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1389_1_lut.init = 16'h5555;
    FD1P3AX rec_r0__i3 (.D(rec_r0[1]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i3.GSR = "DISABLED";
    FD1P3AX rec_r0__i4 (.D(rec_r0[2]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[3])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i4.GSR = "DISABLED";
    FD1P3AX rec_r0__i5 (.D(rec_r0[3]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[4])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i5.GSR = "DISABLED";
    FD1P3AX rec_r0__i6 (.D(rec_r0[4]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[5])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i6.GSR = "DISABLED";
    FD1P3AX rec_r0__i7 (.D(rec_r0[5]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[6])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i7.GSR = "DISABLED";
    FD1P3AX rec_r0__i8 (.D(rec_r0[6]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[7])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i8.GSR = "DISABLED";
    FD1P3AX rec_r0__i9 (.D(rec_r0[7]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[8])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i9.GSR = "DISABLED";
    FD1P3AX rec_r0__i10 (.D(rec_r0[8]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[9])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i10.GSR = "DISABLED";
    FD1P3AX rec_r0__i11 (.D(rec_r0[9]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[10])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i11.GSR = "DISABLED";
    FD1P3AX rec_r0__i12 (.D(rec_r0[10]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[11])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i12.GSR = "DISABLED";
    FD1P3AX rec_r0__i13 (.D(rec_r0[11]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[12])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i13.GSR = "DISABLED";
    FD1P3AX rec_r0__i14 (.D(rec_r0[12]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[13])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i14.GSR = "DISABLED";
    FD1P3AX rec_r0__i15 (.D(rec_r0[13]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[14])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i15.GSR = "DISABLED";
    FD1P3AX rec_r0__i16 (.D(rec_r0[14]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[15])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i16.GSR = "DISABLED";
    FD1P3AX rec_r0__i17 (.D(rec_r0[15]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[16])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i17.GSR = "DISABLED";
    FD1P3AX rec_r0__i18 (.D(rec_r0[16]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[17])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i18.GSR = "DISABLED";
    FD1P3AX rec_r0__i19 (.D(rec_r0[17]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[18])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i19.GSR = "DISABLED";
    FD1P3AX rec_r0__i20 (.D(rec_r0[18]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[19])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i20.GSR = "DISABLED";
    FD1P3AX rec_r0__i21 (.D(rec_r0[19]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[20])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i21.GSR = "DISABLED";
    FD1P3AX rec_r0__i22 (.D(rec_r0[20]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[21])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i22.GSR = "DISABLED";
    FD1P3AX rec_r0__i23 (.D(rec_r0[21]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[22])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i23.GSR = "DISABLED";
    FD1P3AX rec_r0__i24 (.D(rec_r0[22]), .SP(scki_N_353_enable_169), .CK(scki_N_353), 
            .Q(rec_r0[23])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam rec_r0__i24.GSR = "DISABLED";
    LUT4 i1385_1_lut (.A(scki_N_353_enable_31), .Z(scki_N_353_enable_14)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1385_1_lut.init = 16'h5555;
    LUT4 busy_I_0_409_2_lut (.A(busy_c), .B(rec_r0[9]), .Z(ad_data0_23__N_403)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_409_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i2_1276_1277_reset (.D(n1908), .SP(scki_N_353_enable_170), 
            .CK(scki_N_353), .CD(ad_data1_23__N_545), .Q(n1909)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i2_1276_1277_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i6_1168_1169_reset (.D(n1800), .SP(scki_N_353_enable_171), 
            .CK(scki_N_353), .CD(ad_data2_23__N_654), .Q(n1801)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i6_1168_1169_reset.GSR = "DISABLED";
    LUT4 busy_I_0_388_2_lut (.A(busy_c), .B(rec_r0[3]), .Z(ad_data0_23__N_320)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_388_2_lut.init = 16'h8888;
    LUT4 busy_I_0_408_2_lut (.A(busy_c), .B(rec_r0[10]), .Z(ad_data0_23__N_400)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_408_2_lut.init = 16'h2222;
    LUT4 busy_I_0_543_2_lut (.A(busy_c), .B(rec_r3[19]), .Z(ad_data3_23__N_736)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_543_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i3_1272_1273_reset (.D(n1904), .SP(scki_N_353_enable_172), 
            .CK(scki_N_353), .CD(ad_data1_23__N_542), .Q(n1905)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i3_1272_1273_reset.GSR = "DISABLED";
    LUT4 busy_I_0_547_2_lut (.A(busy_c), .B(rec_r3[15]), .Z(ad_data3_23__N_748)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_547_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i4_1268_1269_reset (.D(n1900), .SP(scki_N_353_enable_173), 
            .CK(scki_N_353), .CD(ad_data1_23__N_539), .Q(n1901)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i4_1268_1269_reset.GSR = "DISABLED";
    LUT4 busy_I_0_407_2_lut (.A(busy_c), .B(rec_r0[11]), .Z(ad_data0_23__N_397)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_407_2_lut.init = 16'h2222;
    LUT4 i1384_1_lut (.A(scki_N_353_enable_30), .Z(scki_N_353_enable_15)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1384_1_lut.init = 16'h5555;
    LUT4 busy_I_0_406_2_lut (.A(busy_c), .B(rec_r0[12]), .Z(ad_data0_23__N_394)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_406_2_lut.init = 16'h2222;
    LUT4 busy_I_0_389_2_lut (.A(busy_c), .B(rec_r0[2]), .Z(ad_data0_23__N_321)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_389_2_lut.init = 16'h8888;
    LUT4 i1383_1_lut (.A(scki_N_353_enable_27), .Z(scki_N_353_enable_16)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1383_1_lut.init = 16'h5555;
    FD1P3DX ad_data1_i5_1264_1265_reset (.D(n1896), .SP(scki_N_353_enable_174), 
            .CK(scki_N_353), .CD(ad_data1_23__N_536), .Q(n1897)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i5_1264_1265_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i7_1164_1165_reset (.D(n1796), .SP(scki_N_353_enable_175), 
            .CK(scki_N_353), .CD(ad_data2_23__N_651), .Q(n1797)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i7_1164_1165_reset.GSR = "DISABLED";
    LUT4 busy_I_0_390_2_lut (.A(busy_c), .B(rec_r0[1]), .Z(ad_data0_23__N_322)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_390_2_lut.init = 16'h8888;
    LUT4 i1234_3_lut (.A(n1865), .B(n1864), .C(scki_N_353_enable_189), 
         .Z(ad_data1[13])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1234_3_lut.init = 16'hcaca;
    FD1P3DX ad_data1_i6_1260_1261_reset (.D(n1892), .SP(scki_N_353_enable_176), 
            .CK(scki_N_353), .CD(ad_data1_23__N_533), .Q(n1893)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i6_1260_1261_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i21_1108_1109_reset (.D(n1740), .SP(scki_N_353_enable_177), 
            .CK(scki_N_353), .CD(ad_data2_23__N_609), .Q(n1741)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i21_1108_1109_reset.GSR = "DISABLED";
    LUT4 i568_3_lut_4_lut (.A(scnt[3]), .B(scnt[4]), .C(n416), .D(n413[3]), 
         .Z(n1194)) /* synthesis lut_function=(A (B (C+(D)))) */ ;
    defparam i568_3_lut_4_lut.init = 16'h8880;
    FD1P3DX ad_data3_i13_1048_1049_reset (.D(n1680), .SP(scki_N_353_enable_178), 
            .CK(scki_N_353), .CD(ad_data3_23__N_754), .Q(n1681)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i13_1048_1049_reset.GSR = "DISABLED";
    LUT4 busy_I_0_512_2_lut (.A(busy_c), .B(rec_r2[2]), .Z(ad_data2_23__N_666)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_512_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i7_1256_1257_reset (.D(n1888), .SP(scki_N_353_enable_179), 
            .CK(scki_N_353), .CD(ad_data1_23__N_530), .Q(n1889)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i7_1256_1257_reset.GSR = "DISABLED";
    FD1P3DX ad_data3_i6_1076_1077_reset (.D(n1708), .SP(scki_N_353_enable_180), 
            .CK(scki_N_353), .CD(ad_data3_23__N_775), .Q(n1709)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i6_1076_1077_reset.GSR = "DISABLED";
    LUT4 i1326_3_lut (.A(n1957), .B(n1956), .C(scki_N_353_enable_52), 
         .Z(ad_data0[13])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1326_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_511_2_lut (.A(busy_c), .B(rec_r2[3]), .Z(ad_data2_23__N_663)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_511_2_lut.init = 16'h2222;
    LUT4 busy_I_0_510_2_lut (.A(busy_c), .B(rec_r2[4]), .Z(ad_data2_23__N_660)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_510_2_lut.init = 16'h2222;
    LUT4 i178_1_lut_4_lut (.A(n3659), .B(n3657), .C(n427), .D(n413[3]), 
         .Z(scki_N_356)) /* synthesis lut_function=(!(A (B (C))+!A (B (C+(D))))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam i178_1_lut_4_lut.init = 16'h3b3f;
    LUT4 i1238_3_lut (.A(n1869), .B(n1868), .C(scki_N_353_enable_188), 
         .Z(ad_data1[12])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1238_3_lut.init = 16'hcaca;
    FD1P3DX ad_data1_i8_1252_1253_reset (.D(n1884), .SP(scki_N_353_enable_181), 
            .CK(scki_N_353), .CD(ad_data1_23__N_527), .Q(n1885)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i8_1252_1253_reset.GSR = "DISABLED";
    LUT4 i985_2_lut_3_lut_4_lut (.A(scnt[3]), .B(scnt[4]), .C(send_data[12]), 
         .D(n3644), .Z(n1617)) /* synthesis lut_function=(A (B (C)+!B !((D)+!C))+!A !((D)+!C)) */ ;
    defparam i985_2_lut_3_lut_4_lut.init = 16'h80f0;
    LUT4 busy_I_0_405_2_lut (.A(busy_c), .B(rec_r0[13]), .Z(ad_data0_23__N_391)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_405_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i9_1248_1249_reset (.D(n1880), .SP(scki_N_353_enable_182), 
            .CK(scki_N_353), .CD(ad_data1_23__N_524), .Q(n1881)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i9_1248_1249_reset.GSR = "DISABLED";
    LUT4 i1270_3_lut (.A(n1901), .B(n1900), .C(scki_N_353_enable_173), 
         .Z(ad_data1[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1270_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_524_2_lut (.A(busy_c), .B(rec_r3[14]), .Z(ad_data3_23__N_684)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_524_2_lut.init = 16'h8888;
    LUT4 busy_I_0_548_2_lut (.A(busy_c), .B(rec_r3[14]), .Z(ad_data3_23__N_751)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_548_2_lut.init = 16'h2222;
    LUT4 busy_I_0_525_2_lut (.A(busy_c), .B(rec_r3[13]), .Z(ad_data3_23__N_685)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_525_2_lut.init = 16'h8888;
    FD1P3DX ad_data3_i7_1072_1073_reset (.D(n1704), .SP(scki_N_353_enable_183), 
            .CK(scki_N_353), .CD(ad_data3_23__N_772), .Q(n1705)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i7_1072_1073_reset.GSR = "DISABLED";
    LUT4 i2_3_lut_3_lut_4_lut (.A(n413[0]), .B(n3658), .C(n413[2]), .D(n3657), 
         .Z(\send_data_23__N_809[12] )) /* synthesis lut_function=(A ((C+!(D))+!B)+!A (C+!(D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(73[2] 87[9])
    defparam i2_3_lut_3_lut_4_lut.init = 16'hf2ff;
    FD1P3DX ad_data2_i8_1160_1161_reset (.D(n1792), .SP(scki_N_353_enable_184), 
            .CK(scki_N_353), .CD(ad_data2_23__N_648), .Q(n1793)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i8_1160_1161_reset.GSR = "DISABLED";
    FD1P3DX ad_data1_i10_1244_1245_reset (.D(n1876), .SP(scki_N_353_enable_185), 
            .CK(scki_N_353), .CD(ad_data1_23__N_521), .Q(n1877)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i10_1244_1245_reset.GSR = "DISABLED";
    LUT4 busy_I_0_549_2_lut (.A(busy_c), .B(rec_r3[13]), .Z(ad_data3_23__N_754)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_549_2_lut.init = 16'h2222;
    LUT4 busy_I_0_526_2_lut (.A(busy_c), .B(rec_r3[12]), .Z(ad_data3_23__N_686)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_526_2_lut.init = 16'h8888;
    LUT4 busy_I_0_550_2_lut (.A(busy_c), .B(rec_r3[12]), .Z(ad_data3_23__N_757)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_550_2_lut.init = 16'h2222;
    LUT4 busy_I_0_527_2_lut (.A(busy_c), .B(rec_r3[11]), .Z(ad_data3_23__N_687)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_527_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i11_1240_1241_reset (.D(n1872), .SP(scki_N_353_enable_186), 
            .CK(scki_N_353), .CD(ad_data1_23__N_518), .Q(n1873)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i11_1240_1241_reset.GSR = "DISABLED";
    FD1P3DX ad_data3_i14_1044_1045_reset (.D(n1676), .SP(scki_N_353_enable_187), 
            .CK(scki_N_353), .CD(ad_data3_23__N_751), .Q(n1677)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i14_1044_1045_reset.GSR = "DISABLED";
    LUT4 busy_I_0_551_2_lut (.A(busy_c), .B(rec_r3[11]), .Z(ad_data3_23__N_760)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_551_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i12_1236_1237_reset (.D(n1868), .SP(scki_N_353_enable_188), 
            .CK(scki_N_353), .CD(ad_data1_23__N_515), .Q(n1869)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i12_1236_1237_reset.GSR = "DISABLED";
    LUT4 busy_I_0_528_2_lut (.A(busy_c), .B(rec_r3[10]), .Z(ad_data3_23__N_688)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_528_2_lut.init = 16'h8888;
    LUT4 i1150_3_lut (.A(n1781), .B(n1780), .C(scki_N_353_enable_199), 
         .Z(ad_data2[11])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1150_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_552_2_lut (.A(busy_c), .B(rec_r3[10]), .Z(ad_data3_23__N_763)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_552_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i13_1232_1233_reset (.D(n1864), .SP(scki_N_353_enable_189), 
            .CK(scki_N_353), .CD(ad_data1_23__N_512), .Q(n1865)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i13_1232_1233_reset.GSR = "DISABLED";
    LUT4 busy_I_0_391_2_lut (.A(busy_c), .B(rec_r0[0]), .Z(ad_data0_23__N_323)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_391_2_lut.init = 16'h8888;
    LUT4 busy_I_0_529_2_lut (.A(busy_c), .B(rec_r3[9]), .Z(ad_data3_23__N_689)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_529_2_lut.init = 16'h8888;
    LUT4 busy_I_0_553_2_lut (.A(busy_c), .B(rec_r3[9]), .Z(ad_data3_23__N_766)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_553_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i14_1228_1229_reset (.D(n1860), .SP(scki_N_353_enable_190), 
            .CK(scki_N_353), .CD(ad_data1_23__N_509), .Q(n1861)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i14_1228_1229_reset.GSR = "DISABLED";
    LUT4 busy_I_0_530_2_lut (.A(busy_c), .B(rec_r3[8]), .Z(ad_data3_23__N_690)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_530_2_lut.init = 16'h8888;
    LUT4 busy_I_0_554_2_lut (.A(busy_c), .B(rec_r3[8]), .Z(ad_data3_23__N_769)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_554_2_lut.init = 16'h2222;
    FD1P3DX ad_data3_i15_1040_1041_reset (.D(n1672), .SP(scki_N_353_enable_191), 
            .CK(scki_N_353), .CD(ad_data3_23__N_748), .Q(n1673)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i15_1040_1041_reset.GSR = "DISABLED";
    LUT4 busy_I_0_379_2_lut (.A(busy_c), .B(rec_r0[12]), .Z(ad_data0_23__N_311)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_379_2_lut.init = 16'h8888;
    LUT4 busy_I_0_531_2_lut (.A(busy_c), .B(rec_r3[7]), .Z(ad_data3_23__N_691)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_531_2_lut.init = 16'h8888;
    LUT4 busy_I_0_555_2_lut (.A(busy_c), .B(rec_r3[7]), .Z(ad_data3_23__N_772)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_555_2_lut.init = 16'h2222;
    LUT4 i1058_3_lut (.A(n1689), .B(n1688), .C(scki_N_353_enable_210), 
         .Z(ad_data3[11])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1058_3_lut.init = 16'hcaca;
    FD1P3DX ad_data1_i15_1224_1225_reset (.D(n1856), .SP(scki_N_353_enable_192), 
            .CK(scki_N_353), .CD(ad_data1_23__N_506), .Q(n1857)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i15_1224_1225_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i9_1156_1157_reset (.D(n1788), .SP(scki_N_353_enable_193), 
            .CK(scki_N_353), .CD(ad_data2_23__N_645), .Q(n1789)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i9_1156_1157_reset.GSR = "DISABLED";
    LUT4 busy_I_0_532_2_lut (.A(busy_c), .B(rec_r3[6]), .Z(ad_data3_23__N_692)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_532_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i16_1220_1221_reset (.D(n1852), .SP(scki_N_353_enable_194), 
            .CK(scki_N_353), .CD(ad_data1_23__N_503), .Q(n1853)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i16_1220_1221_reset.GSR = "DISABLED";
    LUT4 busy_I_0_556_2_lut (.A(busy_c), .B(rec_r3[6]), .Z(ad_data3_23__N_775)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_556_2_lut.init = 16'h2222;
    LUT4 busy_I_0_533_2_lut (.A(busy_c), .B(rec_r3[5]), .Z(ad_data3_23__N_693)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_533_2_lut.init = 16'h8888;
    LUT4 busy_I_0_385_2_lut (.A(busy_c), .B(rec_r0[6]), .Z(ad_data0_23__N_317)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_385_2_lut.init = 16'h8888;
    LUT4 busy_I_0_534_2_lut (.A(busy_c), .B(rec_r3[4]), .Z(ad_data3_23__N_694)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_534_2_lut.init = 16'h8888;
    LUT4 busy_I_0_558_2_lut (.A(busy_c), .B(rec_r3[4]), .Z(ad_data3_23__N_781)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_558_2_lut.init = 16'h2222;
    LUT4 busy_I_0_535_2_lut (.A(busy_c), .B(rec_r3[3]), .Z(ad_data3_23__N_695)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_535_2_lut.init = 16'h8888;
    LUT4 i1006_3_lut (.A(n1637), .B(n1636), .C(scki_N_353_enable_35), 
         .Z(ad_data3[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1006_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_559_2_lut (.A(busy_c), .B(rec_r3[3]), .Z(ad_data3_23__N_784)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_559_2_lut.init = 16'h2222;
    FD1P3DX ad_data3_i8_1068_1069_reset (.D(n1700), .SP(scki_N_353_enable_195), 
            .CK(scki_N_353), .CD(ad_data3_23__N_769), .Q(n1701)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i8_1068_1069_reset.GSR = "DISABLED";
    FD1P3DX ad_data1_i17_1216_1217_reset (.D(n1848), .SP(scki_N_353_enable_196), 
            .CK(scki_N_353), .CD(ad_data1_23__N_500), .Q(n1849)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i17_1216_1217_reset.GSR = "DISABLED";
    LUT4 i1388_1_lut (.A(scki_N_353_enable_34), .Z(scki_N_353_enable_9)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1388_1_lut.init = 16'h5555;
    LUT4 busy_I_0_536_2_lut (.A(busy_c), .B(rec_r3[2]), .Z(ad_data3_23__N_696)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_536_2_lut.init = 16'h8888;
    LUT4 busy_I_0_560_2_lut (.A(busy_c), .B(rec_r3[2]), .Z(ad_data3_23__N_787)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_560_2_lut.init = 16'h2222;
    LUT4 busy_I_0_537_2_lut (.A(busy_c), .B(rec_r3[1]), .Z(ad_data3_23__N_697)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_537_2_lut.init = 16'h8888;
    FD1P3DX ad_data2_i10_1152_1153_reset (.D(n1784), .SP(scki_N_353_enable_197), 
            .CK(scki_N_353), .CD(ad_data2_23__N_642), .Q(n1785)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i10_1152_1153_reset.GSR = "DISABLED";
    LUT4 busy_I_0_561_2_lut (.A(busy_c), .B(rec_r3[1]), .Z(ad_data3_23__N_790)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_561_2_lut.init = 16'h2222;
    FD1P3DX ad_data2_i22_1104_1105_reset (.D(n1736), .SP(scki_N_353_enable_198), 
            .CK(scki_N_353), .CD(ad_data2_23__N_606), .Q(n1737)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i22_1104_1105_reset.GSR = "DISABLED";
    LUT4 busy_I_0_467_2_lut (.A(busy_c), .B(rec_r2[23]), .Z(ad_data2_23__N_554)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_467_2_lut.init = 16'h8888;
    LUT4 busy_I_0_491_2_lut (.A(busy_c), .B(rec_r2[23]), .Z(ad_data2_23__N_578)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_491_2_lut.init = 16'h2222;
    LUT4 busy_I_0_468_2_lut (.A(busy_c), .B(rec_r2[22]), .Z(ad_data2_23__N_555)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_468_2_lut.init = 16'h8888;
    FD1P3DX ad_data2_i11_1148_1149_reset (.D(n1780), .SP(scki_N_353_enable_199), 
            .CK(scki_N_353), .CD(ad_data2_23__N_639), .Q(n1781)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i11_1148_1149_reset.GSR = "DISABLED";
    LUT4 busy_I_0_492_2_lut (.A(busy_c), .B(rec_r2[22]), .Z(ad_data2_23__N_606)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_492_2_lut.init = 16'h2222;
    LUT4 busy_I_0_517_2_lut (.A(busy_c), .B(rec_r3[21]), .Z(ad_data3_23__N_677)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_517_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i18_1212_1213_reset (.D(n1844), .SP(scki_N_353_enable_200), 
            .CK(scki_N_353), .CD(ad_data1_23__N_497), .Q(n1845)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i18_1212_1213_reset.GSR = "DISABLED";
    FD1P3DX ad_data3_i9_1064_1065_reset (.D(n1696), .SP(scki_N_353_enable_201), 
            .CK(scki_N_353), .CD(ad_data3_23__N_766), .Q(n1697)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i9_1064_1065_reset.GSR = "DISABLED";
    LUT4 busy_I_0_469_2_lut (.A(busy_c), .B(rec_r2[21]), .Z(ad_data2_23__N_556)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_469_2_lut.init = 16'h8888;
    LUT4 busy_I_0_493_2_lut (.A(busy_c), .B(rec_r2[21]), .Z(ad_data2_23__N_609)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_493_2_lut.init = 16'h2222;
    LUT4 busy_I_0_471_2_lut (.A(busy_c), .B(rec_r2[19]), .Z(ad_data2_23__N_558)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_471_2_lut.init = 16'h8888;
    LUT4 busy_I_0_495_2_lut (.A(busy_c), .B(rec_r2[19]), .Z(ad_data2_23__N_615)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_495_2_lut.init = 16'h2222;
    LUT4 busy_I_0_472_2_lut (.A(busy_c), .B(rec_r2[18]), .Z(ad_data2_23__N_559)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_472_2_lut.init = 16'h8888;
    FD1P3DX ad_data3_i16_1036_1037_reset (.D(n1668), .SP(scki_N_353_enable_202), 
            .CK(scki_N_353), .CD(ad_data3_23__N_745), .Q(n1669)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i16_1036_1037_reset.GSR = "DISABLED";
    LUT4 busy_I_0_496_2_lut (.A(busy_c), .B(rec_r2[18]), .Z(ad_data2_23__N_618)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_496_2_lut.init = 16'h2222;
    LUT4 busy_I_0_473_2_lut (.A(busy_c), .B(rec_r2[17]), .Z(ad_data2_23__N_560)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_473_2_lut.init = 16'h8888;
    LUT4 busy_I_0_497_2_lut (.A(busy_c), .B(rec_r2[17]), .Z(ad_data2_23__N_621)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_497_2_lut.init = 16'h2222;
    FD1P3DX ad_data1_i19_1208_1209_reset (.D(n1840), .SP(scki_N_353_enable_203), 
            .CK(scki_N_353), .CD(ad_data1_23__N_494), .Q(n1841)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i19_1208_1209_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i12_1144_1145_reset (.D(n1776), .SP(scki_N_353_enable_204), 
            .CK(scki_N_353), .CD(ad_data2_23__N_636), .Q(n1777)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i12_1144_1145_reset.GSR = "DISABLED";
    LUT4 busy_I_0_474_2_lut (.A(busy_c), .B(rec_r2[16]), .Z(ad_data2_23__N_561)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_474_2_lut.init = 16'h8888;
    LUT4 i1330_3_lut (.A(n1961), .B(n1960), .C(scki_N_353_enable_47), 
         .Z(ad_data0[12])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1330_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_3_lut (.A(scnt[3]), .B(scnt[4]), .C(scnt[0]), .Z(n3270)) /* synthesis lut_function=(A (B (C)+!B !(C))+!A !(C)) */ ;
    defparam i1_2_lut_3_lut.init = 16'h8787;
    FD1P3DX ad_data3_i10_1060_1061_reset (.D(n1692), .SP(scki_N_353_enable_205), 
            .CK(scki_N_353), .CD(ad_data3_23__N_763), .Q(n1693)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i10_1060_1061_reset.GSR = "DISABLED";
    LUT4 i1242_3_lut (.A(n1873), .B(n1872), .C(scki_N_353_enable_186), 
         .Z(ad_data1[11])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1242_3_lut.init = 16'hcaca;
    FD1P3DX ad_data2_i13_1140_1141_reset (.D(n1772), .SP(scki_N_353_enable_206), 
            .CK(scki_N_353), .CD(ad_data2_23__N_633), .Q(n1773)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i13_1140_1141_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i23_1100_1101_reset (.D(n1732), .SP(scki_N_353_enable_207), 
            .CK(scki_N_353), .CD(ad_data2_23__N_578), .Q(n1733)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i23_1100_1101_reset.GSR = "DISABLED";
    LUT4 i423_2_lut_rep_24_3_lut (.A(scnt[3]), .B(scnt[4]), .C(scnt[0]), 
         .Z(n3651)) /* synthesis lut_function=(!(A (B+!(C))+!A !(C))) */ ;
    defparam i423_2_lut_rep_24_3_lut.init = 16'h7070;
    LUT4 i1334_3_lut (.A(n1965), .B(n1964), .C(scki_N_353_enable_46), 
         .Z(ad_data0[11])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1334_3_lut.init = 16'hcaca;
    FD1P3DX ad_data1_i20_1204_1205_reset (.D(n1836), .SP(scki_N_353_enable_208), 
            .CK(scki_N_353), .CD(ad_data1_23__N_491), .Q(n1837)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i20_1204_1205_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i14_1136_1137_reset (.D(n1768), .SP(scki_N_353_enable_209), 
            .CK(scki_N_353), .CD(ad_data2_23__N_630), .Q(n1769)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i14_1136_1137_reset.GSR = "DISABLED";
    LUT4 busy_I_0_498_2_lut (.A(busy_c), .B(rec_r2[16]), .Z(ad_data2_23__N_624)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_498_2_lut.init = 16'h2222;
    LUT4 busy_I_0_475_2_lut (.A(busy_c), .B(rec_r2[15]), .Z(ad_data2_23__N_562)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_475_2_lut.init = 16'h8888;
    LUT4 busy_I_0_499_2_lut (.A(busy_c), .B(rec_r2[15]), .Z(ad_data2_23__N_627)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_499_2_lut.init = 16'h2222;
    LUT4 busy_I_0_476_2_lut (.A(busy_c), .B(rec_r2[14]), .Z(ad_data2_23__N_563)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_476_2_lut.init = 16'h8888;
    LUT4 busy_I_0_500_2_lut (.A(busy_c), .B(rec_r2[14]), .Z(ad_data2_23__N_630)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_500_2_lut.init = 16'h2222;
    FD1P3DX ad_data3_i11_1056_1057_reset (.D(n1688), .SP(scki_N_353_enable_210), 
            .CK(scki_N_353), .CD(ad_data3_23__N_760), .Q(n1689)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i11_1056_1057_reset.GSR = "DISABLED";
    LUT4 busy_I_0_477_2_lut (.A(busy_c), .B(rec_r2[13]), .Z(ad_data2_23__N_564)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_477_2_lut.init = 16'h8888;
    LUT4 busy_I_0_501_2_lut (.A(busy_c), .B(rec_r2[13]), .Z(ad_data2_23__N_633)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_501_2_lut.init = 16'h2222;
    LUT4 busy_I_0_478_2_lut (.A(busy_c), .B(rec_r2[12]), .Z(ad_data2_23__N_565)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_478_2_lut.init = 16'h8888;
    FD1P3DX ad_data1_i21_1200_1201_reset (.D(n1832), .SP(scki_N_353_enable_211), 
            .CK(scki_N_353), .CD(ad_data1_23__N_488), .Q(n1833)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i21_1200_1201_reset.GSR = "DISABLED";
    LUT4 i429_2_lut_3_lut_4_lut (.A(scnt[3]), .B(scnt[4]), .C(scnt[1]), 
         .D(scnt[0]), .Z(n1085[1])) /* synthesis lut_function=(A (B (C)+!B !(C (D)+!C !(D)))+!A !(C (D)+!C !(D))) */ ;
    defparam i429_2_lut_3_lut_4_lut.init = 16'h87f0;
    FD1P3DX ad_data3_i12_1052_1053_reset (.D(n1684), .SP(scki_N_353_enable_212), 
            .CK(scki_N_353), .CD(ad_data3_23__N_757), .Q(n1685)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i12_1052_1053_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i15_1132_1133_reset (.D(n1764), .SP(scki_N_353_enable_213), 
            .CK(scki_N_353), .CD(ad_data2_23__N_627), .Q(n1765)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i15_1132_1133_reset.GSR = "DISABLED";
    LUT4 i1246_3_lut (.A(n1877), .B(n1876), .C(scki_N_353_enable_185), 
         .Z(ad_data1[10])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1246_3_lut.init = 16'hcaca;
    LUT4 i1338_3_lut (.A(n1969), .B(n1968), .C(scki_N_353_enable_44), 
         .Z(ad_data0[10])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1338_3_lut.init = 16'hcaca;
    FD1P3DX ad_data3_i1_1096_1097_reset (.D(n1728), .SP(scki_N_353_enable_214), 
            .CK(scki_N_353), .CD(ad_data3_23__N_790), .Q(n1729)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i1_1096_1097_reset.GSR = "DISABLED";
    LUT4 busy_I_0_502_2_lut (.A(busy_c), .B(rec_r2[12]), .Z(ad_data2_23__N_636)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_502_2_lut.init = 16'h2222;
    FD1P3DX ad_data2_i16_1128_1129_reset (.D(n1760), .SP(scki_N_353_enable_215), 
            .CK(scki_N_353), .CD(ad_data2_23__N_624), .Q(n1761)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i16_1128_1129_reset.GSR = "DISABLED";
    LUT4 busy_I_0_479_2_lut (.A(busy_c), .B(rec_r2[11]), .Z(ad_data2_23__N_566)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_479_2_lut.init = 16'h8888;
    FD1P3DX ad_data3_i2_1092_1093_reset (.D(n1724), .SP(scki_N_353_enable_216), 
            .CK(scki_N_353), .CD(ad_data3_23__N_787), .Q(n1725)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i2_1092_1093_reset.GSR = "DISABLED";
    FD1P3DX ad_data2_i17_1124_1125_reset (.D(n1756), .SP(scki_N_353_enable_217), 
            .CK(scki_N_353), .CD(ad_data2_23__N_621), .Q(n1757)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i17_1124_1125_reset.GSR = "DISABLED";
    LUT4 i1250_3_lut (.A(n1881), .B(n1880), .C(scki_N_353_enable_182), 
         .Z(ad_data1[9])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1250_3_lut.init = 16'hcaca;
    FD1P3DX ad_data3_i3_1088_1089_reset (.D(n1720), .SP(scki_N_353_enable_218), 
            .CK(scki_N_353), .CD(ad_data3_23__N_784), .Q(n1721)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i3_1088_1089_reset.GSR = "DISABLED";
    LUT4 i1342_3_lut (.A(n1973), .B(n1972), .C(scki_N_353_enable_43), 
         .Z(ad_data0[9])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1342_3_lut.init = 16'hcaca;
    LUT4 i1254_3_lut (.A(n1885), .B(n1884), .C(scki_N_353_enable_181), 
         .Z(ad_data1[8])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1254_3_lut.init = 16'hcaca;
    FD1P3DX ad_data2_i18_1120_1121_reset (.D(n1752), .SP(scki_N_353_enable_219), 
            .CK(scki_N_353), .CD(ad_data2_23__N_618), .Q(n1753)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i18_1120_1121_reset.GSR = "DISABLED";
    LUT4 busy_I_0_503_2_lut (.A(busy_c), .B(rec_r2[11]), .Z(ad_data2_23__N_639)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_503_2_lut.init = 16'h2222;
    LUT4 busy_I_0_386_2_lut (.A(busy_c), .B(rec_r0[5]), .Z(ad_data0_23__N_318)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_386_2_lut.init = 16'h8888;
    LUT4 busy_I_0_480_2_lut (.A(busy_c), .B(rec_r2[10]), .Z(ad_data2_23__N_567)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_480_2_lut.init = 16'h8888;
    LUT4 busy_I_0_504_2_lut (.A(busy_c), .B(rec_r2[10]), .Z(ad_data2_23__N_642)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_504_2_lut.init = 16'h2222;
    LUT4 i1346_3_lut (.A(n1977), .B(n1976), .C(scki_N_353_enable_42), 
         .Z(ad_data0[8])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1346_3_lut.init = 16'hcaca;
    FD1P3DX ad_data3_i4_1084_1085_reset (.D(n1716), .SP(scki_N_353_enable_220), 
            .CK(scki_N_353), .CD(ad_data3_23__N_781), .Q(n1717)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i4_1084_1085_reset.GSR = "DISABLED";
    LUT4 busy_I_0_481_2_lut (.A(busy_c), .B(rec_r2[9]), .Z(ad_data2_23__N_568)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_481_2_lut.init = 16'h8888;
    FD1P3DX ad_data2_i19_1116_1117_reset (.D(n1748), .SP(scki_N_353_enable_221), 
            .CK(scki_N_353), .CD(ad_data2_23__N_615), .Q(n1749)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i19_1116_1117_reset.GSR = "DISABLED";
    LUT4 busy_I_0_505_2_lut (.A(busy_c), .B(rec_r2[9]), .Z(ad_data2_23__N_645)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_505_2_lut.init = 16'h2222;
    LUT4 busy_I_0_482_2_lut (.A(busy_c), .B(rec_r2[8]), .Z(ad_data2_23__N_569)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_482_2_lut.init = 16'h8888;
    LUT4 i1002_3_lut (.A(n1633), .B(n1632), .C(scki_N_353_enable_41), 
         .Z(ad_data2[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1002_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_506_2_lut (.A(busy_c), .B(rec_r2[8]), .Z(ad_data2_23__N_648)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_506_2_lut.init = 16'h2222;
    FD1P3BX ad_data2_i5_1172_1173_set (.D(n1805), .SP(scki_N_353_enable_222), 
            .CK(scki_N_353), .PD(ad_data2_23__N_572), .Q(n1804)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i5_1172_1173_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i14_1320_1321_set (.D(n1953), .SP(scki_N_353_enable_223), 
            .CK(scki_N_353), .PD(ad_data0_23__N_309), .Q(n1952)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i14_1320_1321_set.GSR = "DISABLED";
    FD1P3AX sdi_66 (.D(n3241), .SP(pre_clk_enable_6), .CK(pre_clk), .Q(sdi_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam sdi_66.GSR = "DISABLED";
    FD1P3JX send_data_i23 (.D(send_data[22]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[23])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i23.GSR = "DISABLED";
    FD1P3JX send_data_i22 (.D(send_data[21]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[22])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i22.GSR = "DISABLED";
    FD1P3JX send_data_i21 (.D(send_data[20]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[21])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i21.GSR = "DISABLED";
    FD1P3JX send_data_i20 (.D(send_data[19]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[20])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i20.GSR = "DISABLED";
    FD1P3JX send_data_i19 (.D(send_data[18]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[19])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i19.GSR = "DISABLED";
    FD1P3JX send_data_i18 (.D(send_data[17]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[18])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i18.GSR = "DISABLED";
    FD1P3JX send_data_i17 (.D(send_data[16]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[17])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i17.GSR = "DISABLED";
    FD1P3JX send_data_i16 (.D(send_data[15]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[16])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i16.GSR = "DISABLED";
    FD1P3JX send_data_i15 (.D(send_data[14]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[15])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i15.GSR = "DISABLED";
    FD1P3JX send_data_i14 (.D(send_data[13]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[14])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i14.GSR = "DISABLED";
    FD1P3JX send_data_i13 (.D(send_data[12]), .SP(pre_clk_enable_17), .PD(\send_data_23__N_809[12] ), 
            .CK(pre_clk), .Q(send_data[13])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(100[7] 140[4])
    defparam send_data_i13.GSR = "DISABLED";
    FD1P3BX ad_data0_i15_1316_1317_set (.D(n1949), .SP(scki_N_353_enable_224), 
            .CK(scki_N_353), .PD(ad_data0_23__N_308), .Q(n1948)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i15_1316_1317_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i16_1312_1313_set (.D(n1945), .SP(scki_N_353_enable_225), 
            .CK(scki_N_353), .PD(ad_data0_23__N_307), .Q(n1944)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i16_1312_1313_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i17_1308_1309_set (.D(n1941), .SP(scki_N_353_enable_226), 
            .CK(scki_N_353), .PD(ad_data0_23__N_306), .Q(n1940)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i17_1308_1309_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i18_1304_1305_set (.D(n1937), .SP(scki_N_353_enable_227), 
            .CK(scki_N_353), .PD(ad_data0_23__N_305), .Q(n1936)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i18_1304_1305_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i19_1300_1301_set (.D(n1933), .SP(scki_N_353_enable_228), 
            .CK(scki_N_353), .PD(ad_data0_23__N_304), .Q(n1932)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i19_1300_1301_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i20_1296_1297_set (.D(n1929), .SP(scki_N_353_enable_229), 
            .CK(scki_N_353), .PD(ad_data0_23__N_303), .Q(n1928)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i20_1296_1297_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i21_1292_1293_set (.D(n1925), .SP(scki_N_353_enable_230), 
            .CK(scki_N_353), .PD(ad_data0_23__N_302), .Q(n1924)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i21_1292_1293_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i21_1016_1017_set (.D(n1649), .SP(scki_N_353_enable_231), 
            .CK(scki_N_353), .PD(ad_data3_23__N_677), .Q(n1648)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i21_1016_1017_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i22_1288_1289_set (.D(n1921), .SP(scki_N_353_enable_232), 
            .CK(scki_N_353), .PD(ad_data0_23__N_301), .Q(n1920)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i22_1288_1289_set.GSR = "DISABLED";
    FD1P3BX ad_data0_i23_1284_1285_set (.D(n1917), .SP(scki_N_353_enable_233), 
            .CK(scki_N_353), .PD(ad_data0_23__N_300), .Q(n1916)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data0_i23_1284_1285_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i1_1280_1281_set (.D(n1913), .SP(scki_N_353_enable_234), 
            .CK(scki_N_353), .PD(ad_data1_23__N_455), .Q(n1912)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i1_1280_1281_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i2_1276_1277_set (.D(n1909), .SP(scki_N_353_enable_235), 
            .CK(scki_N_353), .PD(ad_data1_23__N_454), .Q(n1908)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i2_1276_1277_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i3_1272_1273_set (.D(n1905), .SP(scki_N_353_enable_236), 
            .CK(scki_N_353), .PD(ad_data1_23__N_453), .Q(n1904)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i3_1272_1273_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i6_1168_1169_set (.D(n1801), .SP(scki_N_353_enable_237), 
            .CK(scki_N_353), .PD(ad_data2_23__N_571), .Q(n1800)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i6_1168_1169_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i4_1268_1269_set (.D(n1901), .SP(scki_N_353_enable_238), 
            .CK(scki_N_353), .PD(ad_data1_23__N_452), .Q(n1900)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i4_1268_1269_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i5_1080_1081_set (.D(n1713), .SP(scki_N_353_enable_239), 
            .CK(scki_N_353), .PD(ad_data3_23__N_693), .Q(n1712)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i5_1080_1081_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i0_996_997_set (.D(n1629), .SP(scki_N_353_enable_240), 
            .CK(scki_N_353), .PD(ad_data1_23__N_456), .Q(n1628)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i0_996_997_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i5_1264_1265_set (.D(n1897), .SP(scki_N_353_enable_241), 
            .CK(scki_N_353), .PD(ad_data1_23__N_451), .Q(n1896)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i5_1264_1265_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i6_1260_1261_set (.D(n1893), .SP(scki_N_353_enable_242), 
            .CK(scki_N_353), .PD(ad_data1_23__N_450), .Q(n1892)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i6_1260_1261_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i7_1164_1165_set (.D(n1797), .SP(scki_N_353_enable_243), 
            .CK(scki_N_353), .PD(ad_data2_23__N_570), .Q(n1796)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i7_1164_1165_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i7_1256_1257_set (.D(n1889), .SP(scki_N_353_enable_244), 
            .CK(scki_N_353), .PD(ad_data1_23__N_449), .Q(n1888)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i7_1256_1257_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i13_1048_1049_set (.D(n1681), .SP(scki_N_353_enable_245), 
            .CK(scki_N_353), .PD(ad_data3_23__N_685), .Q(n1680)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i13_1048_1049_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i8_1252_1253_set (.D(n1885), .SP(scki_N_353_enable_246), 
            .CK(scki_N_353), .PD(ad_data1_23__N_448), .Q(n1884)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i8_1252_1253_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i6_1076_1077_set (.D(n1709), .SP(scki_N_353_enable_247), 
            .CK(scki_N_353), .PD(ad_data3_23__N_692), .Q(n1708)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i6_1076_1077_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i9_1248_1249_set (.D(n1881), .SP(scki_N_353_enable_248), 
            .CK(scki_N_353), .PD(ad_data1_23__N_447), .Q(n1880)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i9_1248_1249_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i10_1244_1245_set (.D(n1877), .SP(scki_N_353_enable_249), 
            .CK(scki_N_353), .PD(ad_data1_23__N_446), .Q(n1876)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i10_1244_1245_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i7_1072_1073_set (.D(n1705), .SP(scki_N_353_enable_250), 
            .CK(scki_N_353), .PD(ad_data3_23__N_691), .Q(n1704)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i7_1072_1073_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i8_1160_1161_set (.D(n1793), .SP(scki_N_353_enable_251), 
            .CK(scki_N_353), .PD(ad_data2_23__N_569), .Q(n1792)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i8_1160_1161_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i11_1240_1241_set (.D(n1873), .SP(scki_N_353_enable_252), 
            .CK(scki_N_353), .PD(ad_data1_23__N_445), .Q(n1872)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i11_1240_1241_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i12_1236_1237_set (.D(n1869), .SP(scki_N_353_enable_253), 
            .CK(scki_N_353), .PD(ad_data1_23__N_444), .Q(n1868)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i12_1236_1237_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i14_1044_1045_set (.D(n1677), .SP(scki_N_353_enable_254), 
            .CK(scki_N_353), .PD(ad_data3_23__N_684), .Q(n1676)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i14_1044_1045_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i13_1232_1233_set (.D(n1865), .SP(scki_N_353_enable_255), 
            .CK(scki_N_353), .PD(ad_data1_23__N_443), .Q(n1864)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i13_1232_1233_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i14_1228_1229_set (.D(n1861), .SP(scki_N_353_enable_256), 
            .CK(scki_N_353), .PD(ad_data1_23__N_442), .Q(n1860)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i14_1228_1229_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i21_1108_1109_set (.D(n1741), .SP(scki_N_353_enable_257), 
            .CK(scki_N_353), .PD(ad_data2_23__N_556), .Q(n1740)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i21_1108_1109_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i15_1040_1041_set (.D(n1673), .SP(scki_N_353_enable_258), 
            .CK(scki_N_353), .PD(ad_data3_23__N_683), .Q(n1672)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i15_1040_1041_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i15_1224_1225_set (.D(n1857), .SP(scki_N_353_enable_259), 
            .CK(scki_N_353), .PD(ad_data1_23__N_441), .Q(n1856)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i15_1224_1225_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i9_1156_1157_set (.D(n1789), .SP(scki_N_353_enable_260), 
            .CK(scki_N_353), .PD(ad_data2_23__N_568), .Q(n1788)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i9_1156_1157_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i16_1220_1221_set (.D(n1853), .SP(scki_N_353_enable_261), 
            .CK(scki_N_353), .PD(ad_data1_23__N_440), .Q(n1852)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i16_1220_1221_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i8_1068_1069_set (.D(n1701), .SP(scki_N_353_enable_262), 
            .CK(scki_N_353), .PD(ad_data3_23__N_690), .Q(n1700)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i8_1068_1069_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i17_1216_1217_set (.D(n1849), .SP(scki_N_353_enable_263), 
            .CK(scki_N_353), .PD(ad_data1_23__N_439), .Q(n1848)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i17_1216_1217_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i10_1152_1153_set (.D(n1785), .SP(scki_N_353_enable_264), 
            .CK(scki_N_353), .PD(ad_data2_23__N_567), .Q(n1784)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i10_1152_1153_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i18_1212_1213_set (.D(n1845), .SP(scki_N_353_enable_265), 
            .CK(scki_N_353), .PD(ad_data1_23__N_438), .Q(n1844)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i18_1212_1213_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i9_1064_1065_set (.D(n1697), .SP(scki_N_353_enable_266), 
            .CK(scki_N_353), .PD(ad_data3_23__N_689), .Q(n1696)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i9_1064_1065_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i22_1104_1105_set (.D(n1737), .SP(scki_N_353_enable_267), 
            .CK(scki_N_353), .PD(ad_data2_23__N_555), .Q(n1736)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i22_1104_1105_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i11_1148_1149_set (.D(n1781), .SP(scki_N_353_enable_268), 
            .CK(scki_N_353), .PD(ad_data2_23__N_566), .Q(n1780)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i11_1148_1149_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i12_1144_1145_set (.D(n1777), .SP(scki_N_353_enable_269), 
            .CK(scki_N_353), .PD(ad_data2_23__N_565), .Q(n1776)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i12_1144_1145_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i13_1140_1141_set (.D(n1773), .SP(scki_N_353_enable_270), 
            .CK(scki_N_353), .PD(ad_data2_23__N_564), .Q(n1772)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i13_1140_1141_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i19_1208_1209_set (.D(n1841), .SP(scki_N_353_enable_271), 
            .CK(scki_N_353), .PD(ad_data1_23__N_437), .Q(n1840)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i19_1208_1209_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i10_1060_1061_set (.D(n1693), .SP(scki_N_353_enable_272), 
            .CK(scki_N_353), .PD(ad_data3_23__N_688), .Q(n1692)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i10_1060_1061_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i23_1100_1101_set (.D(n1733), .SP(scki_N_353_enable_273), 
            .CK(scki_N_353), .PD(ad_data2_23__N_554), .Q(n1732)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i23_1100_1101_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i14_1136_1137_set (.D(n1769), .SP(scki_N_353_enable_274), 
            .CK(scki_N_353), .PD(ad_data2_23__N_563), .Q(n1768)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i14_1136_1137_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i20_1204_1205_set (.D(n1837), .SP(scki_N_353_enable_275), 
            .CK(scki_N_353), .PD(ad_data1_23__N_436), .Q(n1836)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i20_1204_1205_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i11_1056_1057_set (.D(n1689), .SP(scki_N_353_enable_276), 
            .CK(scki_N_353), .PD(ad_data3_23__N_687), .Q(n1688)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i11_1056_1057_set.GSR = "DISABLED";
    FD1P3BX ad_data1_i21_1200_1201_set (.D(n1833), .SP(scki_N_353_enable_277), 
            .CK(scki_N_353), .PD(ad_data1_23__N_435), .Q(n1832)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data1_i21_1200_1201_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i12_1052_1053_set (.D(n1685), .SP(scki_N_353_enable_278), 
            .CK(scki_N_353), .PD(ad_data3_23__N_686), .Q(n1684)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i12_1052_1053_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i15_1132_1133_set (.D(n1765), .SP(scki_N_353_enable_279), 
            .CK(scki_N_353), .PD(ad_data2_23__N_562), .Q(n1764)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i15_1132_1133_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i1_1096_1097_set (.D(n1729), .SP(scki_N_353_enable_280), 
            .CK(scki_N_353), .PD(ad_data3_23__N_697), .Q(n1728)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i1_1096_1097_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i16_1128_1129_set (.D(n1761), .SP(scki_N_353_enable_281), 
            .CK(scki_N_353), .PD(ad_data2_23__N_561), .Q(n1760)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i16_1128_1129_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i2_1092_1093_set (.D(n1725), .SP(scki_N_353_enable_282), 
            .CK(scki_N_353), .PD(ad_data3_23__N_696), .Q(n1724)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i2_1092_1093_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i17_1124_1125_set (.D(n1757), .SP(scki_N_353_enable_283), 
            .CK(scki_N_353), .PD(ad_data2_23__N_560), .Q(n1756)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i17_1124_1125_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i3_1088_1089_set (.D(n1721), .SP(scki_N_353_enable_284), 
            .CK(scki_N_353), .PD(ad_data3_23__N_695), .Q(n1720)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i3_1088_1089_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i18_1120_1121_set (.D(n1753), .SP(scki_N_353_enable_285), 
            .CK(scki_N_353), .PD(ad_data2_23__N_559), .Q(n1752)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i18_1120_1121_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i16_1036_1037_set (.D(n1669), .SP(scki_N_353_enable_286), 
            .CK(scki_N_353), .PD(ad_data3_23__N_682), .Q(n1668)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i16_1036_1037_set.GSR = "DISABLED";
    FD1P3BX ad_data3_i4_1084_1085_set (.D(n1717), .SP(scki_N_353_enable_287), 
            .CK(scki_N_353), .PD(ad_data3_23__N_694), .Q(n1716)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data3_i4_1084_1085_set.GSR = "DISABLED";
    FD1P3BX ad_data2_i19_1116_1117_set (.D(n1749), .SP(scki_N_353_enable_288), 
            .CK(scki_N_353), .PD(ad_data2_23__N_558), .Q(n1748)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=16, LSE_RCOL=2, LSE_LLINE=84, LSE_RLINE=101 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam ad_data2_i19_1116_1117_set.GSR = "DISABLED";
    LUT4 busy_I_0_483_2_lut (.A(busy_c), .B(rec_r2[7]), .Z(ad_data2_23__N_570)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_483_2_lut.init = 16'h8888;
    LUT4 busy_I_0_380_2_lut (.A(busy_c), .B(rec_r0[11]), .Z(ad_data0_23__N_312)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_380_2_lut.init = 16'h8888;
    LUT4 busy_I_0_507_2_lut (.A(busy_c), .B(rec_r2[7]), .Z(ad_data2_23__N_651)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_507_2_lut.init = 16'h2222;
    LUT4 busy_I_0_484_2_lut (.A(busy_c), .B(rec_r2[6]), .Z(ad_data2_23__N_571)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_484_2_lut.init = 16'h8888;
    LUT4 busy_I_0_508_2_lut (.A(busy_c), .B(rec_r2[6]), .Z(ad_data2_23__N_654)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_508_2_lut.init = 16'h2222;
    LUT4 busy_I_0_485_2_lut (.A(busy_c), .B(rec_r2[5]), .Z(ad_data2_23__N_572)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_485_2_lut.init = 16'h8888;
    LUT4 busy_I_0_486_2_lut (.A(busy_c), .B(rec_r2[4]), .Z(ad_data2_23__N_573)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_486_2_lut.init = 16'h8888;
    LUT4 busy_I_0_487_2_lut (.A(busy_c), .B(rec_r2[3]), .Z(ad_data2_23__N_574)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_487_2_lut.init = 16'h8888;
    LUT4 busy_I_0_488_2_lut (.A(busy_c), .B(rec_r2[2]), .Z(ad_data2_23__N_575)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_488_2_lut.init = 16'h8888;
    LUT4 i998_3_lut (.A(n1629), .B(n1628), .C(scki_N_353_enable_48), .Z(ad_data1[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i998_3_lut.init = 16'hcaca;
    LUT4 i994_3_lut (.A(n1625), .B(n1624), .C(scki_N_353_enable_38), .Z(ad_data0[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i994_3_lut.init = 16'hcaca;
    LUT4 i1086_3_lut (.A(n1717), .B(n1716), .C(scki_N_353_enable_220), 
         .Z(ad_data3[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1086_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_541_2_lut (.A(busy_c), .B(rec_r3[21]), .Z(ad_data3_23__N_730)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_541_2_lut.init = 16'h2222;
    LUT4 i1178_3_lut (.A(n1809), .B(n1808), .C(scki_N_353_enable_51), 
         .Z(ad_data2[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1178_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_542_2_lut (.A(busy_c), .B(rec_r3[20]), .Z(ad_data3_23__N_733)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_542_2_lut.init = 16'h2222;
    LUT4 i1038_3_lut (.A(n1669), .B(n1668), .C(scki_N_353_enable_202), 
         .Z(ad_data3[16])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1038_3_lut.init = 16'hcaca;
    LUT4 i1130_3_lut (.A(n1761), .B(n1760), .C(scki_N_353_enable_215), 
         .Z(ad_data2[16])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1130_3_lut.init = 16'hcaca;
    LUT4 i1082_3_lut (.A(n1713), .B(n1712), .C(scki_N_353_enable_22), 
         .Z(ad_data3[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1082_3_lut.init = 16'hcaca;
    LUT4 i1174_3_lut (.A(n1805), .B(n1804), .C(scki_N_353_enable_56), 
         .Z(ad_data2[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1174_3_lut.init = 16'hcaca;
    LUT4 i1078_3_lut (.A(n1709), .B(n1708), .C(scki_N_353_enable_180), 
         .Z(ad_data3[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1078_3_lut.init = 16'hcaca;
    LUT4 i1170_3_lut (.A(n1801), .B(n1800), .C(scki_N_353_enable_171), 
         .Z(ad_data2[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1170_3_lut.init = 16'hcaca;
    LUT4 i1034_3_lut (.A(n1665), .B(n1664), .C(scki_N_353_enable_29), 
         .Z(ad_data3[17])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1034_3_lut.init = 16'hcaca;
    LUT4 i1126_3_lut (.A(n1757), .B(n1756), .C(scki_N_353_enable_217), 
         .Z(ad_data2[17])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1126_3_lut.init = 16'hcaca;
    LUT4 i1074_3_lut (.A(n1705), .B(n1704), .C(scki_N_353_enable_183), 
         .Z(ad_data3[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1074_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_519_2_lut (.A(busy_c), .B(rec_r3[19]), .Z(ad_data3_23__N_679)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_519_2_lut.init = 16'h8888;
    LUT4 i1166_3_lut (.A(n1797), .B(n1796), .C(scki_N_353_enable_175), 
         .Z(ad_data2[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1166_3_lut.init = 16'hcaca;
    LUT4 i1070_3_lut (.A(n1701), .B(n1700), .C(scki_N_353_enable_195), 
         .Z(ad_data3[8])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1070_3_lut.init = 16'hcaca;
    LUT4 i1030_3_lut (.A(n1661), .B(n1660), .C(scki_N_353_enable_11), 
         .Z(ad_data3[18])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1030_3_lut.init = 16'hcaca;
    LUT4 i1399_1_lut (.A(scki_N_353_enable_44), .Z(scki_N_353_enable_55)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1399_1_lut.init = 16'h5555;
    LUT4 i1398_1_lut (.A(scki_N_353_enable_11), .Z(scki_N_353_enable_105)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1398_1_lut.init = 16'h5555;
    LUT4 i1162_3_lut (.A(n1793), .B(n1792), .C(scki_N_353_enable_184), 
         .Z(ad_data2[8])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1162_3_lut.init = 16'hcaca;
    LUT4 i1262_3_lut (.A(n1893), .B(n1892), .C(scki_N_353_enable_176), 
         .Z(ad_data1[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1262_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_381_2_lut (.A(busy_c), .B(rec_r0[10]), .Z(ad_data0_23__N_313)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_381_2_lut.init = 16'h8888;
    LUT4 i1354_3_lut (.A(n1985), .B(n1984), .C(scki_N_353_enable_36), 
         .Z(ad_data0[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1354_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_520_2_lut (.A(busy_c), .B(rec_r3[18]), .Z(ad_data3_23__N_680)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_520_2_lut.init = 16'h8888;
    LUT4 i1198_3_lut (.A(n1829), .B(n1828), .C(scki_N_353_enable_12), 
         .Z(ad_data1[22])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1198_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_522_2_lut (.A(busy_c), .B(rec_r3[16]), .Z(ad_data3_23__N_682)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_522_2_lut.init = 16'h8888;
    LUT4 i1290_3_lut (.A(n1921), .B(n1920), .C(scki_N_353_enable_144), 
         .Z(ad_data0[22])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1290_3_lut.init = 16'hcaca;
    LUT4 i1202_3_lut (.A(n1833), .B(n1832), .C(scki_N_353_enable_211), 
         .Z(ad_data1[21])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1202_3_lut.init = 16'hcaca;
    LUT4 i1294_3_lut (.A(n1925), .B(n1924), .C(scki_N_353_enable_111), 
         .Z(ad_data0[21])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1294_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_546_2_lut (.A(busy_c), .B(rec_r3[16]), .Z(ad_data3_23__N_745)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_546_2_lut.init = 16'h2222;
    LUT4 i1206_3_lut (.A(n1837), .B(n1836), .C(scki_N_353_enable_208), 
         .Z(ad_data1[20])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1206_3_lut.init = 16'hcaca;
    LUT4 i1298_3_lut (.A(n1929), .B(n1928), .C(scki_N_353_enable_110), 
         .Z(ad_data0[20])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1298_3_lut.init = 16'hcaca;
    LUT4 i1210_3_lut (.A(n1841), .B(n1840), .C(scki_N_353_enable_203), 
         .Z(ad_data1[19])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1210_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_509_2_lut (.A(busy_c), .B(rec_r2[5]), .Z(ad_data2_23__N_657)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_509_2_lut.init = 16'h2222;
    LUT4 i1302_3_lut (.A(n1933), .B(n1932), .C(scki_N_353_enable_109), 
         .Z(ad_data0[19])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1302_3_lut.init = 16'hcaca;
    LUT4 i1362_3_lut (.A(n1993), .B(n1992), .C(scki_N_353_enable_32), 
         .Z(ad_data0[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1362_3_lut.init = 16'hcaca;
    LUT4 i1214_3_lut (.A(n1845), .B(n1844), .C(scki_N_353_enable_200), 
         .Z(ad_data1[18])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1214_3_lut.init = 16'hcaca;
    LUT4 i1306_3_lut (.A(n1937), .B(n1936), .C(scki_N_353_enable_108), 
         .Z(ad_data0[18])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1306_3_lut.init = 16'hcaca;
    LUT4 i1218_3_lut (.A(n1849), .B(n1848), .C(scki_N_353_enable_196), 
         .Z(ad_data1[17])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1218_3_lut.init = 16'hcaca;
    LUT4 i1310_3_lut (.A(n1941), .B(n1940), .C(scki_N_353_enable_107), 
         .Z(ad_data0[17])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1310_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_404_2_lut (.A(busy_c), .B(rec_r0[14]), .Z(ad_data0_23__N_388)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_404_2_lut.init = 16'h2222;
    LUT4 i1266_3_lut (.A(n1897), .B(n1896), .C(scki_N_353_enable_174), 
         .Z(ad_data1[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1266_3_lut.init = 16'hcaca;
    LUT4 busy_I_0_523_2_lut (.A(busy_c), .B(rec_r3[15]), .Z(ad_data3_23__N_683)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam busy_I_0_523_2_lut.init = 16'h8888;
    LUT4 i1222_3_lut (.A(n1853), .B(n1852), .C(scki_N_353_enable_194), 
         .Z(ad_data1[16])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/ltc2357_master.v(158[7] 163[5])
    defparam i1222_3_lut.init = 16'hcaca;
    
endmodule
//
// Verilog Description of module \divide(WIDTH=10,N=3) 
//

module \divide(WIDTH=10,N=3)  (clk, n3647, clk_N_242, pre_clk, GND_net, 
            n3657, \nst_2__N_277[1] , \cst[2] , \cst[0] , y_N_295) /* synthesis syn_module_defined=1 */ ;
    input clk;
    input n3647;
    input clk_N_242;
    output pre_clk;
    input GND_net;
    input n3657;
    input \nst_2__N_277[1] ;
    input \cst[2] ;
    input \cst[0] ;
    output y_N_295;
    
    wire clk /* synthesis SET_AS_NETWORK=clk, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(21[6:9])
    wire clk_N_242 /* synthesis is_inv_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[25:30])
    wire pre_clk /* synthesis is_clock=1, SET_AS_NETWORK=pre_clk */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(46[6:13])
    
    wire clk_p, clk_p_N_266;
    wire [9:0]cnt_n;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[25:30])
    wire [9:0]n46;
    
    wire clk_n, n3356;
    wire [9:0]n35;
    
    wire clk_n_N_269, n3187;
    wire [9:0]cnt_p;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[19:24])
    wire [9:0]n35_adj_1011;
    
    wire n3186, n3185;
    wire [9:0]n46_adj_1012;
    
    wire n3184, n3183, n3164, n3165, n14, n3163, n3365, n3166, 
        n15, n14_adj_1009, n3162, n15_adj_1010;
    
    FD1S3DX clk_p_30 (.D(clk_p_N_266), .CK(clk), .CD(n3647), .Q(clk_p)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=3, LSE_RCOL=2, LSE_LLINE=50, LSE_RLINE=54 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(46[9] 49[14])
    defparam clk_p_30.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i0 (.D(n46[0]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i0.GSR = "DISABLED";
    LUT4 clk_p_I_0_2_lut (.A(clk_p), .B(clk_n), .Z(pre_clk)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(73[43:56])
    defparam clk_p_I_0_2_lut.init = 16'h8888;
    LUT4 i1_2_lut_3_lut (.A(cnt_n[1]), .B(n3356), .C(n35[0]), .Z(n46[0])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_24 (.A(cnt_n[1]), .B(n3356), .C(n35[9]), .Z(n46[9])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_24.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_25 (.A(cnt_n[1]), .B(n3356), .C(n35[8]), .Z(n46[8])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_25.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_26 (.A(cnt_n[1]), .B(n3356), .C(n35[7]), .Z(n46[7])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_26.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_27 (.A(cnt_n[1]), .B(n3356), .C(n35[6]), .Z(n46[6])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_27.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_28 (.A(cnt_n[1]), .B(n3356), .C(n35[3]), .Z(n46[3])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_28.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_29 (.A(cnt_n[1]), .B(n3356), .C(n35[5]), .Z(n46[5])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_29.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_30 (.A(cnt_n[1]), .B(n3356), .C(n35[2]), .Z(n46[2])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_30.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_31 (.A(cnt_n[1]), .B(n3356), .C(n35[1]), .Z(n46[1])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_31.init = 16'hd0d0;
    FD1S3IX clk_n_32 (.D(clk_n_N_269), .CK(clk_N_242), .CD(n3647), .Q(clk_n));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(63[9] 71[6])
    defparam clk_n_32.GSR = "DISABLED";
    CCU2D cnt_p_323_add_4_11 (.A0(cnt_p[9]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3187), .S0(n35_adj_1011[9]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_p_323_add_4_11.INIT1 = 16'h0000;
    defparam cnt_p_323_add_4_11.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_11.INJECT1_1 = "NO";
    CCU2D cnt_p_323_add_4_9 (.A0(cnt_p[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3186), .COUT(n3187), .S0(n35_adj_1011[7]), .S1(n35_adj_1011[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_p_323_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_p_323_add_4_9.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_9.INJECT1_1 = "NO";
    CCU2D cnt_p_323_add_4_7 (.A0(cnt_p[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3185), .COUT(n3186), .S0(n35_adj_1011[5]), .S1(n35_adj_1011[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_p_323_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_p_323_add_4_7.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_7.INJECT1_1 = "NO";
    FD1S3DX cnt_p_323__i0 (.D(n46_adj_1012[0]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i0.GSR = "DISABLED";
    CCU2D cnt_p_323_add_4_5 (.A0(cnt_p[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3184), .COUT(n3185), .S0(n35_adj_1011[3]), .S1(n35_adj_1011[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_p_323_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_p_323_add_4_5.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_5.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_32 (.A(cnt_n[1]), .B(n3356), .C(n35[4]), .Z(n46[4])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut_3_lut_adj_32.init = 16'hd0d0;
    CCU2D cnt_p_323_add_4_3 (.A0(cnt_p[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3183), .COUT(n3184), .S0(n35_adj_1011[1]), .S1(n35_adj_1011[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_p_323_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_p_323_add_4_3.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_3.INJECT1_1 = "NO";
    CCU2D cnt_n_322_add_4_7 (.A0(cnt_n[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_n[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3164), .COUT(n3165), .S0(n35[5]), .S1(n35[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_n_322_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_n_322_add_4_7.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_7.INJECT1_1 = "NO";
    CCU2D cnt_p_323_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .COUT(n3183), .S1(n35_adj_1011[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323_add_4_1.INIT0 = 16'hF000;
    defparam cnt_p_323_add_4_1.INIT1 = 16'h0555;
    defparam cnt_p_323_add_4_1.INJECT1_0 = "NO";
    defparam cnt_p_323_add_4_1.INJECT1_1 = "NO";
    LUT4 i5_3_lut (.A(cnt_p[8]), .B(cnt_p[5]), .C(cnt_p[6]), .Z(n14)) /* synthesis lut_function=(A+(B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i5_3_lut.init = 16'hfefe;
    CCU2D cnt_n_322_add_4_5 (.A0(cnt_n[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_n[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3163), .COUT(n3164), .S0(n35[3]), .S1(n35[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_n_322_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_n_322_add_4_5.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_5.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_33 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[3]), 
         .Z(n46_adj_1012[3])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_33.init = 16'hd0d0;
    FD1S3DX cnt_n_322__i9 (.D(n46[9]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i9.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i8 (.D(n46[8]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i8.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i7 (.D(n46[7]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i7.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i6 (.D(n46[6]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i6.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i5 (.D(n46[5]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i5.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i4 (.D(n46[4]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i4.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i3 (.D(n46[3]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i3.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i2 (.D(n46[2]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i2.GSR = "DISABLED";
    FD1S3DX cnt_n_322__i1 (.D(n46[1]), .CK(clk_N_242), .CD(n3647), .Q(cnt_n[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322__i1.GSR = "DISABLED";
    CCU2D cnt_n_322_add_4_11 (.A0(cnt_n[9]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3166), .S0(n35[9]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_n_322_add_4_11.INIT1 = 16'h0000;
    defparam cnt_n_322_add_4_11.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_11.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_34 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[2]), 
         .Z(n46_adj_1012[2])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_34.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_35 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[4]), 
         .Z(n46_adj_1012[4])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_35.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_36 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[5]), 
         .Z(n46_adj_1012[5])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_36.init = 16'hd0d0;
    FD1S3DX cnt_p_323__i1 (.D(n46_adj_1012[1]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i1.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i2 (.D(n46_adj_1012[2]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i2.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i3 (.D(n46_adj_1012[3]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i3.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i4 (.D(n46_adj_1012[4]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i4.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i5 (.D(n46_adj_1012[5]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i5.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i6 (.D(n46_adj_1012[6]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i6.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i7 (.D(n46_adj_1012[7]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i7.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i8 (.D(n46_adj_1012[8]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i8.GSR = "DISABLED";
    FD1S3DX cnt_p_323__i9 (.D(n46_adj_1012[9]), .CK(clk), .CD(n3647), 
            .Q(cnt_p[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_323__i9.GSR = "DISABLED";
    LUT4 i1_2_lut (.A(cnt_n[1]), .B(n3356), .Z(clk_n_N_269)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i1_2_lut.init = 16'heeee;
    LUT4 i1_2_lut_3_lut_adj_37 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[6]), 
         .Z(n46_adj_1012[6])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_37.init = 16'hd0d0;
    LUT4 i8_4_lut (.A(n15), .B(cnt_n[7]), .C(n14_adj_1009), .D(cnt_n[3]), 
         .Z(n3356)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i8_4_lut.init = 16'hfffe;
    LUT4 i6_4_lut (.A(cnt_n[0]), .B(cnt_n[4]), .C(cnt_n[2]), .D(cnt_n[9]), 
         .Z(n15)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i5_3_lut_adj_38 (.A(cnt_n[8]), .B(cnt_n[5]), .C(cnt_n[6]), .Z(n14_adj_1009)) /* synthesis lut_function=(A+(B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam i5_3_lut_adj_38.init = 16'hfefe;
    LUT4 i1_4_lut_4_lut (.A(n3657), .B(\nst_2__N_277[1] ), .C(\cst[2] ), 
         .D(\cst[0] ), .Z(y_N_295)) /* synthesis lut_function=((B (C)+!B (C+(D)))+!A) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(44[7:13])
    defparam i1_4_lut_4_lut.init = 16'hf7f5;
    CCU2D cnt_n_322_add_4_3 (.A0(cnt_n[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_n[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3162), .COUT(n3163), .S0(n35[1]), .S1(n35[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_n_322_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_n_322_add_4_3.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_3.INJECT1_1 = "NO";
    CCU2D cnt_n_322_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_n[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .COUT(n3162), .S1(n35[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_1.INIT0 = 16'hF000;
    defparam cnt_n_322_add_4_1.INIT1 = 16'h0555;
    defparam cnt_n_322_add_4_1.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_1.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_39 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[7]), 
         .Z(n46_adj_1012[7])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_39.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_40 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[8]), 
         .Z(n46_adj_1012[8])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_40.init = 16'hd0d0;
    CCU2D cnt_n_322_add_4_9 (.A0(cnt_n[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_n[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3165), .COUT(n3166), .S0(n35[7]), .S1(n35[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(59[16:23])
    defparam cnt_n_322_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_n_322_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_n_322_add_4_9.INJECT1_0 = "NO";
    defparam cnt_n_322_add_4_9.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_41 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[9]), 
         .Z(n46_adj_1012[9])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_41.init = 16'hd0d0;
    LUT4 i1_2_lut_adj_42 (.A(cnt_p[1]), .B(n3365), .Z(clk_p_N_266)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_adj_42.init = 16'heeee;
    LUT4 i8_4_lut_adj_43 (.A(n15_adj_1010), .B(cnt_p[7]), .C(n14), .D(cnt_p[3]), 
         .Z(n3365)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i8_4_lut_adj_43.init = 16'hfffe;
    LUT4 i6_4_lut_adj_44 (.A(cnt_p[0]), .B(cnt_p[4]), .C(cnt_p[2]), .D(cnt_p[9]), 
         .Z(n15_adj_1010)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i6_4_lut_adj_44.init = 16'hfffe;
    LUT4 i1_2_lut_3_lut_adj_45 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[0]), 
         .Z(n46_adj_1012[0])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_45.init = 16'hd0d0;
    LUT4 i1_2_lut_3_lut_adj_46 (.A(cnt_p[1]), .B(n3365), .C(n35_adj_1011[1]), 
         .Z(n46_adj_1012[1])) /* synthesis lut_function=(A (B (C))+!A (C)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_2_lut_3_lut_adj_46.init = 16'hd0d0;
    
endmodule
//
// Verilog Description of module Uart_Bus
//

module Uart_Bus (clk_in_c, rs232_tx_c, \tx_data[0] , \tx_data[4] , n3874, 
            rs232_rx_c, rx_data, GND_net) /* synthesis syn_module_defined=1 */ ;
    input clk_in_c;
    output rs232_tx_c;
    input \tx_data[0] ;
    input \tx_data[4] ;
    input n3874;
    input rs232_rx_c;
    output [7:0]rx_data;
    input GND_net;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire [3:0]num;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(138[14:17])
    
    wire clk_in_c_enable_73, n2968, clk_in_c_enable_5, rs232_tx_N_983;
    wire [9:0]tx_data_r;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(139[14:23])
    
    wire bps_en_tx, bps_en_rx, n3298, bps_clk_rx;
    
    Uart_Tx Uart_Tx_uut (.num({num}), .clk_in_c(clk_in_c), .clk_in_c_enable_73(clk_in_c_enable_73), 
            .n2968(n2968), .rs232_tx_c(rs232_tx_c), .clk_in_c_enable_5(clk_in_c_enable_5), 
            .rs232_tx_N_983(rs232_tx_N_983), .\tx_data_r[1] (tx_data_r[1]), 
            .\tx_data[0] (\tx_data[0] ), .bps_en_tx(bps_en_tx), .bps_en_rx(bps_en_rx), 
            .n3298(n3298), .\tx_data_r[5] (tx_data_r[5]), .\tx_data[4] (\tx_data[4] ), 
            .\tx_data_r[9] (tx_data_r[9]), .n3874(n3874)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(230[9] 239[2])
    Uart_Rx Uart_Rx_uut (.clk_in_c(clk_in_c), .rs232_rx_c(rs232_rx_c), .rx_data({rx_data}), 
            .bps_en_rx(bps_en_rx), .bps_clk_rx(bps_clk_rx)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(202[9] 210[2])
    Baud Baud_tx (.bps_en_tx(bps_en_tx), .n2968(n2968), .GND_net(GND_net), 
         .num({num}), .\tx_data_r[1] (tx_data_r[1]), .\tx_data_r[5] (tx_data_r[5]), 
         .clk_in_c(clk_in_c), .clk_in_c_enable_73(clk_in_c_enable_73), .\tx_data_r[9] (tx_data_r[9]), 
         .clk_in_c_enable_5(clk_in_c_enable_5), .rs232_tx_N_983(rs232_tx_N_983), 
         .n3298(n3298)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(221[1] 227[2])
    Baud_U0 Baud_rx (.clk_in_c(clk_in_c), .bps_clk_rx(bps_clk_rx), .GND_net(GND_net), 
            .bps_en_rx(bps_en_rx)) /* synthesis syn_module_defined=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(193[1] 199[2])
    
endmodule
//
// Verilog Description of module Uart_Tx
//

module Uart_Tx (num, clk_in_c, clk_in_c_enable_73, n2968, rs232_tx_c, 
            clk_in_c_enable_5, rs232_tx_N_983, \tx_data_r[1] , \tx_data[0] , 
            bps_en_tx, bps_en_rx, n3298, \tx_data_r[5] , \tx_data[4] , 
            \tx_data_r[9] , n3874) /* synthesis syn_module_defined=1 */ ;
    output [3:0]num;
    input clk_in_c;
    input clk_in_c_enable_73;
    input n2968;
    output rs232_tx_c;
    input clk_in_c_enable_5;
    input rs232_tx_N_983;
    output \tx_data_r[1] ;
    input \tx_data[0] ;
    output bps_en_tx;
    input bps_en_rx;
    input n3298;
    output \tx_data_r[5] ;
    input \tx_data[4] ;
    output \tx_data_r[9] ;
    input n3874;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire [3:0]n21;
    
    wire clk_in_c_enable_19, n1622, rx_bps_en_r;
    
    LUT4 i2538_1_lut (.A(num[0]), .Z(n21[0])) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam i2538_1_lut.init = 16'h5555;
    FD1P3IX num_327__i0 (.D(n21[0]), .SP(clk_in_c_enable_73), .CD(n2968), 
            .CK(clk_in_c), .Q(num[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam num_327__i0.GSR = "ENABLED";
    LUT4 i2540_2_lut (.A(num[1]), .B(num[0]), .Z(n21[1])) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam i2540_2_lut.init = 16'h6666;
    FD1P3AY rs232_tx_34 (.D(rs232_tx_N_983), .SP(clk_in_c_enable_5), .CK(clk_in_c), 
            .Q(rs232_tx_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(158[11] 164[5])
    defparam rs232_tx_34.GSR = "ENABLED";
    FD1P3AX tx_data_r__i1 (.D(\tx_data[0] ), .SP(clk_in_c_enable_19), .CK(clk_in_c), 
            .Q(\tx_data_r[1] )) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=230, LSE_RLINE=239 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(145[11] 150[5])
    defparam tx_data_r__i1.GSR = "ENABLED";
    FD1S3AX bps_en_30 (.D(n1622), .CK(clk_in_c), .Q(bps_en_tx)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=230, LSE_RLINE=239 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(145[11] 150[5])
    defparam bps_en_30.GSR = "ENABLED";
    FD1S3AX rx_bps_en_r_29 (.D(bps_en_rx), .CK(clk_in_c), .Q(rx_bps_en_r)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=230, LSE_RLINE=239 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(132[7:32])
    defparam rx_bps_en_r_29.GSR = "ENABLED";
    LUT4 i2554_3_lut_4_lut (.A(num[1]), .B(num[0]), .C(num[2]), .D(num[3]), 
         .Z(n21[3])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(D))+!A !(D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam i2554_3_lut_4_lut.init = 16'h7f80;
    LUT4 i2547_2_lut_3_lut (.A(num[1]), .B(num[0]), .C(num[2]), .Z(n21[2])) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam i2547_2_lut_3_lut.init = 16'h7878;
    LUT4 rx_bps_en_r_I_0_2_lut_rep_28 (.A(rx_bps_en_r), .B(bps_en_rx), .Z(clk_in_c_enable_19)) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(136[22:48])
    defparam rx_bps_en_r_I_0_2_lut_rep_28.init = 16'h2222;
    LUT4 i1_3_lut_4_lut (.A(rx_bps_en_r), .B(bps_en_rx), .C(bps_en_tx), 
         .D(n3298), .Z(n1622)) /* synthesis lut_function=(A ((C (D))+!B)+!A (C (D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(136[22:48])
    defparam i1_3_lut_4_lut.init = 16'hf222;
    FD1P3AX tx_data_r__i2 (.D(\tx_data[4] ), .SP(clk_in_c_enable_19), .CK(clk_in_c), 
            .Q(\tx_data_r[5] )) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=230, LSE_RLINE=239 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(145[11] 150[5])
    defparam tx_data_r__i2.GSR = "ENABLED";
    FD1P3AX tx_data_r__i3 (.D(n3874), .SP(clk_in_c_enable_19), .CK(clk_in_c), 
            .Q(\tx_data_r[9] )) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=230, LSE_RLINE=239 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(145[11] 150[5])
    defparam tx_data_r__i3.GSR = "ENABLED";
    FD1P3IX num_327__i3 (.D(n21[3]), .SP(clk_in_c_enable_73), .CD(n2968), 
            .CK(clk_in_c), .Q(num[3]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam num_327__i3.GSR = "ENABLED";
    FD1P3IX num_327__i2 (.D(n21[2]), .SP(clk_in_c_enable_73), .CD(n2968), 
            .CK(clk_in_c), .Q(num[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam num_327__i2.GSR = "ENABLED";
    FD1P3IX num_327__i1 (.D(n21[1]), .SP(clk_in_c_enable_73), .CD(n2968), 
            .CK(clk_in_c), .Q(num[1]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(160[11:21])
    defparam num_327__i1.GSR = "ENABLED";
    
endmodule
//
// Verilog Description of module Uart_Rx
//

module Uart_Rx (clk_in_c, rs232_rx_c, rx_data, bps_en_rx, bps_clk_rx) /* synthesis syn_module_defined=1 */ ;
    input clk_in_c;
    input rs232_rx_c;
    output [7:0]rx_data;
    output bps_en_rx;
    input bps_clk_rx;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire [3:0]num;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(81[14:17])
    
    wire clk_in_c_enable_63, n2014, n3662;
    wire [7:0]rx_data_r;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(92[14:23])
    
    wire clk_in_c_enable_2, clk_in_c_enable_15, n3652, n3661, n3649, 
        clk_in_c_enable_64, n7, clk_in_c_enable_69;
    wire [3:0]n21;
    
    wire n3664, clk_in_c_enable_67, n3665, rs232_rx1, rs232_rx0, rs232_rx2, 
        n1620, n3660, clk_in_c_enable_65, clk_in_c_enable_68, clk_in_c_enable_66, 
        clk_in_c_enable_70, n3377;
    
    FD1P3IX num_325__i0 (.D(n3662), .SP(clk_in_c_enable_63), .CD(n2014), 
            .CK(clk_in_c), .Q(num[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam num_325__i0.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i0 (.D(rs232_rx_c), .SP(clk_in_c_enable_2), .CK(clk_in_c), 
            .Q(rx_data_r[0])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i0.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i0 (.D(rx_data_r[0]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[0])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i0.GSR = "ENABLED";
    LUT4 i2_3_lut_rep_25_4_lut (.A(bps_en_rx), .B(bps_clk_rx), .C(num[3]), 
         .D(num[1]), .Z(n3652)) /* synthesis lut_function=(!(((C+!(D))+!B)+!A)) */ ;
    defparam i2_3_lut_rep_25_4_lut.init = 16'h0800;
    LUT4 i1_2_lut_rep_34 (.A(num[0]), .B(num[2]), .Z(n3661)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_rep_34.init = 16'heeee;
    LUT4 i2953_2_lut_2_lut_3_lut (.A(num[0]), .B(num[2]), .C(n3649), .Z(clk_in_c_enable_64)) /* synthesis lut_function=(!(A+(B+(C)))) */ ;
    defparam i2953_2_lut_2_lut_3_lut.init = 16'h0101;
    LUT4 i366_1_lut_rep_35 (.A(num[0]), .Z(n3662)) /* synthesis lut_function=(!(A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(102[7:16])
    defparam i366_1_lut_rep_35.init = 16'h5555;
    LUT4 i2937_2_lut_2_lut_3_lut_3_lut (.A(num[0]), .B(n3649), .C(num[2]), 
         .Z(clk_in_c_enable_2)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(102[7:16])
    defparam i2937_2_lut_2_lut_3_lut_3_lut.init = 16'h0202;
    LUT4 i2_3_lut_4_lut_4_lut (.A(num[0]), .B(num[3]), .C(num[1]), .D(num[2]), 
         .Z(n7)) /* synthesis lut_function=(((C+(D))+!B)+!A) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(102[7:16])
    defparam i2_3_lut_4_lut_4_lut.init = 16'hfff7;
    LUT4 i2962_2_lut_2_lut_3_lut_3_lut (.A(num[0]), .B(n3652), .C(num[2]), 
         .Z(clk_in_c_enable_69)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(102[7:16])
    defparam i2962_2_lut_2_lut_3_lut_3_lut.init = 16'h0808;
    LUT4 i2518_2_lut_3_lut (.A(num[1]), .B(num[0]), .C(num[2]), .Z(n21[2])) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam i2518_2_lut_3_lut.init = 16'h7878;
    LUT4 i2525_3_lut_4_lut (.A(num[1]), .B(num[0]), .C(num[2]), .D(num[3]), 
         .Z(n21[3])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(D))+!A !(D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam i2525_3_lut_4_lut.init = 16'h7f80;
    LUT4 i2048_2_lut_rep_37 (.A(num[0]), .B(num[2]), .Z(n3664)) /* synthesis lut_function=(A (B)) */ ;
    defparam i2048_2_lut_rep_37.init = 16'h8888;
    LUT4 i2957_2_lut_2_lut_3_lut (.A(num[0]), .B(num[2]), .C(n3649), .Z(clk_in_c_enable_67)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i2957_2_lut_2_lut_3_lut.init = 16'h0808;
    LUT4 i1_2_lut_rep_38 (.A(num[0]), .B(num[2]), .Z(n3665)) /* synthesis lut_function=(A+!(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(103[4:20])
    defparam i1_2_lut_rep_38.init = 16'hbbbb;
    FD1S3AX rs232_rx1_50 (.D(rs232_rx0), .CK(clk_in_c), .Q(rs232_rx1)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(71[11] 75[5])
    defparam rs232_rx1_50.GSR = "ENABLED";
    LUT4 i2_3_lut (.A(bps_en_rx), .B(bps_clk_rx), .C(n7), .Z(clk_in_c_enable_15)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam i2_3_lut.init = 16'h0202;
    FD1S3AX rs232_rx2_51 (.D(rs232_rx1), .CK(clk_in_c), .Q(rs232_rx2)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(71[11] 75[5])
    defparam rs232_rx2_51.GSR = "ENABLED";
    FD1S3AX bps_en_52 (.D(n1620), .CK(clk_in_c), .Q(bps_en_rx)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(86[7] 89[18])
    defparam bps_en_52.GSR = "ENABLED";
    FD1S3AX rs232_rx0_49 (.D(rs232_rx_c), .CK(clk_in_c), .Q(rs232_rx0)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(71[11] 75[5])
    defparam rs232_rx0_49.GSR = "ENABLED";
    LUT4 i2511_2_lut (.A(num[1]), .B(num[0]), .Z(n21[1])) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam i2511_2_lut.init = 16'h6666;
    LUT4 i2052_2_lut_4_lut (.A(n3660), .B(num[1]), .C(num[3]), .D(n3664), 
         .Z(clk_in_c_enable_65)) /* synthesis lut_function=(!(((C+!(D))+!B)+!A)) */ ;
    defparam i2052_2_lut_4_lut.init = 16'h0800;
    FD1P3AX rx_data_i0_i7 (.D(rx_data_r[7]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[7])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i7.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i6 (.D(rx_data_r[6]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[6])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i6.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i5 (.D(rx_data_r[5]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[5])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i5.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i4 (.D(rx_data_r[4]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[4])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i4.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i3 (.D(rx_data_r[3]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[3])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i3.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i2 (.D(rx_data_r[2]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[2])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i2.GSR = "ENABLED";
    FD1P3AX rx_data_i0_i1 (.D(rx_data_r[1]), .SP(clk_in_c_enable_15), .CK(clk_in_c), 
            .Q(rx_data[1])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_i0_i1.GSR = "ENABLED";
    LUT4 i2960_2_lut_2_lut_3_lut (.A(num[0]), .B(num[2]), .C(n3649), .Z(clk_in_c_enable_68)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(103[4:20])
    defparam i2960_2_lut_2_lut_3_lut.init = 16'h0404;
    LUT4 i2955_2_lut_4_lut_4_lut (.A(n3665), .B(num[3]), .C(num[1]), .D(n3660), 
         .Z(clk_in_c_enable_66)) /* synthesis lut_function=(!(A+(B+!(C (D))))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(103[4:20])
    defparam i2955_2_lut_4_lut_4_lut.init = 16'h1000;
    LUT4 i2964_2_lut_4_lut_4_lut (.A(n3661), .B(num[3]), .C(num[1]), .D(n3660), 
         .Z(clk_in_c_enable_70)) /* synthesis lut_function=(!(A+(B+!(C (D))))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(103[4:20])
    defparam i2964_2_lut_4_lut_4_lut.init = 16'h1000;
    FD1P3IX num_325__i2 (.D(n21[2]), .SP(clk_in_c_enable_63), .CD(n2014), 
            .CK(clk_in_c), .Q(num[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam num_325__i2.GSR = "ENABLED";
    FD1P3IX num_325__i1 (.D(n21[1]), .SP(clk_in_c_enable_63), .CD(n2014), 
            .CK(clk_in_c), .Q(num[1]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam num_325__i1.GSR = "ENABLED";
    LUT4 i2950_3_lut_rep_15 (.A(n7), .B(bps_en_rx), .C(bps_clk_rx), .Z(clk_in_c_enable_63)) /* synthesis lut_function=(A (B (C))+!A (B)) */ ;
    defparam i2950_3_lut_rep_15.init = 16'hc4c4;
    LUT4 i2940_2_lut_3_lut (.A(n7), .B(bps_en_rx), .C(bps_clk_rx), .Z(n2014)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;
    defparam i2940_2_lut_3_lut.init = 16'h0404;
    LUT4 i988_4_lut (.A(n7), .B(n3377), .C(bps_en_rx), .D(rs232_rx1), 
         .Z(n1620)) /* synthesis lut_function=(A (B (C+(D))+!B (C))+!A !((C+!(D))+!B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(86[7] 89[18])
    defparam i988_4_lut.init = 16'haca0;
    LUT4 i2_3_lut_adj_23 (.A(rs232_rx_c), .B(rs232_rx0), .C(rs232_rx2), 
         .Z(n3377)) /* synthesis lut_function=(!(A+(B+!(C)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(86[10:35])
    defparam i2_3_lut_adj_23.init = 16'h1010;
    FD1P3IX num_325__i3 (.D(n21[3]), .SP(clk_in_c_enable_63), .CD(n2014), 
            .CK(clk_in_c), .Q(num[3]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(101[11:19])
    defparam num_325__i3.GSR = "ENABLED";
    LUT4 i1_2_lut_rep_33 (.A(bps_en_rx), .B(bps_clk_rx), .Z(n3660)) /* synthesis lut_function=(A (B)) */ ;
    defparam i1_2_lut_rep_33.init = 16'h8888;
    FD1P3AX rx_data_r_i0_i7 (.D(rs232_rx_c), .SP(clk_in_c_enable_64), .CK(clk_in_c), 
            .Q(rx_data_r[7])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i7.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i6 (.D(rs232_rx_c), .SP(clk_in_c_enable_65), .CK(clk_in_c), 
            .Q(rx_data_r[6])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i6.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i5 (.D(rs232_rx_c), .SP(clk_in_c_enable_66), .CK(clk_in_c), 
            .Q(rx_data_r[5])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i5.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i4 (.D(rs232_rx_c), .SP(clk_in_c_enable_67), .CK(clk_in_c), 
            .Q(rx_data_r[4])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i4.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i3 (.D(rs232_rx_c), .SP(clk_in_c_enable_68), .CK(clk_in_c), 
            .Q(rx_data_r[3])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i3.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i2 (.D(rs232_rx_c), .SP(clk_in_c_enable_69), .CK(clk_in_c), 
            .Q(rx_data_r[2])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i2.GSR = "ENABLED";
    FD1P3AX rx_data_r_i0_i1 (.D(rs232_rx_c), .SP(clk_in_c_enable_70), .CK(clk_in_c), 
            .Q(rx_data_r[1])) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=9, LSE_RCOL=2, LSE_LLINE=202, LSE_RLINE=210 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(99[11] 108[5])
    defparam rx_data_r_i0_i1.GSR = "ENABLED";
    LUT4 i1_4_lut_rep_22 (.A(num[1]), .B(n3661), .C(n3660), .D(num[3]), 
         .Z(n3649)) /* synthesis lut_function=(A+(B ((D)+!C)+!B !(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(103[4:20])
    defparam i1_4_lut_rep_22.init = 16'hefaf;
    
endmodule
//
// Verilog Description of module Baud
//

module Baud (bps_en_tx, n2968, GND_net, num, \tx_data_r[1] , \tx_data_r[5] , 
            clk_in_c, clk_in_c_enable_73, \tx_data_r[9] , clk_in_c_enable_5, 
            rs232_tx_N_983, n3298) /* synthesis syn_module_defined=1 */ ;
    input bps_en_tx;
    output n2968;
    input GND_net;
    input [3:0]num;
    input \tx_data_r[1] ;
    input \tx_data_r[5] ;
    input clk_in_c;
    output clk_in_c_enable_73;
    input \tx_data_r[9] ;
    output clk_in_c_enable_5;
    output rs232_tx_N_983;
    output n3298;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    
    wire bps_clk_tx, n4, n3192;
    wire [12:0]cnt;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(24[15:18])
    wire [12:0]n57;
    
    wire n3193, n3191, n3631, cnt_12__N_899, n3190, n3630, n3632, 
        n3189, n3188, bps_clk_N_903, n3629, n3656, n8, n3359, 
        n4_adj_988, n3389, n3249, n3391;
    
    LUT4 i1_3_lut (.A(bps_clk_tx), .B(bps_en_tx), .C(n4), .Z(n2968)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(40[8] 43[20])
    defparam i1_3_lut.init = 16'h4040;
    CCU2D cnt_326_add_4_11 (.A0(cnt[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3192), 
          .COUT(n3193), .S0(n57[9]), .S1(n57[10]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_11.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_11.INJECT1_0 = "NO";
    defparam cnt_326_add_4_11.INJECT1_1 = "NO";
    CCU2D cnt_326_add_4_9 (.A0(cnt[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3191), 
          .COUT(n3192), .S0(n57[7]), .S1(n57[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_9.INJECT1_0 = "NO";
    defparam cnt_326_add_4_9.INJECT1_1 = "NO";
    LUT4 i1_3_lut_adj_19 (.A(num[3]), .B(num[2]), .C(num[1]), .Z(n4)) /* synthesis lut_function=(A (B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(138[14:17])
    defparam i1_3_lut_adj_19.init = 16'ha8a8;
    LUT4 num_1__bdd_4_lut (.A(num[0]), .B(\tx_data_r[1] ), .C(\tx_data_r[5] ), 
         .D(num[2]), .Z(n3631)) /* synthesis lut_function=(A (B (C+!(D))+!B (C (D)))) */ ;
    defparam num_1__bdd_4_lut.init = 16'ha088;
    FD1S3IX cnt_326__i0 (.D(n57[0]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i0.GSR = "ENABLED";
    CCU2D cnt_326_add_4_7 (.A0(cnt[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3190), 
          .COUT(n3191), .S0(n57[5]), .S1(n57[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_7.INJECT1_0 = "NO";
    defparam cnt_326_add_4_7.INJECT1_1 = "NO";
    PFUMX i2979 (.BLUT(n3631), .ALUT(n3630), .C0(num[1]), .Z(n3632));
    CCU2D cnt_326_add_4_5 (.A0(cnt[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3189), 
          .COUT(n3190), .S0(n57[3]), .S1(n57[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_5.INJECT1_0 = "NO";
    defparam cnt_326_add_4_5.INJECT1_1 = "NO";
    CCU2D cnt_326_add_4_3 (.A0(cnt[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3188), 
          .COUT(n3189), .S0(n57[1]), .S1(n57[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_3.INJECT1_0 = "NO";
    defparam cnt_326_add_4_3.INJECT1_1 = "NO";
    CCU2D cnt_326_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n3188), 
          .S1(n57[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_1.INIT0 = 16'hF000;
    defparam cnt_326_add_4_1.INIT1 = 16'h0555;
    defparam cnt_326_add_4_1.INJECT1_0 = "NO";
    defparam cnt_326_add_4_1.INJECT1_1 = "NO";
    FD1S3AX bps_clk_17 (.D(bps_clk_N_903), .CK(clk_in_c), .Q(bps_clk_tx)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=1, LSE_RCOL=2, LSE_LLINE=221, LSE_RLINE=227 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(40[8] 43[20])
    defparam bps_clk_17.GSR = "ENABLED";
    LUT4 i2747_3_lut (.A(bps_en_tx), .B(n4), .C(bps_clk_tx), .Z(clk_in_c_enable_73)) /* synthesis lut_function=(A (B+(C))) */ ;
    defparam i2747_3_lut.init = 16'ha8a8;
    LUT4 num_3__bdd_4_lut (.A(num[1]), .B(num[0]), .C(num[2]), .D(\tx_data_r[9] ), 
         .Z(n3629)) /* synthesis lut_function=(!(A+((C+!(D))+!B))) */ ;
    defparam num_3__bdd_4_lut.init = 16'h0400;
    LUT4 i1_2_lut_rep_29 (.A(cnt[12]), .B(cnt[11]), .Z(n3656)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i1_2_lut_rep_29.init = 16'heeee;
    LUT4 i3_3_lut_4_lut (.A(cnt[12]), .B(cnt[11]), .C(cnt[10]), .D(cnt[8]), 
         .Z(n8)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i3_3_lut_4_lut.init = 16'hfffe;
    LUT4 i1_4_lut (.A(cnt[7]), .B(n3359), .C(cnt[0]), .D(cnt[4]), .Z(n4_adj_988)) /* synthesis lut_function=(A (B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(24[15:18])
    defparam i1_4_lut.init = 16'haaa8;
    LUT4 i2_4_lut (.A(n3389), .B(cnt[8]), .C(n4_adj_988), .D(cnt[9]), 
         .Z(n3249)) /* synthesis lut_function=(A (B+(C+(D)))+!A (B+(D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(24[15:18])
    defparam i2_4_lut.init = 16'hffec;
    LUT4 i2_3_lut (.A(cnt[1]), .B(cnt[3]), .C(cnt[2]), .Z(n3359)) /* synthesis lut_function=(A+(B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i2_3_lut.init = 16'hfefe;
    LUT4 i1_4_lut_adj_20 (.A(bps_en_tx), .B(n3656), .C(n3249), .D(cnt[10]), 
         .Z(cnt_12__N_899)) /* synthesis lut_function=((B+(C (D)))+!A) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i1_4_lut_adj_20.init = 16'hfddd;
    LUT4 i1_2_lut (.A(cnt[5]), .B(cnt[6]), .Z(n3389)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(24[15:18])
    defparam i1_2_lut.init = 16'h8888;
    FD1S3IX cnt_326__i1 (.D(n57[1]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i1.GSR = "ENABLED";
    LUT4 num_1__bdd_3_lut (.A(num[0]), .B(num[2]), .C(\tx_data_r[9] ), 
         .Z(n3630)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;
    defparam num_1__bdd_3_lut.init = 16'h4040;
    FD1S3IX cnt_326__i2 (.D(n57[2]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i2.GSR = "ENABLED";
    FD1S3IX cnt_326__i3 (.D(n57[3]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i3.GSR = "ENABLED";
    FD1S3IX cnt_326__i4 (.D(n57[4]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i4.GSR = "ENABLED";
    FD1S3IX cnt_326__i5 (.D(n57[5]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i5.GSR = "ENABLED";
    FD1S3IX cnt_326__i6 (.D(n57[6]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i6.GSR = "ENABLED";
    FD1S3IX cnt_326__i7 (.D(n57[7]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i7.GSR = "ENABLED";
    FD1S3IX cnt_326__i8 (.D(n57[8]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i8.GSR = "ENABLED";
    FD1S3IX cnt_326__i9 (.D(n57[9]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i9.GSR = "ENABLED";
    FD1S3IX cnt_326__i10 (.D(n57[10]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[10])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i10.GSR = "ENABLED";
    FD1S3IX cnt_326__i11 (.D(n57[11]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[11])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i11.GSR = "ENABLED";
    FD1S3IX cnt_326__i12 (.D(n57[12]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[12])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326__i12.GSR = "ENABLED";
    LUT4 i1_2_lut_adj_21 (.A(bps_clk_tx), .B(bps_en_tx), .Z(clk_in_c_enable_5)) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(40[8] 43[20])
    defparam i1_2_lut_adj_21.init = 16'h8888;
    LUT4 i2945_4_lut (.A(cnt[7]), .B(n3391), .C(n8), .D(n3359), .Z(bps_clk_N_903)) /* synthesis lut_function=(!(A+((C+(D))+!B))) */ ;
    defparam i2945_4_lut.init = 16'h0004;
    LUT4 i3_4_lut (.A(cnt[9]), .B(cnt[0]), .C(cnt[4]), .D(n3389), .Z(n3391)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i3_4_lut.init = 16'h8000;
    LUT4 n3632_bdd_3_lut (.A(n3632), .B(n3629), .C(num[3]), .Z(rs232_tx_N_983)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam n3632_bdd_3_lut.init = 16'hcaca;
    LUT4 i3_4_lut_adj_22 (.A(num[1]), .B(num[2]), .C(num[0]), .D(num[3]), 
         .Z(n3298)) /* synthesis lut_function=((B+(C+!(D)))+!A) */ ;
    defparam i3_4_lut_adj_22.init = 16'hfdff;
    CCU2D cnt_326_add_4_13 (.A0(cnt[11]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3193), .S0(n57[11]), .S1(n57[12]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_326_add_4_13.INIT0 = 16'hfaaa;
    defparam cnt_326_add_4_13.INIT1 = 16'hfaaa;
    defparam cnt_326_add_4_13.INJECT1_0 = "NO";
    defparam cnt_326_add_4_13.INJECT1_1 = "NO";
    
endmodule
//
// Verilog Description of module Baud_U0
//

module Baud_U0 (clk_in_c, bps_clk_rx, GND_net, bps_en_rx) /* synthesis syn_module_defined=1 */ ;
    input clk_in_c;
    output bps_clk_rx;
    input GND_net;
    input bps_en_rx;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire [12:0]cnt;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(24[15:18])
    
    wire cnt_12__N_899;
    wire [12:0]n57;
    
    wire bps_clk_N_903, n3653, n8, n3385, n3362, n3200, n3199, 
        n3198, n3197, n3196, n3195, n3265, n3383, n4;
    
    FD1S3IX cnt_328__i0 (.D(n57[0]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i0.GSR = "ENABLED";
    FD1S3AX bps_clk_17 (.D(bps_clk_N_903), .CK(clk_in_c), .Q(bps_clk_rx)) /* synthesis LSE_LINE_FILE_ID=9, LSE_LCOL=1, LSE_RCOL=2, LSE_LLINE=193, LSE_RLINE=199 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(40[8] 43[20])
    defparam bps_clk_17.GSR = "ENABLED";
    LUT4 i1_2_lut_rep_26 (.A(cnt[11]), .B(cnt[12]), .Z(n3653)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i1_2_lut_rep_26.init = 16'heeee;
    LUT4 i3_3_lut_4_lut (.A(cnt[11]), .B(cnt[12]), .C(cnt[10]), .D(cnt[7]), 
         .Z(n8)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i3_3_lut_4_lut.init = 16'hfffe;
    FD1S3IX cnt_328__i1 (.D(n57[1]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i1.GSR = "ENABLED";
    FD1S3IX cnt_328__i2 (.D(n57[2]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i2.GSR = "ENABLED";
    FD1S3IX cnt_328__i3 (.D(n57[3]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i3.GSR = "ENABLED";
    FD1S3IX cnt_328__i4 (.D(n57[4]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i4.GSR = "ENABLED";
    FD1S3IX cnt_328__i5 (.D(n57[5]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i5.GSR = "ENABLED";
    FD1S3IX cnt_328__i6 (.D(n57[6]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i6.GSR = "ENABLED";
    FD1S3IX cnt_328__i7 (.D(n57[7]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i7.GSR = "ENABLED";
    FD1S3IX cnt_328__i8 (.D(n57[8]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i8.GSR = "ENABLED";
    FD1S3IX cnt_328__i9 (.D(n57[9]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i9.GSR = "ENABLED";
    FD1S3IX cnt_328__i10 (.D(n57[10]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[10])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i10.GSR = "ENABLED";
    FD1S3IX cnt_328__i11 (.D(n57[11]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[11])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i11.GSR = "ENABLED";
    FD1S3IX cnt_328__i12 (.D(n57[12]), .CK(clk_in_c), .CD(cnt_12__N_899), 
            .Q(cnt[12])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328__i12.GSR = "ENABLED";
    LUT4 i2947_4_lut (.A(n3385), .B(cnt[8]), .C(n8), .D(n3362), .Z(bps_clk_N_903)) /* synthesis lut_function=(!((B+(C+(D)))+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(40[11:31])
    defparam i2947_4_lut.init = 16'h0002;
    CCU2D cnt_328_add_4_13 (.A0(cnt[11]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3200), .S0(n57[11]), .S1(n57[12]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_13.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_13.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_13.INJECT1_0 = "NO";
    defparam cnt_328_add_4_13.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_11 (.A0(cnt[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3199), 
          .COUT(n3200), .S0(n57[9]), .S1(n57[10]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_11.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_11.INJECT1_0 = "NO";
    defparam cnt_328_add_4_11.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_9 (.A0(cnt[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3198), 
          .COUT(n3199), .S0(n57[7]), .S1(n57[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_9.INJECT1_0 = "NO";
    defparam cnt_328_add_4_9.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_7 (.A0(cnt[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3197), 
          .COUT(n3198), .S0(n57[5]), .S1(n57[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_7.INJECT1_0 = "NO";
    defparam cnt_328_add_4_7.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_5 (.A0(cnt[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3196), 
          .COUT(n3197), .S0(n57[3]), .S1(n57[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_5.INJECT1_0 = "NO";
    defparam cnt_328_add_4_5.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_3 (.A0(cnt[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3195), 
          .COUT(n3196), .S0(n57[1]), .S1(n57[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_328_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_328_add_4_3.INJECT1_0 = "NO";
    defparam cnt_328_add_4_3.INJECT1_1 = "NO";
    CCU2D cnt_328_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n3195), 
          .S1(n57[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(32[10:20])
    defparam cnt_328_add_4_1.INIT0 = 16'hF000;
    defparam cnt_328_add_4_1.INIT1 = 16'h0555;
    defparam cnt_328_add_4_1.INJECT1_0 = "NO";
    defparam cnt_328_add_4_1.INJECT1_1 = "NO";
    LUT4 i2_4_lut (.A(n3653), .B(n3265), .C(bps_en_rx), .D(cnt[10]), 
         .Z(cnt_12__N_899)) /* synthesis lut_function=(A+(B ((D)+!C)+!B !(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i2_4_lut.init = 16'hefaf;
    LUT4 i2_4_lut_adj_18 (.A(cnt[9]), .B(cnt[8]), .C(n3383), .D(n4), 
         .Z(n3265)) /* synthesis lut_function=(A+(B+(C (D)))) */ ;
    defparam i2_4_lut_adj_18.init = 16'hfeee;
    LUT4 i1_4_lut (.A(n3362), .B(cnt[7]), .C(cnt[0]), .D(cnt[4]), .Z(n4)) /* synthesis lut_function=(A (B)+!A (B (C+(D)))) */ ;
    defparam i1_4_lut.init = 16'hccc8;
    LUT4 i2_3_lut (.A(cnt[1]), .B(cnt[2]), .C(cnt[3]), .Z(n3362)) /* synthesis lut_function=(A+(B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/Uart_Bus.v(29[10:40])
    defparam i2_3_lut.init = 16'hfefe;
    LUT4 i1_2_lut (.A(cnt[6]), .B(cnt[5]), .Z(n3383)) /* synthesis lut_function=(A (B)) */ ;
    defparam i1_2_lut.init = 16'h8888;
    LUT4 i3_4_lut (.A(cnt[9]), .B(cnt[0]), .C(cnt[4]), .D(n3383), .Z(n3385)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i3_4_lut.init = 16'h8000;
    
endmodule
//
// Verilog Description of module auto_rst
//

module auto_rst (rst_n_auto, clk_in_c, GND_net) /* synthesis syn_module_defined=1 */ ;
    output rst_n_auto;
    input clk_in_c;
    input GND_net;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    
    wire cnt_20__N_146, rst_n_N_148;
    wire [20:0]cnt;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(14[12:15])
    
    wire n18, n2649, n20, n16, n3645;
    wire [20:0]n156;
    
    wire clk_in_c_enable_62;
    wire [20:0]n89;
    
    wire n3368, n3648, n3211, n3210, n3209, n3208, n3207, n3206, 
        n3370, n3205, n3204, n3203, n3202;
    
    FD1S3JX rst_n_14 (.D(rst_n_N_148), .CK(clk_in_c), .PD(cnt_20__N_146), 
            .Q(rst_n_auto)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=10, LSE_RCOL=56, LSE_LLINE=24, LSE_RLINE=24 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(18[8] 30[4])
    defparam rst_n_14.GSR = "DISABLED";
    LUT4 i7_4_lut (.A(cnt[13]), .B(cnt[12]), .C(cnt[14]), .D(cnt[17]), 
         .Z(n18)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(19[6:17])
    defparam i7_4_lut.init = 16'hfffe;
    LUT4 i2020_2_lut (.A(cnt[2]), .B(cnt[3]), .Z(n2649)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i2020_2_lut.init = 16'heeee;
    LUT4 i10_4_lut_rep_18 (.A(cnt[11]), .B(n20), .C(n16), .D(cnt[19]), 
         .Z(n3645)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(19[6:17])
    defparam i10_4_lut_rep_18.init = 16'hfffe;
    FD1P3AX cnt_320__i0 (.D(n89[0]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(n156[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i0.GSR = "DISABLED";
    LUT4 i3_4_lut (.A(cnt[5]), .B(cnt[6]), .C(cnt[8]), .D(cnt[7]), .Z(n3368)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i3_4_lut.init = 16'h8000;
    LUT4 i2967_2_lut_2_lut (.A(n3648), .B(n3645), .Z(cnt_20__N_146)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i2967_2_lut_2_lut.init = 16'h1111;
    CCU2D cnt_320_add_4_21 (.A0(cnt[19]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[20]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3211), .S0(n89[19]), .S1(n89[20]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_21.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_21.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_21.INJECT1_0 = "NO";
    defparam cnt_320_add_4_21.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_19 (.A0(cnt[17]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[18]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3210), .COUT(n3211), .S0(n89[17]), .S1(n89[18]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_19.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_19.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_19.INJECT1_0 = "NO";
    defparam cnt_320_add_4_19.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_17 (.A0(cnt[15]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3209), .COUT(n3210), .S0(n89[15]), .S1(n89[16]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_17.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_17.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_17.INJECT1_0 = "NO";
    defparam cnt_320_add_4_17.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_15 (.A0(cnt[13]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3208), .COUT(n3209), .S0(n89[13]), .S1(n89[14]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_15.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_15.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_15.INJECT1_0 = "NO";
    defparam cnt_320_add_4_15.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_13 (.A0(cnt[11]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3207), .COUT(n3208), .S0(n89[11]), .S1(n89[12]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_13.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_13.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_13.INJECT1_0 = "NO";
    defparam cnt_320_add_4_13.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_11 (.A0(cnt[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3206), 
          .COUT(n3207), .S0(n89[9]), .S1(n89[10]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_11.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_11.INJECT1_0 = "NO";
    defparam cnt_320_add_4_11.INJECT1_1 = "NO";
    LUT4 i2091_2_lut (.A(n3370), .B(n3645), .Z(rst_n_N_148)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i2091_2_lut.init = 16'heeee;
    FD1P3AX cnt_320__i1 (.D(n89[1]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(n156[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i1.GSR = "DISABLED";
    FD1P3AX cnt_320__i2 (.D(n89[2]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i2.GSR = "DISABLED";
    FD1P3AX cnt_320__i3 (.D(n89[3]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i3.GSR = "DISABLED";
    FD1P3AX cnt_320__i4 (.D(n89[4]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i4.GSR = "DISABLED";
    FD1P3AX cnt_320__i5 (.D(n89[5]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i5.GSR = "DISABLED";
    FD1P3AX cnt_320__i6 (.D(n89[6]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i6.GSR = "DISABLED";
    FD1P3AX cnt_320__i7 (.D(n89[7]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i7.GSR = "DISABLED";
    FD1P3AX cnt_320__i8 (.D(n89[8]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i8.GSR = "DISABLED";
    FD1P3AX cnt_320__i9 (.D(n89[9]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i9.GSR = "DISABLED";
    FD1P3AX cnt_320__i10 (.D(n89[10]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[10])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i10.GSR = "DISABLED";
    FD1P3AX cnt_320__i11 (.D(n89[11]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[11])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i11.GSR = "DISABLED";
    FD1P3AX cnt_320__i12 (.D(n89[12]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[12])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i12.GSR = "DISABLED";
    FD1P3AX cnt_320__i13 (.D(n89[13]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[13])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i13.GSR = "DISABLED";
    FD1P3AX cnt_320__i14 (.D(n89[14]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[14])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i14.GSR = "DISABLED";
    FD1P3AX cnt_320__i15 (.D(n89[15]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[15])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i15.GSR = "DISABLED";
    FD1P3AX cnt_320__i16 (.D(n89[16]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[16])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i16.GSR = "DISABLED";
    FD1P3AX cnt_320__i17 (.D(n89[17]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[17])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i17.GSR = "DISABLED";
    FD1P3AX cnt_320__i18 (.D(n89[18]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[18])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i18.GSR = "DISABLED";
    FD1P3AX cnt_320__i19 (.D(n89[19]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[19])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i19.GSR = "DISABLED";
    FD1P3AX cnt_320__i20 (.D(n89[20]), .SP(clk_in_c_enable_62), .CK(clk_in_c), 
            .Q(cnt[20])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320__i20.GSR = "DISABLED";
    LUT4 i2_4_lut (.A(n3368), .B(cnt[3]), .C(cnt[9]), .D(cnt[4]), .Z(n3370)) /* synthesis lut_function=(A (B (C)+!B (C (D)))) */ ;
    defparam i2_4_lut.init = 16'ha080;
    CCU2D cnt_320_add_4_9 (.A0(cnt[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3205), 
          .COUT(n3206), .S0(n89[7]), .S1(n89[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_9.INJECT1_0 = "NO";
    defparam cnt_320_add_4_9.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_7 (.A0(cnt[5]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3204), 
          .COUT(n3205), .S0(n89[5]), .S1(n89[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_7.INJECT1_0 = "NO";
    defparam cnt_320_add_4_7.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_5 (.A0(cnt[3]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3203), 
          .COUT(n3204), .S0(n89[3]), .S1(n89[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_5.INJECT1_0 = "NO";
    defparam cnt_320_add_4_5.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_3 (.A0(n156[1]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3202), 
          .COUT(n3203), .S0(n89[1]), .S1(n89[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_320_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_320_add_4_3.INJECT1_0 = "NO";
    defparam cnt_320_add_4_3.INJECT1_1 = "NO";
    CCU2D cnt_320_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(n156[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n3202), 
          .S1(n89[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(25[10:17])
    defparam cnt_320_add_4_1.INIT0 = 16'hF000;
    defparam cnt_320_add_4_1.INIT1 = 16'h0555;
    defparam cnt_320_add_4_1.INJECT1_0 = "NO";
    defparam cnt_320_add_4_1.INJECT1_1 = "NO";
    LUT4 i9_4_lut (.A(cnt[18]), .B(n18), .C(cnt[16]), .D(cnt[20]), .Z(n20)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(19[6:17])
    defparam i9_4_lut.init = 16'hfffe;
    LUT4 i5_2_lut (.A(cnt[10]), .B(cnt[15]), .Z(n16)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/auto_rst.v(19[6:17])
    defparam i5_2_lut.init = 16'heeee;
    LUT4 i1_4_lut_rep_21 (.A(n3368), .B(cnt[9]), .C(n2649), .D(cnt[4]), 
         .Z(n3648)) /* synthesis lut_function=(A (B+(C (D)))+!A (B)) */ ;
    defparam i1_4_lut_rep_21.init = 16'heccc;
    LUT4 i2943_3_lut_3_lut (.A(n3645), .B(n3648), .C(n3370), .Z(clk_in_c_enable_62)) /* synthesis lut_function=(!(A+(B (C)))) */ ;
    defparam i2943_3_lut_3_lut.init = 16'h1515;
    
endmodule
//
// Verilog Description of module \divide(WIDTH=32,N=12000) 
//

module \divide(WIDTH=32,N=12000)  (GND_net, led1_c, clk_in_c, n3647) /* synthesis syn_module_defined=1 */ ;
    input GND_net;
    output led1_c;
    input clk_in_c;
    input n3647;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    
    wire n3172;
    wire [31:0]cnt_p;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[19:24])
    wire [31:0]n101;
    
    wire n3173, clkout_N_217;
    wire [31:0]n200;
    wire [31:0]n134;
    
    wire n28, n18, n34, n24, n38, n32_adj_986, n3167, n3168, 
        n3225, n3224, n3171, n3170, n3223, n3169, n63, n3222, 
        n3221, n3220, n3219, n3218, n3217, n3216, n3215, n3214, 
        n3213, n3182, n3181, n3180, n3179, n3178, n3177, n3293, 
        n15, n20, n16, n27, n40, n36, n3176, n3175, n3174;
    
    CCU2D cnt_p_321_add_4_13 (.A0(cnt_p[11]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[12]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3172), .COUT(n3173), .S0(n101[11]), .S1(n101[12]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_13.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_13.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_13.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_13.INJECT1_1 = "NO";
    FD1S3DX clk_p_29 (.D(clkout_N_217), .CK(clk_in_c), .CD(n3647), .Q(led1_c)) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=3, LSE_RCOL=2, LSE_LLINE=36, LSE_RLINE=40 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(46[9] 49[14])
    defparam clk_p_29.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i0 (.D(n134[0]), .CK(clk_in_c), .CD(n3647), .Q(n200[0])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i0.GSR = "DISABLED";
    LUT4 i7_2_lut (.A(cnt_p[17]), .B(cnt_p[24]), .Z(n28)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i7_2_lut.init = 16'heeee;
    LUT4 i7_4_lut (.A(cnt_p[13]), .B(n200[2]), .C(n200[3]), .D(n200[0]), 
         .Z(n18)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i7_4_lut.init = 16'h8000;
    LUT4 i17_4_lut (.A(cnt_p[29]), .B(n34), .C(n24), .D(cnt_p[14]), 
         .Z(n38)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i17_4_lut.init = 16'hfffe;
    LUT4 i11_3_lut (.A(cnt_p[22]), .B(cnt_p[21]), .C(cnt_p[31]), .Z(n32_adj_986)) /* synthesis lut_function=(A+(B+(C))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i11_3_lut.init = 16'hfefe;
    LUT4 i13_4_lut (.A(cnt_p[16]), .B(cnt_p[27]), .C(cnt_p[23]), .D(cnt_p[30]), 
         .Z(n34)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i13_4_lut.init = 16'hfffe;
    LUT4 i3_2_lut (.A(cnt_p[19]), .B(cnt_p[18]), .Z(n24)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i3_2_lut.init = 16'heeee;
    CCU2D cnt_p_321_add_4_3 (.A0(n200[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(n200[2]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3167), .COUT(n3168), .S0(n101[1]), .S1(n101[2]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_3.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_3.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_3.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_3.INJECT1_1 = "NO";
    CCU2D add_2498_28 (.A0(cnt_p[31]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3225), 
          .S1(clkout_N_217));
    defparam add_2498_28.INIT0 = 16'h5555;
    defparam add_2498_28.INIT1 = 16'h0000;
    defparam add_2498_28.INJECT1_0 = "NO";
    defparam add_2498_28.INJECT1_1 = "NO";
    CCU2D add_2498_26 (.A0(cnt_p[29]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[30]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3224), .COUT(n3225));
    defparam add_2498_26.INIT0 = 16'h5555;
    defparam add_2498_26.INIT1 = 16'h5555;
    defparam add_2498_26.INJECT1_0 = "NO";
    defparam add_2498_26.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_11 (.A0(cnt_p[9]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[10]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3171), .COUT(n3172), .S0(n101[9]), .S1(n101[10]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_11.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_11.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_11.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_11.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_9 (.A0(cnt_p[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3170), .COUT(n3171), .S0(n101[7]), .S1(n101[8]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_9.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_9.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_9.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_9.INJECT1_1 = "NO";
    CCU2D add_2498_24 (.A0(cnt_p[27]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[28]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3223), .COUT(n3224));
    defparam add_2498_24.INIT0 = 16'h5555;
    defparam add_2498_24.INIT1 = 16'h5555;
    defparam add_2498_24.INJECT1_0 = "NO";
    defparam add_2498_24.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_7 (.A0(cnt_p[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3169), .COUT(n3170), .S0(n101[5]), .S1(n101[6]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_7.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_7.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_7.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_7.INJECT1_1 = "NO";
    LUT4 i1982_2_lut (.A(n101[1]), .B(n63), .Z(n134[1])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1982_2_lut.init = 16'h8888;
    LUT4 i1981_2_lut (.A(n101[2]), .B(n63), .Z(n134[2])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1981_2_lut.init = 16'h8888;
    LUT4 i1980_2_lut (.A(n101[3]), .B(n63), .Z(n134[3])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1980_2_lut.init = 16'h8888;
    LUT4 i1979_2_lut (.A(n101[4]), .B(n63), .Z(n134[4])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1979_2_lut.init = 16'h8888;
    LUT4 i1978_2_lut (.A(n101[5]), .B(n63), .Z(n134[5])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1978_2_lut.init = 16'h8888;
    LUT4 i1977_2_lut (.A(n101[6]), .B(n63), .Z(n134[6])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1977_2_lut.init = 16'h8888;
    LUT4 i1976_2_lut (.A(n101[7]), .B(n63), .Z(n134[7])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1976_2_lut.init = 16'h8888;
    LUT4 i1975_2_lut (.A(n101[8]), .B(n63), .Z(n134[8])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1975_2_lut.init = 16'h8888;
    LUT4 i1974_2_lut (.A(n101[9]), .B(n63), .Z(n134[9])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1974_2_lut.init = 16'h8888;
    LUT4 i1973_2_lut (.A(n101[10]), .B(n63), .Z(n134[10])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1973_2_lut.init = 16'h8888;
    LUT4 i1972_2_lut (.A(n101[11]), .B(n63), .Z(n134[11])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1972_2_lut.init = 16'h8888;
    LUT4 i1971_2_lut (.A(n101[12]), .B(n63), .Z(n134[12])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1971_2_lut.init = 16'h8888;
    CCU2D cnt_p_321_add_4_5 (.A0(n200[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[4]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3168), .COUT(n3169), .S0(n101[3]), .S1(n101[4]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_5.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_5.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_5.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_5.INJECT1_1 = "NO";
    LUT4 i1970_2_lut (.A(n101[13]), .B(n63), .Z(n134[13])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1970_2_lut.init = 16'h8888;
    LUT4 i1969_2_lut (.A(n101[14]), .B(n63), .Z(n134[14])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1969_2_lut.init = 16'h8888;
    CCU2D add_2498_22 (.A0(cnt_p[25]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[26]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3222), .COUT(n3223));
    defparam add_2498_22.INIT0 = 16'h5555;
    defparam add_2498_22.INIT1 = 16'h5555;
    defparam add_2498_22.INJECT1_0 = "NO";
    defparam add_2498_22.INJECT1_1 = "NO";
    LUT4 i1968_2_lut (.A(n101[15]), .B(n63), .Z(n134[15])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1968_2_lut.init = 16'h8888;
    LUT4 i1967_2_lut (.A(n101[16]), .B(n63), .Z(n134[16])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1967_2_lut.init = 16'h8888;
    LUT4 i1966_2_lut (.A(n101[17]), .B(n63), .Z(n134[17])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1966_2_lut.init = 16'h8888;
    CCU2D add_2498_20 (.A0(cnt_p[23]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[24]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3221), .COUT(n3222));
    defparam add_2498_20.INIT0 = 16'h5555;
    defparam add_2498_20.INIT1 = 16'h5555;
    defparam add_2498_20.INJECT1_0 = "NO";
    defparam add_2498_20.INJECT1_1 = "NO";
    LUT4 i1965_2_lut (.A(n101[18]), .B(n63), .Z(n134[18])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1965_2_lut.init = 16'h8888;
    LUT4 i1964_2_lut (.A(n101[19]), .B(n63), .Z(n134[19])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1964_2_lut.init = 16'h8888;
    LUT4 i1963_2_lut (.A(n101[20]), .B(n63), .Z(n134[20])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1963_2_lut.init = 16'h8888;
    CCU2D add_2498_18 (.A0(cnt_p[21]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[22]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3220), .COUT(n3221));
    defparam add_2498_18.INIT0 = 16'h5555;
    defparam add_2498_18.INIT1 = 16'h5555;
    defparam add_2498_18.INJECT1_0 = "NO";
    defparam add_2498_18.INJECT1_1 = "NO";
    LUT4 i1962_2_lut (.A(n101[21]), .B(n63), .Z(n134[21])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1962_2_lut.init = 16'h8888;
    CCU2D add_2498_16 (.A0(cnt_p[19]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[20]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3219), .COUT(n3220));
    defparam add_2498_16.INIT0 = 16'h5555;
    defparam add_2498_16.INIT1 = 16'h5555;
    defparam add_2498_16.INJECT1_0 = "NO";
    defparam add_2498_16.INJECT1_1 = "NO";
    LUT4 i1961_2_lut (.A(n101[22]), .B(n63), .Z(n134[22])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1961_2_lut.init = 16'h8888;
    LUT4 i1960_2_lut (.A(n101[23]), .B(n63), .Z(n134[23])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1960_2_lut.init = 16'h8888;
    LUT4 i1959_2_lut (.A(n101[24]), .B(n63), .Z(n134[24])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1959_2_lut.init = 16'h8888;
    CCU2D add_2498_14 (.A0(cnt_p[17]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[18]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3218), .COUT(n3219));
    defparam add_2498_14.INIT0 = 16'h5555;
    defparam add_2498_14.INIT1 = 16'h5555;
    defparam add_2498_14.INJECT1_0 = "NO";
    defparam add_2498_14.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(n200[0]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .COUT(n3167), .S1(n101[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_1.INIT0 = 16'hF000;
    defparam cnt_p_321_add_4_1.INIT1 = 16'h0555;
    defparam cnt_p_321_add_4_1.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_1.INJECT1_1 = "NO";
    LUT4 i1958_2_lut (.A(n101[25]), .B(n63), .Z(n134[25])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1958_2_lut.init = 16'h8888;
    LUT4 i1957_2_lut (.A(n101[26]), .B(n63), .Z(n134[26])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1957_2_lut.init = 16'h8888;
    CCU2D add_2498_12 (.A0(cnt_p[15]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[16]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3217), .COUT(n3218));
    defparam add_2498_12.INIT0 = 16'h5555;
    defparam add_2498_12.INIT1 = 16'h5555;
    defparam add_2498_12.INJECT1_0 = "NO";
    defparam add_2498_12.INJECT1_1 = "NO";
    LUT4 i1956_2_lut (.A(n101[27]), .B(n63), .Z(n134[27])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1956_2_lut.init = 16'h8888;
    CCU2D add_2498_10 (.A0(cnt_p[13]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[14]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3216), .COUT(n3217));
    defparam add_2498_10.INIT0 = 16'h5555;
    defparam add_2498_10.INIT1 = 16'h5555;
    defparam add_2498_10.INJECT1_0 = "NO";
    defparam add_2498_10.INJECT1_1 = "NO";
    LUT4 i1955_2_lut (.A(n101[28]), .B(n63), .Z(n134[28])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1955_2_lut.init = 16'h8888;
    LUT4 i1954_2_lut (.A(n101[29]), .B(n63), .Z(n134[29])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1954_2_lut.init = 16'h8888;
    LUT4 i1953_2_lut (.A(n101[30]), .B(n63), .Z(n134[30])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1953_2_lut.init = 16'h8888;
    LUT4 i1952_2_lut (.A(n101[31]), .B(n63), .Z(n134[31])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1952_2_lut.init = 16'h8888;
    CCU2D add_2498_8 (.A0(cnt_p[11]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[12]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3215), .COUT(n3216));
    defparam add_2498_8.INIT0 = 16'h5555;
    defparam add_2498_8.INIT1 = 16'h5aaa;
    defparam add_2498_8.INJECT1_0 = "NO";
    defparam add_2498_8.INJECT1_1 = "NO";
    FD1S3DX cnt_p_321__i31 (.D(n134[31]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[31])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i31.GSR = "DISABLED";
    CCU2D add_2498_6 (.A0(cnt_p[9]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[10]), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3214), .COUT(n3215));
    defparam add_2498_6.INIT0 = 16'h5aaa;
    defparam add_2498_6.INIT1 = 16'h5aaa;
    defparam add_2498_6.INJECT1_0 = "NO";
    defparam add_2498_6.INJECT1_1 = "NO";
    CCU2D add_2498_4 (.A0(cnt_p[7]), .B0(GND_net), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[8]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .CIN(n3213), 
          .COUT(n3214));
    defparam add_2498_4.INIT0 = 16'h5555;
    defparam add_2498_4.INIT1 = 16'h5aaa;
    defparam add_2498_4.INJECT1_0 = "NO";
    defparam add_2498_4.INJECT1_1 = "NO";
    FD1S3DX cnt_p_321__i30 (.D(n134[30]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[30])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i30.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i29 (.D(n134[29]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[29])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i29.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i28 (.D(n134[28]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[28])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i28.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i27 (.D(n134[27]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[27])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i27.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i26 (.D(n134[26]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[26])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i26.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i25 (.D(n134[25]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[25])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i25.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i24 (.D(n134[24]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[24])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i24.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i23 (.D(n134[23]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[23])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i23.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i22 (.D(n134[22]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[22])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i22.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i21 (.D(n134[21]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[21])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i21.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i20 (.D(n134[20]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[20])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i20.GSR = "DISABLED";
    CCU2D add_2498_2 (.A0(cnt_p[5]), .B0(cnt_p[4]), .C0(GND_net), .D0(GND_net), 
          .A1(cnt_p[6]), .B1(GND_net), .C1(GND_net), .D1(GND_net), .COUT(n3213));
    defparam add_2498_2.INIT0 = 16'h7000;
    defparam add_2498_2.INIT1 = 16'h5aaa;
    defparam add_2498_2.INJECT1_0 = "NO";
    defparam add_2498_2.INJECT1_1 = "NO";
    FD1S3DX cnt_p_321__i19 (.D(n134[19]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[19])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i19.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i18 (.D(n134[18]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[18])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i18.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i17 (.D(n134[17]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[17])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i17.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i16 (.D(n134[16]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[16])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i16.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i15 (.D(n134[15]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[15])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i15.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i14 (.D(n134[14]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[14])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i14.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i13 (.D(n134[13]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[13])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i13.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i12 (.D(n134[12]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[12])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i12.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i11 (.D(n134[11]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[11])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i11.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i10 (.D(n134[10]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[10])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i10.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i9 (.D(n134[9]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[9])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i9.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i8 (.D(n134[8]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[8])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i8.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i7 (.D(n134[7]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[7])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i7.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i6 (.D(n134[6]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[6])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i6.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i5 (.D(n134[5]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[5])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i5.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i4 (.D(n134[4]), .CK(clk_in_c), .CD(n3647), .Q(cnt_p[4])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i4.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i3 (.D(n134[3]), .CK(clk_in_c), .CD(n3647), .Q(n200[3])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i3.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i2 (.D(n134[2]), .CK(clk_in_c), .CD(n3647), .Q(n200[2])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i2.GSR = "DISABLED";
    FD1S3DX cnt_p_321__i1 (.D(n134[1]), .CK(clk_in_c), .CD(n3647), .Q(n200[1])) /* synthesis syn_use_carry_chain=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321__i1.GSR = "DISABLED";
    CCU2D cnt_p_321_add_4_33 (.A0(cnt_p[31]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3182), .S0(n101[31]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_33.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_33.INIT1 = 16'h0000;
    defparam cnt_p_321_add_4_33.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_33.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_31 (.A0(cnt_p[29]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[30]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3181), .COUT(n3182), .S0(n101[29]), .S1(n101[30]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_31.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_31.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_31.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_31.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_29 (.A0(cnt_p[27]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[28]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3180), .COUT(n3181), .S0(n101[27]), .S1(n101[28]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_29.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_29.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_29.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_29.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_27 (.A0(cnt_p[25]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[26]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3179), .COUT(n3180), .S0(n101[25]), .S1(n101[26]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_27.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_27.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_27.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_27.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_25 (.A0(cnt_p[23]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[24]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3178), .COUT(n3179), .S0(n101[23]), .S1(n101[24]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_25.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_25.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_25.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_25.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_23 (.A0(cnt_p[21]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[22]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3177), .COUT(n3178), .S0(n101[21]), .S1(n101[22]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_23.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_23.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_23.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_23.INJECT1_1 = "NO";
    LUT4 i1929_2_lut (.A(n101[0]), .B(n63), .Z(n134[0])) /* synthesis lut_function=(A (B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1929_2_lut.init = 16'h8888;
    LUT4 i1_4_lut (.A(n3293), .B(n15), .C(n20), .D(n16), .Z(n63)) /* synthesis lut_function=(A+!(B (C (D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i1_4_lut.init = 16'hbfff;
    LUT4 i20_4_lut (.A(n27), .B(n40), .C(n36), .D(n28), .Z(n3293)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i20_4_lut.init = 16'hfffe;
    LUT4 i4_2_lut (.A(cnt_p[11]), .B(cnt_p[10]), .Z(n15)) /* synthesis lut_function=(A (B)) */ ;
    defparam i4_2_lut.init = 16'h8888;
    LUT4 i9_4_lut (.A(cnt_p[9]), .B(n18), .C(cnt_p[6]), .D(cnt_p[7]), 
         .Z(n20)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i9_4_lut.init = 16'h8000;
    LUT4 i5_2_lut (.A(n200[1]), .B(cnt_p[4]), .Z(n16)) /* synthesis lut_function=(A (B)) */ ;
    defparam i5_2_lut.init = 16'h8888;
    LUT4 i6_2_lut (.A(cnt_p[28]), .B(cnt_p[12]), .Z(n27)) /* synthesis lut_function=(A+(B)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i6_2_lut.init = 16'heeee;
    LUT4 i19_4_lut (.A(cnt_p[5]), .B(n38), .C(n32_adj_986), .D(cnt_p[20]), 
         .Z(n40)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i19_4_lut.init = 16'hfffe;
    CCU2D cnt_p_321_add_4_21 (.A0(cnt_p[19]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[20]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3176), .COUT(n3177), .S0(n101[19]), .S1(n101[20]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_21.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_21.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_21.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_21.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_19 (.A0(cnt_p[17]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[18]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3175), .COUT(n3176), .S0(n101[17]), .S1(n101[18]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_19.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_19.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_19.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_19.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_17 (.A0(cnt_p[15]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[16]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3174), .COUT(n3175), .S0(n101[15]), .S1(n101[16]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_17.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_17.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_17.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_17.INJECT1_1 = "NO";
    CCU2D cnt_p_321_add_4_15 (.A0(cnt_p[13]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(cnt_p[14]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3173), .COUT(n3174), .S0(n101[13]), .S1(n101[14]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam cnt_p_321_add_4_15.INIT0 = 16'hfaaa;
    defparam cnt_p_321_add_4_15.INIT1 = 16'hfaaa;
    defparam cnt_p_321_add_4_15.INJECT1_0 = "NO";
    defparam cnt_p_321_add_4_15.INJECT1_1 = "NO";
    LUT4 i15_4_lut (.A(cnt_p[8]), .B(cnt_p[25]), .C(cnt_p[15]), .D(cnt_p[26]), 
         .Z(n36)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(38[16:23])
    defparam i15_4_lut.init = 16'hfffe;
    
endmodule
//
// Verilog Description of module pll_clk
//

module pll_clk (clk_in_c, clk, GND_net, clk_N_242) /* synthesis NGD_DRC_MASK=1, syn_module_defined=1 */ ;
    input clk_in_c;
    output clk;
    input GND_net;
    output clk_N_242;
    
    wire clk_in_c /* synthesis SET_AS_NETWORK=clk_in_c, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(3[8:14])
    wire clk /* synthesis SET_AS_NETWORK=clk, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(21[6:9])
    wire clk_N_242 /* synthesis is_inv_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/divide.v(27[25:30])
    
    EHXPLLJ PLLInst_0 (.CLKI(clk_in_c), .CLKFB(clk), .PHASESEL0(GND_net), 
            .PHASESEL1(GND_net), .PHASEDIR(GND_net), .PHASESTEP(GND_net), 
            .LOADREG(GND_net), .STDBY(GND_net), .PLLWAKESYNC(GND_net), 
            .RST(GND_net), .RESETC(GND_net), .RESETD(GND_net), .RESETM(GND_net), 
            .ENCLKOP(GND_net), .ENCLKOS(GND_net), .ENCLKOS2(GND_net), 
            .ENCLKOS3(GND_net), .PLLCLK(GND_net), .PLLRST(GND_net), .PLLSTB(GND_net), 
            .PLLWE(GND_net), .PLLDATI0(GND_net), .PLLDATI1(GND_net), .PLLDATI2(GND_net), 
            .PLLDATI3(GND_net), .PLLDATI4(GND_net), .PLLDATI5(GND_net), 
            .PLLDATI6(GND_net), .PLLDATI7(GND_net), .PLLADDR0(GND_net), 
            .PLLADDR1(GND_net), .PLLADDR2(GND_net), .PLLADDR3(GND_net), 
            .PLLADDR4(GND_net), .CLKOP(clk)) /* synthesis FREQUENCY_PIN_CLKOP="120.000000", FREQUENCY_PIN_CLKI="12.000000", ICP_CURRENT="7", LPF_RESISTOR="8", syn_instantiated=1, LSE_LINE_FILE_ID=4, LSE_LCOL=9, LSE_RCOL=43, LSE_LLINE=28, LSE_RLINE=28 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(28[9:43])
    defparam PLLInst_0.CLKI_DIV = 1;
    defparam PLLInst_0.CLKFB_DIV = 10;
    defparam PLLInst_0.CLKOP_DIV = 4;
    defparam PLLInst_0.CLKOS_DIV = 1;
    defparam PLLInst_0.CLKOS2_DIV = 1;
    defparam PLLInst_0.CLKOS3_DIV = 1;
    defparam PLLInst_0.CLKOP_ENABLE = "ENABLED";
    defparam PLLInst_0.CLKOS_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS2_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS3_ENABLE = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_A0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_B0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_C0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_D0 = "DISABLED";
    defparam PLLInst_0.CLKOP_CPHASE = 3;
    defparam PLLInst_0.CLKOS_CPHASE = 0;
    defparam PLLInst_0.CLKOS2_CPHASE = 0;
    defparam PLLInst_0.CLKOS3_CPHASE = 0;
    defparam PLLInst_0.CLKOP_FPHASE = 0;
    defparam PLLInst_0.CLKOS_FPHASE = 0;
    defparam PLLInst_0.CLKOS2_FPHASE = 0;
    defparam PLLInst_0.CLKOS3_FPHASE = 0;
    defparam PLLInst_0.FEEDBK_PATH = "CLKOP";
    defparam PLLInst_0.FRACN_ENABLE = "DISABLED";
    defparam PLLInst_0.FRACN_DIV = 0;
    defparam PLLInst_0.CLKOP_TRIM_POL = "RISING";
    defparam PLLInst_0.CLKOP_TRIM_DELAY = 0;
    defparam PLLInst_0.CLKOS_TRIM_POL = "FALLING";
    defparam PLLInst_0.CLKOS_TRIM_DELAY = 0;
    defparam PLLInst_0.PLL_USE_WB = "DISABLED";
    defparam PLLInst_0.PREDIVIDER_MUXA1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXB1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXC1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXD1 = 0;
    defparam PLLInst_0.OUTDIVIDER_MUXA2 = "DIVA";
    defparam PLLInst_0.OUTDIVIDER_MUXB2 = "DIVB";
    defparam PLLInst_0.OUTDIVIDER_MUXC2 = "DIVC";
    defparam PLLInst_0.OUTDIVIDER_MUXD2 = "DIVD";
    defparam PLLInst_0.PLL_LOCK_MODE = 0;
    defparam PLLInst_0.STDBY_ENABLE = "DISABLED";
    defparam PLLInst_0.DPHASE_SOURCE = "DISABLED";
    defparam PLLInst_0.PLLRST_ENA = "DISABLED";
    defparam PLLInst_0.MRST_ENA = "DISABLED";
    defparam PLLInst_0.DCRST_ENA = "DISABLED";
    defparam PLLInst_0.DDRST_ENA = "DISABLED";
    defparam PLLInst_0.INTFB_WAKE = "DISABLED";
    INV i3133 (.A(clk), .Z(clk_N_242));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module \monopulse(N=6) 
//

module \monopulse(N=6)  (cst, clk, y_N_295, cnt, n3646, \cst[2] , 
            n3647, n3371, led1_c, n477, n3657, cnv_c, \nst_2__N_277[1] ) /* synthesis syn_module_defined=1 */ ;
    output [2:0]cst;
    input clk;
    input y_N_295;
    output [2:0]cnt;
    input n3646;
    output \cst[2] ;
    input n3647;
    input n3371;
    input led1_c;
    output n477;
    input n3657;
    output cnv_c;
    output \nst_2__N_277[1] ;
    
    wire clk /* synthesis SET_AS_NETWORK=clk, is_clock=1 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/top.v(21[6:9])
    wire [2:0]n48;
    wire [2:0]cst_c;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(26[11:14])
    wire [2:0]cnt_c;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(27[26:29])
    
    wire n3663, n3650, start_diff2, start_diff1, n904;
    
    FD1S3AX cst_FSM_i2 (.D(y_N_295), .CK(clk), .Q(cst[0]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam cst_FSM_i2.GSR = "DISABLED";
    FD1S3IX cnt__i0 (.D(n48[0]), .CK(clk), .CD(y_N_295), .Q(cnt[0])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=20, LSE_RCOL=2, LSE_LLINE=76, LSE_RLINE=83 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(73[7] 97[4])
    defparam cnt__i0.GSR = "DISABLED";
    FD1S3AX cst_FSM_i1 (.D(n3646), .CK(clk), .Q(cst_c[1]));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam cst_FSM_i1.GSR = "DISABLED";
    LUT4 i2018_2_lut_rep_36 (.A(cnt_c[1]), .B(cnt_c[2]), .Z(n3663)) /* synthesis lut_function=(A (B)) */ ;
    defparam i2018_2_lut_rep_36.init = 16'h8888;
    LUT4 i114_2_lut_rep_23_3_lut (.A(cnt_c[1]), .B(cnt_c[2]), .C(cst_c[1]), 
         .Z(n3650)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i114_2_lut_rep_23_3_lut.init = 16'h8080;
    FD1S3IX cst_FSM_i0 (.D(n3650), .CK(clk), .CD(n3647), .Q(\cst[2] ));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam cst_FSM_i0.GSR = "DISABLED";
    FD1S3IX start_diff2_36 (.D(start_diff1), .CK(clk), .CD(n3647), .Q(start_diff2));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(37[7] 47[4])
    defparam start_diff2_36.GSR = "DISABLED";
    LUT4 i464_3_lut (.A(cnt_c[1]), .B(n3371), .C(n904), .Z(n48[1])) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(B))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(88[8] 94[6])
    defparam i464_3_lut.init = 16'h6c6c;
    LUT4 i471_4_lut (.A(cnt_c[2]), .B(cnt_c[1]), .C(n904), .D(n3371), 
         .Z(n48[2])) /* synthesis lut_function=(!(A (B (C (D)+!C !(D))+!B !(C))+!A !(B (D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(88[8] 94[6])
    defparam i471_4_lut.init = 16'h6ca0;
    FD1S3IX start_diff1_35 (.D(led1_c), .CK(clk), .CD(n3647), .Q(start_diff1));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(37[7] 47[4])
    defparam start_diff1_35.GSR = "DISABLED";
    LUT4 i2035_3_lut_4_lut (.A(n3663), .B(cst_c[1]), .C(n477), .D(n3657), 
         .Z(n904)) /* synthesis lut_function=(A (B (D)+!B (C (D)))+!A (C (D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam i2035_3_lut_4_lut.init = 16'hf800;
    LUT4 i456_3_lut_4_lut (.A(n477), .B(n3657), .C(n904), .D(cnt[0]), 
         .Z(n48[0])) /* synthesis lut_function=(!(A (B (C (D))+!B !(C (D)))+!A !(C (D)))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam i456_3_lut_4_lut.init = 16'h7888;
    FD1S3IX y_38 (.D(n477), .CK(clk), .CD(y_N_295), .Q(cnv_c));   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(73[7] 97[4])
    defparam y_38.GSR = "DISABLED";
    FD1S3IX cnt__i2 (.D(n48[2]), .CK(clk), .CD(y_N_295), .Q(cnt_c[2])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=20, LSE_RCOL=2, LSE_LLINE=76, LSE_RLINE=83 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(73[7] 97[4])
    defparam cnt__i2.GSR = "DISABLED";
    FD1S3IX cnt__i1 (.D(n48[1]), .CK(clk), .CD(y_N_295), .Q(cnt_c[1])) /* synthesis LSE_LINE_FILE_ID=4, LSE_LCOL=20, LSE_RCOL=2, LSE_LLINE=76, LSE_RLINE=83 */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(73[7] 97[4])
    defparam cnt__i1.GSR = "DISABLED";
    LUT4 start_diff1_I_0_2_lut (.A(start_diff1), .B(start_diff2), .Z(\nst_2__N_277[1] )) /* synthesis lut_function=(!((B)+!A)) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(36[18:44])
    defparam start_diff1_I_0_2_lut.init = 16'h2222;
    LUT4 i584_4_lut (.A(cst_c[1]), .B(\nst_2__N_277[1] ), .C(n3663), .D(cst[0]), 
         .Z(n477)) /* synthesis lut_function=(A (B ((D)+!C)+!B !(C))+!A (B (D))) */ ;   // /home/yhp/data/stepfpga/ltc2357_debug_2020_07/monopulse.v(57[2] 68[9])
    defparam i584_4_lut.init = 16'hce0a;
    
endmodule
