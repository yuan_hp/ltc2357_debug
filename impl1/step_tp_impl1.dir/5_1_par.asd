[ActiveSupport PAR]
; Global primary clocks
GLOBAL_PRIMARY_USED = 4;
; Global primary clock #0
GLOBAL_PRIMARY_0_SIGNALNAME = clk;
GLOBAL_PRIMARY_0_DRIVERTYPE = PLL;
GLOBAL_PRIMARY_0_LOADNUM = 19;
; Global primary clock #1
GLOBAL_PRIMARY_1_SIGNALNAME = scki_c;
GLOBAL_PRIMARY_1_DRIVERTYPE = SLICE;
GLOBAL_PRIMARY_1_LOADNUM = 240;
; Global primary clock #2
GLOBAL_PRIMARY_2_SIGNALNAME = clk_in_c;
GLOBAL_PRIMARY_2_DRIVERTYPE = PIO;
GLOBAL_PRIMARY_2_LOADNUM = 91;
; Global primary clock #3
GLOBAL_PRIMARY_3_SIGNALNAME = pre_clk;
GLOBAL_PRIMARY_3_DRIVERTYPE = SLICE;
GLOBAL_PRIMARY_3_LOADNUM = 17;
; # of global secondary clocks
GLOBAL_SECONDARY_USED = 5;
; Global secondary clock #0
GLOBAL_SECONDARY_0_SIGNALNAME = busy_c;
GLOBAL_SECONDARY_0_DRIVERTYPE = CLK_PIN;
GLOBAL_SECONDARY_0_LOADNUM = 241;
GLOBAL_SECONDARY_0_SIGTYPE = CE+RST;
; Global secondary clock #1
GLOBAL_SECONDARY_1_SIGNALNAME = send_data_23_N_809_12;
GLOBAL_SECONDARY_1_DRIVERTYPE = SLICE;
GLOBAL_SECONDARY_1_LOADNUM = 15;
GLOBAL_SECONDARY_1_SIGTYPE = RST;
; Global secondary clock #2
GLOBAL_SECONDARY_2_SIGNALNAME = clk_in_c_enable_42;
GLOBAL_SECONDARY_2_DRIVERTYPE = SLICE;
GLOBAL_SECONDARY_2_LOADNUM = 24;
GLOBAL_SECONDARY_2_SIGTYPE = CE;
; Global secondary clock #3
GLOBAL_SECONDARY_3_SIGNALNAME = n3647;
GLOBAL_SECONDARY_3_DRIVERTYPE = SLICE;
GLOBAL_SECONDARY_3_LOADNUM = 33;
GLOBAL_SECONDARY_3_SIGTYPE = RST;
; Global secondary clock #4
GLOBAL_SECONDARY_4_SIGNALNAME = auto_rst_inst/clk_in_c_enable_62;
GLOBAL_SECONDARY_4_DRIVERTYPE = SLICE;
GLOBAL_SECONDARY_4_LOADNUM = 11;
GLOBAL_SECONDARY_4_SIGTYPE = CE;
; I/O Bank 0 Usage
BANK_0_USED = 0;
BANK_0_AVAIL = 26;
BANK_0_VCCIO = NA;
BANK_0_VREF1 = NA;
; I/O Bank 1 Usage
BANK_1_USED = 2;
BANK_1_AVAIL = 26;
BANK_1_VCCIO = 3.3V;
BANK_1_VREF1 = NA;
; I/O Bank 2 Usage
BANK_2_USED = 2;
BANK_2_AVAIL = 28;
BANK_2_VCCIO = 3.3V;
BANK_2_VREF1 = NA;
; I/O Bank 3 Usage
BANK_3_USED = 4;
BANK_3_AVAIL = 7;
BANK_3_VCCIO = 3.3V;
BANK_3_VREF1 = NA;
; I/O Bank 4 Usage
BANK_4_USED = 4;
BANK_4_AVAIL = 8;
BANK_4_VCCIO = 3.3V;
BANK_4_VREF1 = NA;
; I/O Bank 5 Usage
BANK_5_USED = 2;
BANK_5_AVAIL = 10;
BANK_5_VCCIO = 3.3V;
BANK_5_VREF1 = NA;
