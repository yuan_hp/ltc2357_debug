module top
(
	input clk_in,             //输入系统12MHz时钟
	input rst_n_in,           //输入复位信号 
	output led1,              //指示灯

	input busy, //以下是芯片测试连接的引脚
	output cnv, //触发信号
	output scki, //
	output sdi,
	output csn, //片选信号低电平有效 
	//input ltc_scko,
	input sdo0,  
	input sdo1, 
	input sdo2, 
	input sdo3,  
	//串口引脚
	input rs232_rx,
	output rs232_tx
);
wire clk;
wire  rst_n_auto,rst_n; //实现上电自动复位
assign rst_n = rst_n_auto & rst_n_in;
auto_rst auto_rst_inst(.clk(clk_in),.rst_n(rst_n_auto)); //自动复位信号
//wire scko;


pll_clk pll_clk(.CLKI(clk_in),.CLKOP(clk));  //clk 120MHz

wire [23:0]ad_data0 , ad_data1 ,ad_data2 ,ad_data3;

//产生采样频率控制时钟
divide #(
	.WIDTH(32),
	.N(12000) //
) divide_led (
	.clk(clk_in),
	.rst_n(rst_n),
	.clkout(led1)
);


//---------------------测试ltc2357-----------------------
//得到ltc的spi时钟 40MHz /2 = 20MHz

wire pre_clk;
divide #(
	.WIDTH(10),
	.N(3) //3分频
) divide_clk (
	.clk(clk),
	.rst_n(rst_n),
	.clkout(pre_clk)
);

//产生cnv信号
reg led1_diff1 ,led1_diff2;
wire led1_p;
wire done;
assign led1_p = done ? (led1_diff1 & (~led1_diff2)) : 1'b0;

always@(posedge clk)
begin
	if(!rst_n)begin
		led1_diff1 <= 0;
		led1_diff2 <= 0;
	end
	else begin
		led1_diff1 <= led1;
		led1_diff2 <= led1_diff1;
	end
end

//单脉冲信号产生模块 N设置单脉冲维持高电平的时间 N×Tclk 
//设置 1000 /(120/N) ns = 8.333333 × N （ns）
monopulse #(.N(6) )monopulse_inst  
(
    .clk(clk) ,
    .rst_n(rst_n),
	.start(led1), //触发信号
	.y(cnv)

);
ltc2357_master ltc2357_master(
	.clk(pre_clk),    //12MHz 
	.rst_n(rst_n),
	.busy(busy),
	.scki(scki),
	.csn(csn), 
	.sdi(sdi),
	//.scko(scko),
	.sdo0(sdo0),
	.sdo1(sdo1),
	.sdo2(sdo2),
	.sdo3(sdo3),
	.ad_data0(ad_data0),
	.ad_data1(ad_data1),
	.ad_data2(ad_data2),
	.ad_data3(ad_data3),
	.done(done)
);

//+++++++++++++++++  串口模块  +++++++++++++++++++++++
reg [7:0] tx_data;
wire [7:0] rx_data;
reg [23:0] tmp;
//assign tmp = 8'b11001010;
//ascii=8'h60 锁存即将发送到上位机的ad数据
always@(posedge clk_in)
begin
	tmp = (rx_data==8'h60) ? ad_data0 : 
		  (rx_data==8'h61) ? ad_data1 :
		  (rx_data==8'h62) ? ad_data2 :
		  (rx_data==8'h63) ? ad_data3 :tmp ;
	//tmp=24'haaaaaa;
	/*
	case(rx_data)  // 选通ch0 ～ ch1
		8'h60:tmp = ad_data0;
		8'h61:tmp = ad_data1;
		8'h62:tmp = ad_data2;
		8'h63:tmp = ad_data3;
		default: tmp = tmp;
	endcase	*/
end

//转换数据 -- 实现发送到上位机
always@(rx_data)
begin
	if(rx_data >= 8'd0 && rx_data < 8'd24 )begin //处理需要发送的数据
		tx_data = 8'h30 + ((tmp >> rx_data) & 24'd1 ) ;
	end
	else begin
		tx_data = 8'h20; //空格的ascii
	end
end

Uart_Bus #(.BPS_PARA(1250) //当使用12MHz时钟时波特率参数选择1250对应9600的波特率
)
Uart_Debug(
    .clk_in(clk_in),			//系统时钟
    .rst_n_in(rst_n_in),		//系统复位，低有效
    .rs232_rx(rs232_rx),		//FPGA中UART接收端，分配给UART模块中的发送端TXD
    .rs232_tx(rs232_tx),		//FPGA中UART发送端，分配给UART模块中的接收端RXD
	.rx_data(rx_data),
	.tx_data(tx_data)
);	

endmodule
